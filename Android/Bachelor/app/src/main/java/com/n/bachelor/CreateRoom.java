package com.n.bachelor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Switch;


import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Standard class for displaying the base out of 2 fragments
 */
public class CreateRoom extends AppCompatActivity {

    SharedPreferences sharedpreferences;


    /**
     * onCreate runs when the room is created and gets a view from either fragment selected by the user
     * If user is on CasualFragment display it, same with PartyFragment
     * @param savedInstanceState - the state of the mobile device.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_room);

        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);


    }

    /**
     * Method to check what radio button is checked.
     * @param view - gets the view which the radio button is displayed in.
     *             This way we can get the id of the radio button, and set the right category.
     */

    public void onRadioButtonClicked(View view){

        boolean checked = ((RadioButton) view).isChecked();

        sharedpreferences = getSharedPreferences("cat", 0);
        SharedPreferences.Editor editor = sharedpreferences.edit();


        //Switch-statement to check what button is selected, which is sendt to SharedPreference
        switch (view.getId()){
            case R.id.casual_Batman:
                if(checked){
                    editor.putString("cat", "batman");
                }
                break;
            case R.id.musikk_cat:
                if(checked){
                    editor.putString("cat", "musikk");
                }
                break;
            case R.id.meme_cat:
                if(checked){
                    editor.putString("cat", "memes");
                }
                break;
            case R.id.øl_cat:
                if(checked) {
                    editor.putString("cat", "Øl");
                }
                break;
            case R.id.random_cat:
                if(checked) {
                    editor.putString("cat", "random");
                }
                break;
            case R.id.pop_cat:
                if(checked) {
                    editor.putString("cat", "pop");
                }
                break;
        }
        editor.apply();


    }


}
