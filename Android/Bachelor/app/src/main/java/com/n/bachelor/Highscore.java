package com.n.bachelor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EventListener;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class for displaying the highscore after a game is completed
 * User is sendt here from StartGame.
 */
public class Highscore extends AppCompatActivity {

    //Global var for the class
    String name, code;
    int score;
    FirebaseDatabase database;
    DatabaseReference roomsRef;
    ArrayList<String> roomsList;
    RecyclerView list;
    final ArrayList<User> userList = new ArrayList<>();
    final ArrayList<User> activeUserList = new ArrayList<>();
    ValueEventListener evListener;

    /**
     * onCreate runs when the activity is created.
     * Get the info from the user to display the score.
     * @param savedInstanceState - state of the device
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);
        Intent il = getIntent();
        name = il.getStringExtra("name");
        code = il.getStringExtra("roomcode");
        score = il.getIntExtra("score", 0);

        list = findViewById(R.id.highscoreList);
        database = FirebaseDatabase.getInstance();
        DatabaseReference mDatabase= database.getReference();
        mDatabase.child("rooms").child(code).child(name).child("score").setValue(score);


        addRoomsEventListener();
    }

    /**
     * Gets all the users from the database, then sends a list to adapter class to display score and name
     */
    private void addRoomsEventListener() {
        roomsRef = database.getReference("rooms/" + code);
        evListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, Object> maps = (Map<String, Object>) dataSnapshot.getValue();

                if (maps != null) {
                    for (Map.Entry<String, Object> map : maps.entrySet()) {
                        if (map.getKey().equals("category") || map.getKey().equals("message"))
                            continue;

                        if (map.getValue() != null && map.getValue() instanceof Map) {
                            User addUser = new User(map.getKey());
                            activeUserList.add(addUser);
                            Map<String, Object> castMap = (Map<String, Object>) map.getValue();
                            for (Map.Entry<String, Object> participant : castMap.entrySet()) {
                                System.out.println("Key: " + participant.getKey() + ", value: " + participant.getValue());
                                if (participant.getKey().equals("score")) {
                                    addUser.setScore((long) (participant.getValue()));
                                }
                            }

                            boolean contains = false;
                            for (User u : userList) {
                                if (u.getName().equals(addUser.getName())) {
                                    contains = true;
                                    break;
                                }
                            }

                            if (!contains) {
                                userList.add(addUser);
                                System.out.println("Legger til bruker: " + addUser.getName() + " i liste!");
                            } else {
                                System.out.println("Mangler ikke bruker: " + addUser.getName());
                                for (int i = 0; i < userList.size(); i++) {
                                    if (userList.get(i).getName().equals(addUser.getName())) {
                                        if (userList.get(i).getScore() < addUser.getScore()) {
                                            userList.get(i).setScore(addUser.getScore());
                                            System.out.println("Setter score for bruker " + userList.get(i).getName() + " til score: " + userList.get(i).getScore());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (userList.size() > 0) {
                    System.out.println("Størrelse på userList er : " + userList.size());
                    Collections.sort(userList);
                    for (User user : userList) {
                        System.out.println(user.getName() + ": " + user.getScore());
                    }
                    HighscoreAdapter adapter = new HighscoreAdapter(getBaseContext(), userList);
                    list.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                    list.setAdapter(adapter);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Database error", databaseError.toString());
            }
        };
        roomsRef.addValueEventListener(evListener);
    }


    /**
     * When user presses the back button, leave the room, and send the user to a different activity.
     */
    public void onBackPressed(){
        checkRoom();
        roomsRef.removeEventListener(evListener);
        Handler wait = new Handler();
        wait.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                Intent il = new Intent(getBaseContext(), MainActivity.class);
                startActivity(il);
            }
        }, 1000);


    }

    /**
     * Checks if the user is the last one in the room, if so, delete the room.
     */
    public void checkRoom(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference refSpiller = database.getReference("rooms/").child(code);

        refSpiller.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                ArrayList<String> antSpillere = new ArrayList<>();
                if(map != null){
                    for(String s : map.keySet()){
                        if(!s.equals("message") && !s.equals("category") && !s.equals(name)){
                            antSpillere.add(s);
                        }
                    }

                    if(antSpillere.isEmpty()){
                        deleteGame();
                    } else{
                        deleteSpiller();

                    }
                }
                antSpillere.clear();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    /**
     * Leave room.
     * Triggers when user presses the leave button
     * @param view - view that the button is in.
     */
    public void leave(View view) {
        onBackPressed();

    }

    /**
     * Deleting a single player from the database
     * @params null
     */

    public void deleteSpiller(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rooms/"+code).child(name);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot stock : dataSnapshot.getChildren()) {
                    stock.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("error", "error" + error);
            }
        });

    }

    /**
     * Deletes the room from the database.
     *
     */
    public void deleteGame(){
        deleteSpiller();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference gameRef = database.getReference("rooms/"+code);

        gameRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot stock : dataSnapshot.getChildren()){
                    stock.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
