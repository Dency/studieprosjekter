package com.n.bachelor;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

/**
 * Adapter class for displaying.
 */
public class HighscoreAdapter extends RecyclerView.Adapter<HighscoreAdapter.HighscoreAdapterHolder>{

    private LayoutInflater minInflater;
    private ArrayList<User> playerList;

    /**
     * Constructor for making adapter
     * @param context
     * @param playerList
     */
    public HighscoreAdapter(Context context, ArrayList<User> playerList) {
        minInflater = LayoutInflater.from(context);
        this.playerList = playerList;
        Log.d("Spiller liste 2 : ", playerList.toString());
    }


    /**
     * When Highscore gets created this method runs
     * @param parent - The parent adapter
     * @param viewType - the brach of the current view for the device
     * @return
     */
    @NonNull
    @Override
    public HighscoreAdapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mItemView = minInflater.inflate(R.layout.activity_highscore_items, parent, false);
        return new HighscoreAdapterHolder(mItemView);
    }

    /**
     * Holder for alle elementene
     * @param holder - holder for the items
     * @param pos - what position in the array which should be applied
     */
    public void onBindViewHolder(HighscoreAdapter.HighscoreAdapterHolder holder, int pos) {


        User u = playerList.get(pos);
        HighscoreAdapterHolder wh = (HighscoreAdapterHolder) holder;
        wh.name.setText(u.getName());
        wh.score.setText("Score : " + u.getScore());


    }

    /**
     * Get size of the array
     * @return the size of the array
     */
    @Override
    public int getItemCount() {
        return playerList.size();
    }

    /**
     * Inner class for making an object based on the holder
     * Extends this class with the RecyclerView
     */
    public class HighscoreAdapterHolder extends RecyclerView.ViewHolder{
        private TextView name;
        private TextView score;

        /**
         * Constructor for the holder. Sets the text of the different elements which will be displayed.
         * @param itemView - gets the current view where the holder should be placed
         */
        public HighscoreAdapterHolder(@NonNull View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.nameList);
            this.score = itemView.findViewById(R.id.scoreList);
        }
    }
}