package com.n.bachelor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The first activity where the user lands when booting up the application
 */
public class MainActivity extends AppCompatActivity {

    //Global variables
    ArrayList<String> roomList;
    String username ="", room="";
    EditText name;
    EditText roomcode;
    DatabaseReference ref;
    ValueEventListener evListener;


    /**
     * Runs when the activity is created. Gathers the informastion which is written by the user.
     * @param savedInstanceState - The state of the device
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name = findViewById(R.id.username);
        roomcode = findViewById(R.id.roomcode);
        username ="";
        room="";
    }

    /**
     * Runs when the activity is resumed after a pause
     */
    protected void onResume(){
        super.onResume();
    }

    /**
     * Sends the user to a room if the name is not taken and the code to the room is correct.
     * @param view - the View of the button.
     */
    public void goToRoom(View view) {
        //Getting userinput
        username = name.getText().toString();
        room = roomcode.getText().toString();
        if(!username.equals("")) {

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            ref = database.getReference("rooms/"+room);
            evListener = new ValueEventListener(){

            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                ArrayList<String> spillere = new ArrayList<String>();
                if(map != null && !map.isEmpty()) {
                    for (String s : map.keySet()) {
                        spillere.add(s);
                    }
                    countPlayers(spillere);
                } else {
                    showToast("Feil romkode");
                }
            }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            };

            ref.addValueEventListener(evListener);

        }

    }

    /**
     * Sends message to the user with a Toast
     * @param s - the text displayed on the Toast
     */
    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }


    /**
     * checks the amount of players in a room. then sends the user to a room.
     * @param spillere  - array of player names
     */
    private void countPlayers(ArrayList<String> spillere) {
        System.out.println("countplayers kjører!");
        ref.removeEventListener(evListener);

        Log.d("spillere", "Spiller array" + spillere);
        System.out.println("Brukernavn " + username);

        boolean nameTaken = false;

        for(String s: spillere){
            if(s.equals(username)) {
                nameTaken = true;
                break;
            }
        }

        if(nameTaken){
            showToast("Brukernavn er tatt");
        } else {

            //Database reference
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

            //Creating object user and pushing it firebase
            User user = new User(username, room, 0, false);
            mDatabase.child("rooms").child(room).child(username).setValue(user);

            //Sending user to next activity with the room number
            Intent intent = new Intent(this, WaitingRoom.class);
            intent.putExtra("roomcode", room);
            intent.putExtra("name", username);
            startActivity(intent);
        }

    }

    /**
     * Sends the user to CreateRoom where they can create a room instead of joining one.
     * Triggered by onClick.
     * @param view - where the button is placed
     */
    public void openSettings(View view) {
    Intent intent = new Intent(this, CreateRoom.class);
    startActivity(intent);
    }
}
