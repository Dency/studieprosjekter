package com.n.bachelor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for creating a view in the Party fragment
 */
public class PartyFragment extends Fragment {

    //Global variable
    DatabaseReference mDatabase;

    /**
     * onCreate runs when the activity is created
     * @param inflater - inflating so the view fits the screen
     * @param container - holder for the elements
     * @param savedInstanceState - the state of the device
     * @return - returns a view which is displayed to the user in CreateRoom.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View rootView = inflater.inflate(R.layout.party_fragment, container, false);


        mDatabase = FirebaseDatabase.getInstance().getReference();

        final RadioGroup radioGroup = rootView.findViewById(R.id.party_radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                EditText userField = rootView.findViewById(R.id.party_username);
                EditText roomField = rootView.findViewById(R.id.party_roomcode);

                String room = roomField.getText().toString();
                String user = userField.getText().toString();

                if(!room.equals("") && !user.equals("")) {
                    Button button = rootView.findViewById(R.id.create_party);
                    button.setEnabled(true);
                }
            }
        });

        //Checks if the name is filled in
        final EditText userField = rootView.findViewById(R.id.party_username);
        final EditText roomField = rootView.findViewById(R.id.party_roomcode);
        userField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Button button = rootView.findViewById(R.id.create_party);
                boolean checked;
                if (radioGroup.getCheckedRadioButtonId() == -1)
                {
                    checked = false;
                }
                else
                {
                    checked = true;
                }
                if (checked) {
                    String room = roomField.getText().toString();
                    String user = userField.getText().toString();
                    if (s.toString().trim().length() == 0 || room.equals("") || user.equals("")) {
                        button.setEnabled(false);
                    } else {
                        button.setEnabled(true);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //Checks that the roomcode is filled in
        roomField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Button button = rootView.findViewById(R.id.create_party);
                boolean checked;
                if (radioGroup.getCheckedRadioButtonId() == -1)
                {
                    checked = false;
                }
                else
                {
                    checked = true;
                }
                if (checked) {
                    String room = roomField.getText().toString();
                    String user = userField.getText().toString();
                    if (s.toString().trim().length() == 0 || room.equals("") || user.equals("")) {
                        button.setEnabled(false);
                    } else {
                        button.setEnabled(true);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        //When user clicks the button to create the room, send data to database and send user to the room as host
        Button buttonInFragment1 = rootView.findViewById(R.id.create_party);
        buttonInFragment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView tNavn = rootView.findViewById(R.id.party_username);
                TextView tCode = rootView.findViewById(R.id.party_roomcode);

                String name = tNavn.getText().toString();
                String code = tCode.getText().toString();

                User user = new User(name, code, 0, true);

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("rooms/"+ user.getRoomcode(), name);

                mDatabase.updateChildren(childUpdates);
                SharedPreferences preferences = getActivity().getSharedPreferences("cat", 0);
                String cat = preferences.getString("cat", "");
                System.out.println("Sjekker SP: " + cat);

                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child("rooms").child(code).child(name).setValue(user);
                mDatabase.child("rooms").child(code).child("message").setValue("waiting");
                mDatabase.child("rooms").child(code).child("category").setValue(cat);





                Intent il = new Intent(rootView.getContext(), WaitingRoom.class);
                il.putExtra("name", name);
                il.putExtra("roomcode", code);
                startActivity(il);

            }
        });

        return rootView;


    }

    //What happens based on the item u selected in the spinner
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }

    //
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }


}