package com.n.bachelor;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Map;

/**
 * Class to setup the functions needed when the player plays the game.
 */
public class StartGame extends AppCompatActivity {

    //Global Variables
    String name, code, cat;
    long ant;
    TextView spm, points;
    Button svar1, svar2, svar3, svar4;
    int score = 0, runde = 0;
    ArrayList<String> spmArray;
    String riktigSvar;
    Drawable btn, btnGreen, btnRed;


    /**
     * Runs when the activity is created. Finds the different buttons to be accessed later.
     *
     * @param savedInstanceState - the state of the machine
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);
        Intent i = getIntent();
        name = i.getStringExtra("name");
        code = i.getStringExtra("roomcode");
        countUsers();
        System.out.println("ANT ER: " + ant);
        spm = findViewById(R.id.spm);
        svar1 = findViewById(R.id.svar1);
        svar2 = findViewById(R.id.svar2);
        svar3 = findViewById(R.id.svar3);
        svar4 = findViewById(R.id.svar4);
        points = findViewById(R.id.score);

        btn = ContextCompat.getDrawable(this, R.drawable.rounded_button);
        btnGreen = ContextCompat.getDrawable(this, R.drawable.rounded_green);
        btnRed = ContextCompat.getDrawable(this, R.drawable.rounded_red);


        getCat();
    }


    /**
     * Runs when the activity stops.
     * If there are no players delete the game, else just delete the user.
     */
    public void onStop(){
        super.onStop();

        countUsers();
        System.out.println("ANT ER: " + ant);


        if(ant == 3 || ant < 3)
            if(runde != 5)
                deleteGame();
        if(runde != 5)
            deleteUser(name, code);

        if(runde != 5){
            Intent il = new Intent(this, MainActivity.class);
            startActivity(il);
        }

    }

    /**
     * Method for deleting the room
     */
    public void deleteGame(){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rooms/"+code);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot stock : dataSnapshot.getChildren()) {
                    stock.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("error", "error" + error);
            }
        });
    }

    /**
     * Method for deleting a single user
     * @param name - name of the user
     * @param roomcode - what room the user is in
     */
    public void deleteUser(final String name, String roomcode){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rooms/"+roomcode).child(name);

        myRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot stock : dataSnapshot.getChildren()) {
                    stock.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("error", "error" + error);
            }
        });

    }

    /**
     * Cpunting how many users are left in the room
     */
    public void countUsers(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("rooms/"+code);
        System.out.println("REF TIL COUNT PLAYERS: " + code);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                getCount(dataSnapshot.getChildrenCount());
                System.out.println("ANT ER: " + dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //Sets the amount of users
    private void getCount(long count) {
        ant = count;
    }

    /**
     * Method for getting the category from CreateRoom, so the right questions are asked
     */
    public void getCat(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("rooms/"+code).child("category");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(String.class) != null)
                    cat= dataSnapshot.getValue(String.class);
                System.out.println("Sjekker cat: " + cat);
                getQuestion();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /**
     * Finding the questions from the database
     */
    public void getQuestion(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference catQ = database.getReference("category/"+cat);
        System.out.println("Sjekker catQ " + catQ);

        catQ.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    spmArray = new ArrayList<String>();

                    for (String s : map.keySet()) {
                        spmArray.add(s);
                    }

                    System.out.println("Sjekker spm Array: " + spmArray.toString());
                    changeQuestion();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    /**
     * Sets the different alternatives for the answers
     * @param s - the question
     */
    public void setAlt(String s){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference altRef = database.getReference("category/"+cat+"/" + s);
        System.out.println("Sjekker altRef: " + altRef);

        altRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null){
                    Map<String, Boolean> map = (Map<String, Boolean>) dataSnapshot.getValue();
                    ArrayList<String> svarArray = new ArrayList<String>();

                    for (Map.Entry<String, Boolean> entry : map.entrySet()) {
                        System.out.println(entry.getKey() + ":" + entry.getValue());
                        svarArray.add(entry.getKey());
                        if(entry.getValue())
                            riktigSvar = entry.getKey();
                    }

                    System.out.println("Sjekker svar: " + svarArray.toString());

                    svar1.setText(svarArray.get(0));
                    svar2.setText(svarArray.get(1));
                    svar3.setText(svarArray.get(2));
                    svar4.setText(svarArray.get(3));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    /**
     * Checks if the answer given is right or wrong
     * @param v - the View of the button pressed
     */
    public void sjekkSvar(View v) {
        Handler riktig = new Handler();
        Handler feil = new Handler();


        //Switch-case to check what answer is pressed
        switch(v.getId()){
            case R.id.svar1:
                if(svar1.getText().toString().equals(riktigSvar)){
                    score++;
                    System.out.println("Sjekker score 1: " + score);
                    svar1.setBackground(btnGreen);
                    riktig.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            svar1.setBackground(btn);
                            changeQuestion();
                        }
                    }, 1000);
                } else {
                    svar1.setBackground(btnRed);
                    feil.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            svar1.setBackground(btn);
                            changeQuestion();
                        }
                    }, 1000);
                } break;

            case R.id.svar2:
                if(svar2.getText().toString().equals(riktigSvar)){
                    score++;
                    System.out.println("Sjekker score 2: " + score);
                    svar2.setBackground(btnGreen);
                    riktig.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            svar2.setBackground(btn);
                            changeQuestion();
                        }
                    }, 1000);
                } else {
                    svar2.setBackground(btnRed);
                    feil.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            svar2.setBackground(btn);
                            changeQuestion();
                        }
                    }, 1000);
                }break;

            case R.id.svar3:
                if(svar3.getText().toString().equals(riktigSvar)){
                    score++;
                    System.out.println("Sjekker score 3: " + score);
                    svar3.setBackground(btnGreen);
                    riktig.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            svar3.setBackground(btn);
                            changeQuestion();
                        }
                    }, 1000);
                } else {
                    svar3.setBackground(btnRed);
                    feil.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            svar3.setBackground(btn);
                            changeQuestion();
                        }
                    }, 1000);
                } break;

            case R.id.svar4:
                if(svar4.getText().toString().equals(riktigSvar)){
                    score++;
                    System.out.println("Sjekker score 1: " + score);
                    svar4.setBackground(btnGreen);
                    riktig.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            svar4.setBackground(btn);
                            changeQuestion();
                        }
                    }, 1000);

                } else {
                    svar4.setBackground(btnRed);
                    feil.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            svar4.setBackground(btn);
                            changeQuestion();
                        }
                    }, 1000);

                }break;
        }

    }

    /**
     * Method for changing the question or sending the user to Highscore depending on the round number.
     */
    public void changeQuestion(){
        points.setText("Score: " +score);

        if(runde == 5){
            System.out.println("To highscore");
            Intent u = new Intent(this, Highscore.class);
            u.putExtra("name", name);
            u.putExtra("roomcode", code);
            u.putExtra("score", score);
            startActivity(u);
        }

        if(runde <5){

            spm.setText(spmArray.get(runde) + "?");

            setAlt(spmArray.get(runde));
            runde++;
            System.out.println("Rundenr: " + runde);
        }

    }
}
