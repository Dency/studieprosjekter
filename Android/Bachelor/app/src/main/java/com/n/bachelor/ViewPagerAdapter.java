package com.n.bachelor;

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/**
 * Adapter for CreateRoom.
 * Displays different view baseed on what the user wants
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {

    private Fragment[] childFragments;

    //Constructing the fragment view
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        childFragments = new Fragment[] {
                new CasualFragment(), //0
                new PartyFragment() //1
        };
    }

    //Return the selected fragment
    @Override
    public Fragment getItem(int position) {
        return childFragments[position];
    }

    @Override
    public int getCount() {
        return childFragments.length; //3 items
    }

    //Sends back what fragment is selected
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Casual Mode";
        else return "Party Mode";
    }
}
