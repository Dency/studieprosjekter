package com.n.bachelor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Map;


/**
 * Class for displaying a WaitingRoom before a game
 */
public class WaitingRoom extends AppCompatActivity {

    //Global variables
    FirebaseDatabase database;
    DatabaseReference messageRef;
    String code, name;
    Boolean userLeft = true;

    /**
     * When the activity is created. Gets the view from the different textfields to show other players joining
     * @param savedInstanceState - the state of the device
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_room);


        //Getting roomcode for last activity
        TextView roomcode = findViewById(R.id.roomcode);
        Intent il = getIntent();
        code = il.getStringExtra("roomcode");
        name = il.getStringExtra("name");

        canceledGameEventListener();
        database = FirebaseDatabase.getInstance();
        messageRef = database.getReference("rooms/" + code + "/message");

        //Shows room code and other players
        roomcode.setText(code);
        getPlayers(code);

        //Listener for when the game starts
        startGameEventListener();



    }

    /**
     * gets the players from the database
     * @param code - the code for the room
     */
    private void getPlayers(String code) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("rooms/"+code);
        System.out.println("Testing : " + ref);

        // Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    ArrayList<String> spillere = new ArrayList<String>();

                    for (String s : map.keySet()) {
                        if(!s.equals("message"))
                            if (!s.equals("category"))
                                spillere.add(s);
                    }

                    showPlayers(spillere);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }

        });

        checkHost(name, code);

    }

    /**
     * Sends array to adapter to display players in a recyclerView
     * @param name - Array of the names of the players
     */
    private void showPlayers(ArrayList<String> name){
        RecyclerView playerList = findViewById(R.id.playerList);
        WaitingRoomAdapter adapter = new WaitingRoomAdapter(this, name);
        playerList.setAdapter(adapter);
        playerList.setLayoutManager(new LinearLayoutManager(this));

    }

    /**
     * if the back button is pressed, checks if the user is the host.
     * If user = host delete the room and kick everyone out.
     * If not just delete the user and open MainActivity.
     */
    @Override
    public void onBackPressed(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("rooms/"+code+"/"+name).child("host");

        messageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(String.class) != null){
                    if(dataSnapshot.getValue(String.class).contains("waiting")){

                        myRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.getValue(Boolean.class) != null)
                                if(dataSnapshot.getValue(Boolean.class)){
                                    System.out.println("onStop Canceling");
                                    messageRef.setValue("canceled");

                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Intent il = getIntent();
        String roomcode = il.getStringExtra("roomcode");
        String name = il.getStringExtra("name");
        if(userLeft)
            deleteUser(name, roomcode);

        userLeft = true;

    }


    /**
     * method for deleting a user.
     * @param name - name of the user
     * @param roomcode - code of the room
     */
    public void deleteUser(final String name, String roomcode){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rooms/"+roomcode).child(name);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot stock : dataSnapshot.getChildren()) {
                    stock.getRef().removeValue();
                }
                startMain();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("error", "error" + error);
            }
        });



    }

    /**
     * Method for starting the MainActivity
     */
    private void startMain(){

        Intent i = new Intent(getBaseContext(), MainActivity.class);
        i.putExtra("name", name);
        i.putExtra("code", code);
        startActivity(i);

    }

    /**
     * Checks if the user is the host
     * @param name - name of the player
     * @param code - the code of the room
     */
    public void checkHost(String name, String code){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rooms/"+code+"/"+name).child("host");


        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(Boolean.class) != null){
                    if(dataSnapshot.getValue(Boolean.class)){
                        Button b = findViewById(R.id.start);
                        b.setVisibility(View.VISIBLE);

                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    /**
     * if the host presses run game, changes a value in the database which sends all the players to StartGame.
     * @param view
     */
    public void gameStart(View view) {
        System.out.println("Knappen kjører");
        messageRef.setValue("start");
    }

    /**
     * Starts the game and sends with the name and roomcode.
     */
    public void start(){
        userLeft = false;
        Intent il = getIntent();
        String name = il.getStringExtra("name");
        String roomcode = il.getStringExtra("roomcode");

        Intent game = new Intent(this, StartGame.class);
        game.putExtra("name", name);
        game.putExtra("roomcode", roomcode);
        startActivity(game);
    }

    /**
     * Listener to check if the game is starting
     */
    private void startGameEventListener(){
        messageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(String.class) != null){
                    if(dataSnapshot.getValue(String.class).contains("start")) {
                        start();
                    } else if(dataSnapshot.getValue(String.class).contains("canceled")){
                        System.out.println("Canceling game!!");
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /**
     * Method if the host leave kick everyone out and delete the room
     */
    public void canceledGameEventListener(){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("rooms/" + code).child("message");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(String.class) != null) {
                    if (dataSnapshot.getValue(String.class).contains("canceled")) {
                        DatabaseReference roomRef = database.getReference("rooms/").child(code);
                        roomRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot stock : dataSnapshot.getChildren()) {
                                    stock.getRef().removeValue();
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
