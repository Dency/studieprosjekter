package com.n.bachelor;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

/**
 * Class for creating adapters in WaitingRoom
 */
public class WaitingRoomAdapter extends RecyclerView.Adapter<WaitingRoomAdapter.WaitingRoomHolder>{

    private LayoutInflater minInflater;
    private ArrayList<String> playerList;

    /**
     * Constructor for creating adapter object.
     * @param context - the context required for the adapter
     * @param playerList - the list of player names that should be displayed
     */
    public WaitingRoomAdapter(Context context, ArrayList<String> playerList) {
        minInflater = LayoutInflater.from(context);
        this.playerList = playerList;
        Log.d("Spiller liste 2 : ", playerList.toString());
    }


    /**
     * Hoolder for different elements,  created when the constructor runs
     * @param parent - the view for the context
     * @param viewType - the branch of the view
     * @return waitingRoomHolder object
     */
    @NonNull
    @Override
    public WaitingRoomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mItemView = minInflater.inflate(R.layout.activity_adapter_items, parent, false);
        return new WaitingRoomHolder(mItemView);
    }

    /**
     *Binds the elements together and creates a object holder.
     * @param holder - holder for gatherring elements
     * @param pos - position in the arrayList
     */
    public void onBindViewHolder(WaitingRoomAdapter.WaitingRoomHolder holder, int pos) {

        String name = playerList.get(pos);
        WaitingRoomHolder wh = (WaitingRoomHolder) holder;
        wh.name.setText(name);

    }

    /**
     * Size of the array
     * @return - int, size of the array
     */
    @Override
    public int getItemCount() {
        return playerList.size();
    }

    /**
     * Inner class for constructing a holder.
     */
    public class WaitingRoomHolder extends RecyclerView.ViewHolder{
        private TextView name;

        //Constructor for the holder
        public WaitingRoomHolder(@NonNull View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.name);
        }
    }
}