﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PilSpill
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            Form2 f = new Form2();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Hei");
        }

    }



    //Indreklasse for å få nytt GUI vindu 
    class Form2 : Form1
    {
        //Global var
        public List<int> tallListe, tallListe2;
        public FlowLayoutPanel f;
        private Form1 NewForm;
        private Timer tid;
        private int counter = 30;
        private int level = 1;
        private Label timeLabel;
        private Label lvlLabel;
        public int telle = 0;
        public Boolean first = true;

        //Konstruktør av Form 2
        public Form2() : base()
        {
            NewForm = new Form1();
            NewForm.Controls.Clear();
            NewForm.Size = new Size(800, 600);
            InitializeComponent();
            tallListe = Pil(4);
            Image i;

            f = new FlowLayoutPanel();

            f.Size = new Size(NewForm.Width, NewForm.Height - 100);
            f.Padding = new Padding(0, 0, NewForm.Width / 4, 0);
            f.BackColor = Color.White;

            //Legger inn bilder
            for (int y = 0; y < tallListe.Count; y++)
            {
                switch (tallListe[y])
                {
                    case 1:
                        i = new Bitmap("C:\\Users\\Martin\\source\\repos\\DesktopApp1\\left.png");
                        f.Controls.Add(new PictureBox { Image = i, SizeMode = PictureBoxSizeMode.Zoom });
                        break;
                    case 2:
                        i = new Bitmap("C:\\Users\\Martin\\source\\repos\\DesktopApp1\\up.png");
                        f.Controls.Add(new PictureBox { Image = i, SizeMode = PictureBoxSizeMode.Zoom });
                        break;
                    case 3:
                        i = new Bitmap("C:\\Users\\Martin\\source\\repos\\DesktopApp1\\right.png");
                        f.Controls.Add(new PictureBox { Image = i, SizeMode = PictureBoxSizeMode.Zoom });
                        break;
                    case 4:
                        i = new Bitmap("C:\\Users\\Martin\\source\\repos\\DesktopApp1\\down.png");
                        f.Controls.Add(new PictureBox { Image = i, SizeMode = PictureBoxSizeMode.Zoom });
                        break;
                }

                f.Padding = new Padding(50);

                f.Padding = new Padding(200, NewForm.Height / 3, NewForm.Width / 6 - 100, 0);



                NewForm.Controls.Add(f);

            }

            //Legger til timer
            tid = new System.Windows.Forms.Timer();
            tid.Tick += timer_eventhandler;
            tid.Interval = 1000;
            timeLabel = new Label()
            {
                Text = $"Tid: {counter}",
                Location = new System.Drawing.Point(NewForm.Width / 2 - 150, NewForm.Height - 75)

            };

            //Legger til Level tekst
            lvlLabel = new Label()
            {
                Text = $"Level: {level}",
                Location = new System.Drawing.Point(NewForm.Width / 2 + 50, NewForm.Height - 75),
                Size = new Size(200, 200)
            };

            timeLabel.Font = new Font(timeLabel.Font.Name, 18, FontStyle.Bold);
            NewForm.ResumeLayout(false);
            NewForm.PerformLayout();
            lvlLabel.Font = new Font(lvlLabel.Font.Name, 18, FontStyle.Bold);
            NewForm.KeyDown += layout_KeyPressed;
            NewForm.Controls.Add(timeLabel);
            NewForm.Controls.Add(lvlLabel);

            NewForm.Show();

            this.Dispose(true);
        }

        //Timer event handler som får tiden til å bevege seg nedover
        private void timer_eventhandler(object sender, EventArgs e)
        {
            counter--;
            timeLabel.Text = $"Tid: {counter}";
            if (counter == 0)
            {
                tid.Stop();
                NewForm.KeyDown -= layout_KeyPressed;
                timeLabel.Text = $"Tid: {counter}";
            }
            else if (counter <= 10)
            {
                timeLabel.ForeColor = Color.Red;
            }
        }

        //Random objekt
        private static readonly Random r = new Random();

        //Henter ut tilfeldige tall
        public List<int> Pil(int antall)
        {
            List<int> liste = new List<int>();


            for (int i = 0; i < antall; i++)
            {
                int tall = r.Next(1, 5);
                liste.Add(tall);
            }

            return liste;
        }

        //Når bruker trykker på en knapp, sjekkes hvilken knapp som trykkes
        private void layout_KeyPressed(object sender, KeyEventArgs e)
        {
            //Første gang spillet kjøres
            if (first)
            {
                tid.Start();
                tallListe2 = new List<int>(tallListe);
                first = false;
            }


            //Sjekker forskjellige knapper
            if (e.KeyCode == Keys.Left)
            {
                Console.WriteLine("Left");
                if (tallListe2[0] == 1)
                {
                    tallListe2.RemoveAt(0);
                    NewForm.Controls.Clear();
                    addAll(tallListe2);
                    telle++;
                }
                else
                {
                    addAll(tallListe);
                    tallListe2 = new List<int>(tallListe);
                    telle = 0;
                }

            }
            else if (e.KeyCode == Keys.Up)
            {
                Console.WriteLine("Up");
                if (tallListe2[0] == 2)
                {
                    tallListe2.RemoveAt(0);
                    NewForm.Controls.Clear();
                    addAll(tallListe2);
                    telle++;
                }
                else
                {
                    addAll(tallListe);
                    tallListe2 = new List<int>(tallListe);
                    telle = 0;
                }

            }
            else if (e.KeyCode == Keys.Right)
            {
                Console.WriteLine("Right");
                if (tallListe2[0] == 3)
                {
                    tallListe2.RemoveAt(0);
                    NewForm.Controls.Clear();
                    addAll(tallListe2);
                    telle++;
                }
                else
                {
                    addAll(tallListe);
                    tallListe2 = new List<int>(tallListe);
                    telle = 0;
                }

            }
            else if (e.KeyCode == Keys.Down)
            {

                Console.WriteLine("Down");
                if (tallListe2[0] == 4)
                {
                    tallListe2.RemoveAt(0);
                    NewForm.Controls.Clear();
                    addAll(tallListe2);
                    telle++;
                }
                else
                {
                    addAll(tallListe);
                    tallListe2 = new List<int>(tallListe);
                    telle = 0;
                }

            }

            //Om bruker har greid 4 knappetrykk, legg til 4 nye
            if (telle == 4)
            {
                level++;
                Console.WriteLine($"Telle er: {telle}");
                telle = 0;
                tallListe2 = Pil(4);
                addAll(tallListe2);
                first = false;
                tallListe = new List<int>(tallListe2);
            }

        }

        //Legger til nye bilder
        private void addAll(List<int> t)
        {
            Image i;

            f = new FlowLayoutPanel();

            NewForm.Controls.Clear();


            f.Size = new Size(NewForm.Width, NewForm.Height - 100);
            f.Padding = new Padding(0, 0, NewForm.Width / 4, 0);
            f.BackColor = Color.White;


            for (int y = 0; y < t.Count; y++)
            {
                switch (t[y])
                {
                    case 1:
                        i = new Bitmap("C:\\Users\\Martin\\source\\repos\\DesktopApp1\\left.png");
                        f.Controls.Add(new PictureBox { Image = i, SizeMode = PictureBoxSizeMode.Zoom });
                        break;
                    case 2:
                        i = new Bitmap("C:\\Users\\Martin\\source\\repos\\DesktopApp1\\up.png");
                        f.Controls.Add(new PictureBox { Image = i, SizeMode = PictureBoxSizeMode.Zoom });
                        break;
                    case 3:
                        i = new Bitmap("C:\\Users\\Martin\\source\\repos\\DesktopApp1\\right.png");
                        f.Controls.Add(new PictureBox { Image = i, SizeMode = PictureBoxSizeMode.Zoom });
                        break;
                    case 4:
                        i = new Bitmap("C:\\Users\\Martin\\source\\repos\\DesktopApp1\\down.png");
                        f.Controls.Add(new PictureBox { Image = i, SizeMode = PictureBoxSizeMode.Zoom });
                        break;
                }

                f.Padding = new Padding(50);

                f.Padding = new Padding(200, NewForm.Height / 3, NewForm.Width / 6 - 100, 0);



                NewForm.Controls.Add(f);

            }

            lvlLabel.Text = $"Level: {level}";
            NewForm.Controls.Add(timeLabel);
            NewForm.Controls.Add(lvlLabel);
            NewForm.ResumeLayout(false);
            NewForm.PerformLayout();

        }

    }
}
