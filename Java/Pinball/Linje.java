import javafx.scene.shape.Line;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseDragEvent;

public class Linje extends Line{
    public Linje(double startX, double startY, double endX, double endY, int w){
        super (startX, startY, endX, endY);
        this.setStrokeWidth(w);
    }
}