import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.geometry.Insets;
import javafx.scene.layout.StackPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Button;
import javafx.scene.Group;
import javafx.scene.shape.Rectangle;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.canvas.GraphicsContext;

public class ObligMks extends Application{
    BorderPane pane;
    CenterPane center;
    double xStart, yStart, xEnd, yEnd;

    @Override
    public void start(Stage primaryStage){
        pane = new BorderPane();
        pane.setLeft(leftVBox());
        pane.setCenter(center);
        pane.setRight(rightVBox());
        pane.setBottom(botHBox());

        Scene scene = new Scene(pane, 1000, 800);
        primaryStage.setTitle("Tegneprogram");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static VBox rightVBox(){
        VBox right = new VBox(5);
        right.setPadding(new Insets(5, 50, 5, 25));
        right.setStyle("-fx-border-color: black");
        TextField utTxt = new TextField("Right");
        utTxt.setEditable(false);
        right.getChildren().add(utTxt);
        return right;
    }

    public static VBox leftVBox() {

        VBox left = new VBox(5);
        left.setPadding(new Insets(5, 50, 5, 25));
        left.setStyle("-fx-border-color: black");

        Knapp clear = new Knapp("Slett Tegning");
        Knapp setTxt = new Knapp("Sett inn Tekst");
        Knapp oppLag = new Knapp("Opp et lag");
        Knapp toppLag = new Knapp("Toppen");
        Knapp nedLag = new Knapp("Ned et lag");
        Knapp nederstLag = new Knapp("Nederst");
        Label fyllTxt = new Label("Fyll farge");
        FyllVelger fill = new FyllVelger(Color.WHITE);
        Label strokeTxt = new Label("Strek farge");
        FyllVelger stroke = new FyllVelger(Color.BLACK);
    
        left.getChildren().addAll(clear, setTxt, oppLag, toppLag, nedLag, nederstLag, fyllTxt, fill, strokeTxt, stroke);      
        return left;
    }

    public HBox botHBox() {
        HBox bot = new HBox(15);
        bot.setPadding(new Insets(5, 50, 5, 300));
        bot.setStyle("-fx-border-color: black");
        Rektangel r = new Rektangel(100, 100, Color.BLACK, 1, 1);
                
        Sirkel s = new Sirkel(50, Color.BLACK);
        Linje l = new Linje(100.0f, 0.0f, 100.0f, 100.0f, 5);
        Penta p = new Penta(100.0, 25.0, 200.0, 25.0, 225.0, 75.0, 200.0, 125.0, 100.0, 125.0, 75.0, 75.0);

        r.setOnMouseClicked(e->{
            RekHandler(e);
        });
         
        bot.getChildren().addAll(r, l, s, p);
        
        return bot;
    }

    //Metode for events
    public void RekHandler(MouseEvent e) {           
        center.setOnMousePressed(f -> {
            xStart = f.getX();
            yStart = f.getY();

        });
    
        center.setOnMouseDragged(f->{
            double x = f.getX();
            double y = f.getY();
            xEnd = x;
            yEnd = y;
        });

            final double w = xEnd - xStart;
            final double h = yEnd - yStart;
            Rektangel tmp = new Rektangel( w, h, Color.BLACK, xStart, yStart);          
            center.getChildren().add(tmp);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}