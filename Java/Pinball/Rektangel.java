import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Rektangel extends Rectangle {

    public Rektangel(double w, double h, Color c, double x, double y){
        super (w, h, c);
        this.setX(x);
        this.setY(y);  
    }
}