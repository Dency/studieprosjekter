import javafx.scene.shape.Circle;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.paint.Paint;

public class Sirkel extends Circle{
    public Sirkel(double radius, Paint fill){
        super(radius, fill);
    }
}