//Imports
import javafx.application.Application;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.geometry.Pos;
import java.io.IOException;
import java.util.ArrayList;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import java.io.File;


public class Admapp extends Application {

    //Dekalrerer globale variabler
    private VBox venstreside = new VBox(), senter = new VBox(), kampListe = new VBox();
    private Button regSpiller, regKamp, kamper, lagre, sendKamp, sendSpiller, registrerKode;
    private BorderPane gui;
    private TextField navn, poeng, spillerH, spillerS, kampNr, dato, kommentarTekst, trekkKode;
    private String spillerString = "", vinner = "";
    private TextArea spillerListe = new TextArea();
    private Label skrivKommentar, skrivKode;

    public void start(Stage stage) {

        gui = new BorderPane();
        gui.setLeft(lagVenstre());
        Scene scene = new Scene(gui, 600, 400);

        stage.setOnCloseRequest(event -> {
            //Lagrer backup filer når programmet slutter
            lagreFil();
        });
        stage.setScene(scene);
        stage.setTitle("Admin Applikasjon");
        stage.show();

    }
    //Oppretter venstre siden av GUI 
    public Group lagVenstre() {

        regSpiller = new Button("Registrer spiller");
        regSpiller.setOnAction(e -> {
            lagSpillerMidt();
        });

        regKamp = new Button("Registrer kamp");
        regKamp.setOnAction(e -> {
            lagKampMidt();
        });

        kamper = new Button("Kamper");
        kamper.setOnAction(e -> {
            hentKamper();
        });

        lagre = new Button("Lagre");
        //Legger til lagringsfunksjon som lagrer en tekst-backup for alle filer som eksisterer.
        lagre.setOnAction(e -> {
        lagreFil();
        });
        venstreside.getChildren().addAll(regSpiller, regKamp, kamper, lagre);
        venstreside.setStyle("-fx-border-color: black");
        venstreside.setPrefHeight(400);
        venstreside.setPrefWidth(150);
        venstreside.setAlignment(Pos.CENTER);
        venstreside.setSpacing(80);
        Group venstre = new Group(venstreside);

        spillerListe.setText("");
        spillerListe.setMaxWidth(150);
        spillerListe.setEditable(false);
        
        
        try{
            ArrayList<Spiller> RegSpillere = Filhandtering.lesFil("Spillere.bin");
            for (Spiller s : RegSpillere) {
                spillerString += s.finnSpillere() + "\r\n";
            }

            
        } catch(IOException | ClassNotFoundException e){

        }

        //Oppdaterer høyresiden etterhvert som spillere blir lagt inn
        spillerListe.setText(spillerString);

        gui.setRight(spillerListe);

        return venstre;
    }

    //Lagrer alle filer
    public void lagreFil() {
        try {
            boolean checkKamper = new File("Kamper.bin").exists();
            boolean checkSpillere = new File("Spillere.bin").exists();
            if(checkKamper == true) {
                 Filhandtering.lagreSomTekst("Kamper.bin");
            }
            if(checkSpillere) {
                 Filhandtering.lagreSomTekst("Spillere.bin");
            }
            
            ArrayList<Kamp> kamper = Filhandtering.lesFil("Kamper.bin");
            for (Kamp k : kamper) {
                 boolean checkTrekk = new File(k.getKampNr() + ".bin").exists();
                 if(checkTrekk == true) {
                     Filhandtering.lagreSomTekst(k.getKampNr() + ".bin");
                 }
             }    
         } catch(Exception ex) {
 
         }
    }
    //Oppretter midten av GUI om bruker skal opprette en ny bruker
    public void lagSpillerMidt() {

        Label navnLabel = new Label("Skriv inn navnet til spilleren");
        navn = new TextField();
        navn.setMaxWidth(200);

        Label poengLabel = new Label("Skriv inn poeng. Om spiller ikke har spilt, skriv inn 0");
        poeng = new TextField();
        poeng.setMaxWidth(200);

        sendSpiller = new Button("Registrer spiller");
        sendSpiller.setOnAction(e -> {
            lagSpiller();
        });

        senter.getChildren().clear();
        senter.getChildren().addAll(navnLabel, navn, poengLabel, poeng, sendSpiller);

        senter.setSpacing(20);
        senter.setAlignment(Pos.CENTER);

        gui.setCenter(senter);
    }
    //Lager midten av GUI om bruker skal legge inn en ny kamp
    public void lagKampMidt() {
        Label spillerHLabel = new Label("Skriv inn spilleren med hvite brikker:");
        spillerH = new TextField();
        spillerH.setMaxWidth(200);

        Label spillerSLabel = new Label("Skriv inn spilleren med svarte brikker:");
        spillerS = new TextField();
        spillerS.setMaxWidth(200);

        Label kampNrLabel = new Label("Skriv inn kamp nummer:");
        kampNr = new TextField();
        kampNr.setMaxWidth(200);

        Label datoLabel = new Label("Datoen for kampen:");
        dato = new TextField();

        sendKamp = new Button("Registrer kamp");
        sendKamp.setOnAction(e -> {
            lagKamp();
        });

        senter.getChildren().clear();
        senter.getChildren().addAll(spillerHLabel, spillerH, spillerSLabel, spillerS, kampNrLabel, kampNr, datoLabel,
                dato, sendKamp);
        senter.setSpacing(20);
        senter.setAlignment(Pos.CENTER);
        gui.setCenter(senter);
        
    }

    //Metode for å opprette en ny spiller.
    public void lagSpiller() {
        String spillerNavn = navn.getText();
        double poengsum = Double.parseDouble(poeng.getText());

        
        navn.setText("");
        poeng.setText("");

        //Dette brukes for å kunne telle opppover slik at nye brukere får en egen id,
        //selvom programmet startes på nytt, ved å telle antall spillere fra Spillere.bin.
        try {
            ArrayList<Spiller> spillere = Filhandtering.lesFil("Spillere.bin");
            int telleId = 1;
            for(Spiller s: spillere){
                telleId ++;
            }
            Spiller spiller = new Spiller(telleId, spillerNavn, poengsum);
            
            Filhandtering.skrivTilFil(spiller, "Spillere.bin");
            ArrayList<Spiller> RegSpillere = Filhandtering.lesFil("Spillere.bin");
            spillerString = "";
            for (Spiller s : RegSpillere) {
                
                spillerString += s.finnSpillere() + "\r\n";
            }

        } catch (IOException | ClassNotFoundException e) {

        }
        
        spillerListe.setText(spillerString);
        gui.setRight(spillerListe);
    }
    //finnSpillerId brukes for å finne id'en til en spesefikk bruker. 
    //Metoden brukes for å hente ut de 2 spillerne som blir lagt inn til en kamp.
    public Spiller finnSpillerID(int id) {
        try {
            ArrayList<Spiller> spillere = Filhandtering.lesFil("Spillere.bin");
            for (Spiller s : spillere) {
                if (s.getID() == id) {
                    return s;
                }
            }
            return null;
        } catch (IOException | ClassNotFoundException e) {
        }
        return null;
    }

    //Oppretter kampobjektet 
    public void lagKamp() {
        int spillerHvit = Integer.parseInt(spillerH.getText());
        int spillerSvart = Integer.parseInt(spillerS.getText());

        Spiller hvit = finnSpillerID(spillerHvit);
        Spiller svart = finnSpillerID(spillerSvart);

        Kamp kamp = new Kamp(hvit, svart, Integer.parseInt(kampNr.getText()), dato.getText(), 23.0);

        try {
            Filhandtering.skrivTilFil(kamp, "Kamper.bin");
        } catch (IOException | ClassNotFoundException e) {

        }

        spillerH.setText("");
        spillerS.setText("");
        dato.setText("");
        kampNr.setText("");

    }

    //Metode som oppretter en knapp for hver instanse av kamper innenfor Kamper.bin.
    public void hentKamper() {
        try {
            ArrayList<Kamp> kamper = Filhandtering.lesFil("Kamper.bin");
            kampListe.getChildren().clear();
            for (Kamp k : kamper) {
                

                Button kamp = new Button(k.henteKamp());
                kamp.setOnAction(e ->{
                    regResultat(k);

                });
                
                kampListe.getChildren().add(kamp);
            }

        } catch (IOException | ClassNotFoundException e) {

        }
        gui.setCenter(kampListe);    

    }

    //Brukes for å registrere et resultat fra en kamp
    public void regResultat(Kamp k) {
        VBox resultat = new VBox();
        resultat.setSpacing(20);
        resultat.setAlignment(Pos.CENTER);

        //registrerKode 
        skrivKode = new Label("Skriv inn trekk kode");
        skrivKommentar = new Label("Skriv inn kommentar/eller ingenting");
        trekkKode = new TextField("");
        trekkKode.setMaxWidth(120);
        kommentarTekst = new TextField("");
        kommentarTekst.setMaxWidth(120);
        registrerKode = new Button("Registrer kode");

        registrerKode.setOnAction(e-> {
            String trekk = trekkKode.getText().toString();
            String kommentar = kommentarTekst.getText().toString();
            
            if ( kommentar != "") {
                
                try { 
                    //Brukes for å hele tiden gi et nytt trekk en egen id, slik at 2 trekk ikke får samme id
                    ArrayList<Trekk> antTrekk = Filhandtering.lesFil(k.getKampNr()+ ".bin");
                    int telleTrekk = 1;
                    for (Trekk t: antTrekk ){
                        telleTrekk ++;
                    }

                    Trekk t = new Trekk(k.getKampNr(), telleTrekk, trekk, kommentar);
                    Filhandtering.skrivTilFil(t, k.getKampNr()+".bin");
                }catch(IOException | ClassNotFoundException ex) {
                    
                }
            }else {
                try{
                    ArrayList<Trekk> antTrekk = Filhandtering.lesFil(k.getKampNr()+ ".bin");
                    int telleTrekk = 1;
                    for (Trekk t: antTrekk ){
                        telleTrekk ++;
                    }
                    Trekk t = new Trekk(k.getKampNr(), telleTrekk , trekk);
                    Filhandtering.skrivTilFil(t, "trekk.bin");
                }catch(IOException | ClassNotFoundException ex) {

                }
            }
            
        trekkKode.setText("");
        kommentarTekst.setText("");
        });


        //Registrer resultat
        Label valgLabel = new Label("Velg hvem som vant: ");
        ComboBox<String> valg = new ComboBox<String>();
        valg.getItems().addAll("Hvit", "Svart", "Remis");

        int nr = k.getKampNr();
        
        Button sendResultat = new Button("Registrer resultat");
        sendResultat.setOnAction(e-> {
            vinner = valg.getValue();
            gui.setCenter(kampListe);            
            try{
                ArrayList<Spiller> spillerListe = Filhandtering.lesFil("Spillere.bin");
                int spillerH = k.getSpillerH().getID();
                int spillerS = k.getSpillerS().getID();

                

                for(Spiller sp : spillerListe){
                    if (spillerH == sp.getID()){
                        
                        if (vinner.equals("Hvit")) {
                            double tempPoeng = sp.getPoeng();
                            tempPoeng++;
                            sp.setPoeng(tempPoeng);
                            System.out.println("Poeng hvit: " + tempPoeng);
                            System.out.println("ID : " + sp.getID() + "Poeng for SP : " + sp.getPoeng());
                        } else if (vinner.equals("Remis")) {
                            double tempPoeng = sp.getPoeng();
                            tempPoeng += 0.5;
                            sp.setPoeng(tempPoeng);
                        }
                        
                    } else if (spillerS == sp.getID()){
                        if (vinner.equals("Svart")) {
                            double tempPoeng = sp.getPoeng();
                            tempPoeng++;
                            sp.setPoeng(tempPoeng);
                            
                        } else if (vinner.equals("Remis")) {
                            double tempPoeng = sp.getPoeng();
                            tempPoeng += 0.5;
                            sp.setPoeng(tempPoeng);
                        }
                    }

                    File file = new File("Spillere.bin");
                        if (file.exists()) {
                            file.delete();
                    } else {
                        System.err.println(
                        "Finner ikke '" + file + "' ('" + file.getAbsolutePath() + "')");
                    }
                    for(Spiller s : spillerListe){
                        Filhandtering.skrivTilFil(s, "Spillere.bin");
                    }
                }
                    
                
            } catch (IOException | ClassNotFoundException ex) {

            }
            
        });
        
        resultat.getChildren().addAll(skrivKode, trekkKode, skrivKommentar, kommentarTekst, registrerKode, valgLabel, valg, sendResultat);

        gui.setCenter(resultat);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}