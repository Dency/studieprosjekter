import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

public class Bonde extends Brikke {
    private Image tempBilde;
    private ImageView bilde = new ImageView();

    //Konstruktør
    public Bonde(char farge, int startX, int startY) {
        super(farge, startX, startY);

        //Bestemmer type farge bonden skal ha
        if (farge == 'h') {
            tempBilde = new Image("bilder/hBonde.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        } else if (farge == 's') {
            tempBilde = new Image("bilder/sBonde.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        }
        //Legger til bildet som en child node på objektet Bonde
        this.getChildren().add(bilde);
    }

    //Overrider superklassen Brikke. Sjekker om trekk er generelt lovlig.
    //Returnerer flase om ikke lov og true om det er lov
    @Override
    public boolean lovligTrekk(int startX, int startY, int endX, int endY) {

        if (farge == 's') {
            if ((startX + startY) - (endX + endY) <= 2 || (startX + startY) - (endX + endY) >= 0) {
                return true;
            } else {
                return false;
            }
        } else if ((startX + startY) - (endX + endY) <= -2 || (startX + startY) - (endX + endY) >= 0) {
            if (endY < startY - 1) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

}