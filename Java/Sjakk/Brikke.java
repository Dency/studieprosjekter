import javafx.scene.layout.Pane;

//Superklassen Brikke 
//Extender Pane slik at alle subklasser blir noder
public abstract class Brikke extends Pane {

    protected char farge;
    protected int startX, startY;

    //Konstruktør
    public Brikke(char farge, int startX, int startY) {
        this.farge = farge;
        this.startX = startX;
        this.startY = startY;
    }

    //GET og SET metoder
    public char getFarge(){
        return this.farge;
    }

    public void setFarge (char farge) {
        this.farge = farge;
    }

    public int getStartX(){
        return this.startX;
    }

    public void setStartX (int startX) {
        this.startX = startX;
    }

    public int getStartY(){
        return this.startY;
    }

    public void setStartY (int startY) {
        this.startY = startY;
    }

    public boolean lovligTrekk(int startX, int startY, int endX, int endY){
        if(endX > 0 && endX<= 7 && endY > 0 && endY <= 7){
            return true;
        }
        return false;
    }
}