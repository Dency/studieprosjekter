import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

//Subklasse av Brikke
public class Dronning extends Brikke{
    private Image tempBilde;
    private ImageView bilde = new ImageView();
    //Konstruktør
    public Dronning(char farge, int startX, int startY) {
            super(farge, startX, startY);

        if (farge == 'h'){
            tempBilde = new Image("bilder/hDronning.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        } else {
            tempBilde = new Image("bilder/sDronning.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        }
        //Legger til bildet på noden
        this.getChildren().add(bilde);
    }

    //Sjekker lovlige trekk
    public boolean lovligTrekk(int startX, int startY, int endX, int endY){
        if(startX != endX || startY != endY){
            return true;
        } else if (startX == endX - 1 && startY == endY || startX + 1 == endX && startY == endY){
            return true;
        } else if (startX == endX && startY == endY + 1 || startX == endX && startY == endY - 1){
            return true;
        } else if (startX == endX + 1 && startY == endY - 1 || startX == endX + 1 && startY == endY + 1 || startX == endX - 1 && startY == endY + 1 || startX == endX - 1 && startY == endY - 1) {
            return true;
        } else if(startX != endX && startY == endY || startY != endY && startX == endX){
            return true;
        } else {
            return false;
        }
    }
}