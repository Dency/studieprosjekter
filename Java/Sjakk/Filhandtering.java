import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.io.FileWriter;

public class Filhandtering {
    // Skriver inn et nytt objet på slutten av en ArrayList og lagrer listen til
    // fil.
    public static <T> void skrivTilFil(T s, String filnavn) throws IOException, ClassNotFoundException {
        ArrayList<T> liste = lesFil(filnavn);
        liste.add(s);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(filnavn));
        objectOutputStream.writeObject(liste);
        objectOutputStream.close();
    }

    // Henter ut ArrayListen som er lagret på fil.
    @SuppressWarnings("unchecked")
    public static <T> ArrayList<T> lesFil(String filnavn) throws IOException, ClassNotFoundException {
        try {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(filnavn));
            Object o = input.readObject();
            input.close();
            if (o instanceof ArrayList<?>) {
                return (ArrayList<T>) o;
            } else {
                return new ArrayList<>();
            }
        } catch (FileNotFoundException e) {
            return new ArrayList<T>();
        }
    }

    // Lagrer en backup fil.
    public static <T> void lagreSomTekst(String filnavn) throws IOException, ClassNotFoundException {
        FileWriter fileWriter = new FileWriter("backup" + filnavn);
        ArrayList<T> arrayList = lesFil(filnavn);
        for (T v : arrayList) {
            fileWriter.write(v.toString());
            fileWriter.write("\n");
        }
        fileWriter.close();
    }

}