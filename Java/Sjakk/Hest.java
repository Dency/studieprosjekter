import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

//Subklasse av brikke
public class Hest extends Brikke {
    private Image tempBilde;
    private ImageView bilde = new ImageView();
    //Konstruktør
    public Hest(char farge, int startX, int startY) {
        super(farge, startX, startY);

        if (farge == 'h') {
            tempBilde = new Image("bilder/hHest.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        } else if (farge == 's') {
            tempBilde = new Image("bilder/sHest.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        }
        //Legger til bilde som en child på noden
        this.getChildren().add(bilde);
    }

    //Sjekker om trekket er lovlig
    public boolean lovligTrekk(int startX, int startY, int endX, int endY){
        if (startX == endX +1 && startY == endY +2 || startX == endX-1 &&startY == endY -2){
            return true;
        } else if (startX == endX -2 && startY == endY +1 || startX == endX -2 && startY == endY-1){
            return true;
        } else if (startX == endX + 1 && startY == endY +2 || startX == endX -1 && startY == endY +2){
            return true;
        } else if (startX == endX +2 && startY == endY + 1 || startX == endX +2 & startY == endY -1){
            return true;
        }
        return false;
    }
}