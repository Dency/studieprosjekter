import java.io.Serializable;

// Klasse for å holde på kampobjekter.
public class Kamp implements Serializable {

    private static final long serialVersionUID = 1L;
    private int kampNr;
    private String dato;
    private Spiller spillerH, spillerS;
    private double tid;

    // Konstruktører.
    public Kamp(Spiller spillerH, Spiller spillerS, int kampNr, String dato, double tid) {
        this.spillerH = spillerH;
        this.spillerS = spillerS;
        this.kampNr = kampNr;
        this.dato = dato;
        this.tid = tid;
    }

    public Kamp(int kampNr2, Spiller spillerH2, Spiller spillerS2) {
    }

    public Kamp(int kampNr2) {
    }

    // Set og get metoder.
    public void setSpillerH(Spiller spillerH) {
        this.spillerH = spillerH;
    }

    public Spiller getSpillerH() {
        return spillerH;
    }

    public void setSpillerS(Spiller spillerS) {
        this.spillerS = spillerS;
    }

    public Spiller getSpillerS() {
        return spillerS;
    }

    public void setKampNr(int kampNr) {
        this.kampNr = kampNr;
    }

    public int getKampNr() {
        return kampNr;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public String getDato() {
        return dato;
    }

    public double getTid() {
        return this.tid;
    }

    public void setTid(double tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        String kamp = "Kamp nummer: " + kampNr + " | " + spillerH.getNavn() + " VS. " + spillerS.getNavn() + " | Dato: "
                + dato + " | Tid:" + tid;
        return kamp;
    }

    // Alternativ toString metode.
    public String henteKamp() {
        String kamper = "Kamp nummer: " + kampNr + " Hvit: " + spillerH.getNavn() + " VS Svart: " + spillerS.getNavn();
        return kamper;
    }
}