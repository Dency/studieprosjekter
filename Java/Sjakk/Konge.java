import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

//Subklasse av Brikke
public class Konge extends Brikke{
    private Image tempBilde;
    private ImageView bilde = new ImageView();
    
    //Konstruktør
    public Konge(char farge, int startX, int startY) {
            super(farge, startX, startY);

        if (farge == 'h'){
            tempBilde = new Image("bilder/hKonge.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        } else {
            tempBilde = new Image("bilder/sKonge.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        }
        
        //Legger til bilde som child på noden
        this.getChildren().add(bilde);
    }
    
    //Sjekker om trekket er lovlig for denne typen brikke
    public boolean lovligTrekk(int startX, int startY, int endX, int endY){
        if (startX == endX - 1 && startY == endY || startX + 1 == endX && startY == endY){
            return true;
        } else if (startX == endX && startY == endY + 1 || startX == endX && startY == endY - 1){
            return true;
        } else if (startX == endX + 1 && startY == endY - 1 || startX == endX + 1 && startY == endY + 1 || startX == endX - 1 && startY == endY + 1 || startX == endX - 1 && startY == endY - 1) {
            return true;
        }
        return false;
    }
}