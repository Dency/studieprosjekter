import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

//Subklasse av Brikke
public class Loper extends Brikke {
    private Image tempBilde;
    private ImageView bilde = new ImageView();

    public Loper(char farge, int startX, int startY) {
        super(farge, startX, startY);

        //Bestemmer farge på brikke
        if (farge == 'h') {
            tempBilde = new Image("bilder/hLoper.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65); 
        } else if (farge == 's') {
            tempBilde = new Image("bilder/sLoper.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        }
        //Legger til bilde som child på Loper objektet
        this.getChildren().add(bilde);
    }

    //Sjekker om trekket er lovlig
    @Override
    public boolean lovligTrekk(int startX, int startY, int endX, int endY){
        if(startX == endX || startY == endY){
            return false;
        }
        return true;
    }
}