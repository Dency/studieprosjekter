//Subklasse av Kamp
public class Resultat extends Kamp{

    Spiller spillerH, spillerS;
    String resultat;
    int kampNr;
    double poengH, poengS;
//Konstruktør
    public Resultat(int kampNr, Spiller spillerH, Spiller spillerS, String resultat){
        super(kampNr, spillerH, spillerS);
        this.resultat = resultat;
        //Sjekker hva resultatet er og gir ut poeng til riktig Spiller objekt
        if (resultat == "Remis"){
            poengH = spillerH.getPoeng();
            poengS = spillerS.getPoeng();
            spillerH.setPoeng(poengH += 0.5);
            spillerS.setPoeng(poengS += 0.5);
        }else if (resultat == "Svart"){
            poengS = spillerS.getPoeng();
            spillerS.setPoeng(poengS += 1);
        }else if (resultat == "Hvit"){
            poengH = spillerH.getPoeng();
            spillerH.setPoeng(poengH += 1);
        }
    }
    //SET- og GET-metoder
    public void setResultat(String resultat){
        this.resultat = resultat;
    }

    public String getResultat(){
        return this.resultat;
    }

    //Egen toString metode som overskriver Kamp sin toString metode.
    @Override
    public String toString(){
        return resultat;
    }
}