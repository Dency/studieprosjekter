import java.io.Serializable;
import java.util.ArrayList;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.File;
import java.lang.Comparable;

public class Spiller implements Serializable, Comparable<Spiller>{
    
    private String navn;
    private double poeng;
    private int id;

    public Spiller(int id, String navn, double poeng) {
        this.id = id;
        this.navn = navn;
        this.poeng = poeng;
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getID() {
        return id;
    }

    @Override
    public String toString() {
        return "id=" + id + " : " + navn + " : " + poeng;
    }

    public String finnSpillere(){
        return id + " : " + navn;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public double getPoeng() {
        return poeng;
    }

    public void setPoeng(double poeng) {
        this.poeng = poeng;
    }

    @Override
    public int compareTo(Spiller compareSpiller) {
        int comparePoeng=(int)((Spiller)compareSpiller).getPoeng();
        /* For Ascending order*/
        return comparePoeng-(int)this.poeng;

}

}
