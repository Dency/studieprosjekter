
//Imports
import javafx.scene.control.SelectionMode;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.control.TextField;
import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.geometry.Pos;
import java.util.ArrayList;
import java.io.IOException;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import java.util.*;

public class Spillerapp extends Application {
    private String text = "";
    private VBox nav;
    private BorderPane brukerGUI;
    private Button rangering, sokeknapp, velgKamp, velgTrekk, nesteTrekk, forigeTrekk;
    private TextField sokefelt;
    private TextArea liste;
    private GridPane root = new GridPane();
    final int size = 8;
    private ListView<Kamp> listView = new ListView<Kamp>();
    private ListView<Trekk> listTrekk = new ListView<Trekk>();
    private ArrayList<Kamp> resultat = new ArrayList<Kamp>();
    private ArrayList<Kamp> sokKamp;
    private ObservableList<Kamp> kamper;
    private ObservableList<Trekk> obsTrekk;
    private ArrayList<Trekk> trekkListe;
    private int trekkNr = 0;
    private ArrayList<Brikke> brikker;
    private Label sokefeltLabel;

    public void start(Stage stage) {

        brukerGUI = new BorderPane();

        brukerGUI.setLeft(venstreMeny());
        brukerGUI.setRight(trekkMeny());

        brukerGUI.setCenter(root);

        Scene scene = new Scene(brukerGUI, 1100, 600);

        stage.setScene(scene);
        stage.setTitle("Spiller applikasjon");
        stage.show();

    }

    // Sletter alle brikker.
    public void slettBrikker() {
        root.getChildren().clear();
    }

    // Plasserer brett og brikker i gridpane.
    public void plasserBrikker() {
        brikker = new ArrayList<Brikke>();
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                Rectangle square = new Rectangle();
                Color color;
                if ((row + col) % 2 == 0)
                    color = Color.WHITE;
                else
                    color = Color.GREY;
                square.setFill(color);
                root.add(square, col, row);
                square.widthProperty().bind(root.widthProperty().divide(size));
                square.heightProperty().bind(root.heightProperty().divide(size));
            }
        }

        // Alle for-loopene legger brikkene på brettet og inn i ArrayList.

        for (int i = 0; i < 8; i++) {
            Bonde sBonde = new Bonde('s', i, 1);
            root.setConstraints(sBonde, i, 1);
            root.getChildren().add(sBonde);
            brikker.add(sBonde);
        }

        for (int i = 0; i < 8; i++) {
            Bonde hBonde = new Bonde('h', i, 6);
            root.setConstraints(hBonde, i, hBonde.getStartY());
            root.getChildren().add(hBonde);
            brikker.add(hBonde);
        }

        for (int st = 0; st < 8; st += 7) {
            Tarn sTarn = new Tarn('s', st, 0);
            root.setConstraints(sTarn, st, sTarn.getStartY());
            root.getChildren().add(sTarn);
            brikker.add(sTarn);
        }

        for (int ht = 0; ht < 8; ht += 7) {
            Tarn hTarn = new Tarn('h', ht, 7);
            root.setConstraints(hTarn, ht, hTarn.getStartY());
            root.getChildren().add(hTarn);
            brikker.add(hTarn);
        }

        for (int sh = 1; sh < 7; sh += 5) {
            Hest sHest = new Hest('s', sh, 0);
            root.setConstraints(sHest, sh, sHest.getStartY());
            root.getChildren().add(sHest);
            brikker.add(sHest);
        }

        for (int hh = 1; hh < 7; hh += 5) {
            Hest hHest = new Hest('h', hh, 7);
            root.setConstraints(hHest, hh, hHest.getStartY());
            root.getChildren().add(hHest);
            brikker.add(hHest);
        }

        for (int hl = 2; hl < 6; hl += 3) {
            Loper hLoper = new Loper('h', hl, 7);
            root.setConstraints(hLoper, hl, hLoper.getStartY());
            root.getChildren().add(hLoper);
            brikker.add(hLoper);
        }

        for (int sl = 2; sl < 6; sl += 3) {
            Loper sLoper = new Loper('s', sl, 0);
            root.setConstraints(sLoper, sl, sLoper.getStartY());
            root.getChildren().add(sLoper);
            brikker.add(sLoper);
        }

        Dronning sDronning = new Dronning('s', 3, 0);
        root.setConstraints(sDronning, sDronning.getStartX(), sDronning.getStartY());
        brikker.add(sDronning);

        Dronning hDronning = new Dronning('h', 3, 7);
        root.setConstraints(hDronning, hDronning.getStartX(), hDronning.getStartY());
        brikker.add(hDronning);

        Konge hKonge = new Konge('h', 4, 7);
        root.setConstraints(hKonge, hKonge.getStartX(), hKonge.getStartY());
        brikker.add(hKonge);

        Konge sKonge = new Konge('s', 4, 0);
        root.setConstraints(sKonge, sKonge.getStartX(), sKonge.getStartY());
        brikker.add(sKonge);

        root.getChildren().addAll(sDronning, hDronning, sKonge, hKonge);

    }

    // flytter alle trekkene i listen.
    public void flyttAlleBrikker(ArrayList<Integer[]> liste) {
        for (Integer[] k : liste) {
            flyttBrikke(k);
        }
    }

    // flytter ett trekk tilbake ved å slette alle brikkene, legger de i
    // startposisjonen og kjører gjennom trekkene frem til forrige trekk.
    public void flyttTilbakeKnapp() {
        slettBrikker();
        plasserBrikker();
        if (trekkNr > 0)
            trekkNr--;
        ArrayList<Integer[]> intlist = trekkTilKoordinat(hentTrekk(hentKampnummer()));
        for (int i = 0; i < trekkNr; i++) {
            flyttBrikke(intlist.get(i));
        }
    }

    // legger funksjon til Flytte-knappen.
    public void flyttBrikkeKnapp() {
        ArrayList<Integer[]> intlist = trekkTilKoordinat(hentTrekk(hentKampnummer()));
        if (trekkNr < intlist.size()) {
            flyttBrikke(intlist.get(trekkNr));
            trekkNr++;
        }
    }

    // flytter en brikke.
    public void flyttBrikke(Integer[] k) {
        int sX = k[0];
        int sY = k[1];
        int eX = k[2];
        int eY = k[3];

        // sletter brikken som ligger på plassen hvor brikken blir flyttet.
        for (Brikke fjernBrikke : brikker) {
            if (fjernBrikke.getStartX() == eX && fjernBrikke.getStartY() == eY) {
                root.getChildren().remove(fjernBrikke);
            }
        }

        // henter ut riktig brikke og flytter den til ny posisjon. har valgt å ikke ha
        // med lovligTrekk, siden den ikke er 100% fuksjonell,
        // men deler av den fungerer.
        for (Brikke brikke : brikker) {
            if (brikke.getStartX() == sX && brikke.getStartY() == sY) {
                // if (brikke.lovligTrekk(sX, sY, eX, eY)) {
                brikke.setStartX(eX);
                brikke.setStartY(eY);
                root.setConstraints(brikke, brikke.getStartX(), brikke.getStartY());
                // }
            }
        }

    }

    // Menyen på venstre side.
    public Group venstreMeny() {
        sokefeltLabel = new Label("Skriv inn spillernavn.");
        sokefelt = new TextField();
        sokefelt.setMaxWidth(250);
        sokeknapp = new Button("Finn kamp");
        sokeknapp.setOnAction(e -> {

            brukerGUI.setRight(kampListe());

        });
        rangering = new Button("Rangering");
        rangering.setOnAction(e -> {
            brukerGUI.setRight(rangeringListe());
        });
        nesteTrekk = new Button("Neste trekk");
        nesteTrekk.setOnAction(e -> {
            flyttBrikkeKnapp();
        });
        forigeTrekk = new Button("Forrige trekk");
        forigeTrekk.setOnAction(e -> {
            flyttTilbakeKnapp();
        });
        nav = new VBox();
        nav.getChildren().addAll(sokefeltLabel, sokefelt, sokeknapp, rangering, nesteTrekk, forigeTrekk);
        nav.setPrefHeight(600);
        nav.setPrefWidth(250);
        nav.setStyle("-fx-border-color: black");
        nav.setSpacing(20);
        nav.setAlignment(Pos.CENTER);

        Group navbar = new Group();
        navbar.getChildren().add(nav);

        return navbar;
    }
    //Går igjennom alle kamper, og sender ut alle kamper hvor en spiller har samme navn som søkefelt til ListView
    public VBox kampListe() {
        listView.getItems().clear();
        listView.getSelectionModel().clearSelection();
        resultat = new ArrayList<Kamp>();
        try {
            sokKamp = Filhandtering.lesFil("Kamper.bin");
            Iterator<Kamp> iter = sokKamp.iterator();
            while (iter.hasNext()) {
                Kamp kamp = iter.next();
                Spiller spillerH = kamp.getSpillerH();
                Spiller spillerS = kamp.getSpillerS();
                if (spillerH.getNavn().equals(sokefelt.getText())) {
                    System.out.println(kamp.toString());
                    resultat.add(kamp);
                } else if (spillerS.getNavn().equals(sokefelt.getText())) {
                    System.out.println(kamp.toString());
                    resultat.add(kamp);
                }
            }

            kamper = FXCollections.observableArrayList(resultat);
            listView.getItems().addAll((kamper));
            listView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        } catch (IOException | ClassNotFoundException ex) {

        }
        //Returnerer VBox med ListView av alle kampene
        VBox kampListe = new VBox();
        velgKamp = new Button("Velg kamp");
        velgKamp.setOnAction(e -> buttonClicked());
        kampListe.getChildren().addAll(listView, velgKamp);
        kampListe.setPrefHeight(600);
        kampListe.setPrefWidth(250);
        kampListe.setStyle("-fx-border-color: black");
        kampListe.setSpacing(20);
        kampListe.setAlignment(Pos.CENTER);

        return kampListe;

    }
    //setter høyresiden til å hente trekkListe
    public void buttonClicked() {
        ObservableList<Kamp> valgtKamp;
        valgtKamp = listView.getSelectionModel().getSelectedItems();
        for (Kamp k : valgtKamp) {
            brukerGUI.setRight(trekkListe(k));
        }
        trekkNr = 0;
        plasserBrikker();
    }

    public int hentKampnummer() {
        int kampNummer = 0;
        ObservableList<Kamp> valgtKamp;
        valgtKamp = listView.getSelectionModel().getSelectedItems();
        for (Kamp k : valgtKamp) {
            kampNummer = k.getKampNr();
        }

        return kampNummer;
    }
    //Returnerer VBox med ListView av alle trekkene gjort i Kamp k
    public VBox trekkListe(Kamp k) {
        listTrekk.getItems().clear();
        listTrekk.getSelectionModel().clearSelection();

        try {
            trekkListe = Filhandtering.lesFil(k.getKampNr() + ".bin");
        } catch (IOException | ClassNotFoundException ex) {

        }
        obsTrekk = FXCollections.observableArrayList(trekkListe);
        listTrekk.getItems().addAll((obsTrekk));
        listTrekk.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        VBox trekkListe = new VBox();

        velgTrekk = new Button("Velg trekk");
        trekkListe.getChildren().addAll(listTrekk, velgTrekk);
        trekkListe.setPrefHeight(600);
        trekkListe.setPrefWidth(250);
        trekkListe.setStyle("-fx-border-color: black");
        trekkListe.setSpacing(20);
        trekkListe.setAlignment(Pos.CENTER);

        return trekkListe;
    }

    // Viser en rangeringsliste
    public VBox rangeringListe() {
        liste = new TextArea("");
        text = "";
        try {
            ArrayList<Spiller> liste = Filhandtering.lesFil("spillere.bin");
            sorterSpillere(liste);
            for (Spiller i : liste) {
                text += i.toString() + "\n";
            }
        } catch (IOException | ClassNotFoundException e) {

        }
        liste.setText(text);
        liste.setEditable(false);

        VBox trekkListe = new VBox();

        trekkListe.getChildren().addAll(liste);
        trekkListe.setPrefHeight(600);
        trekkListe.setPrefWidth(250);
        trekkListe.setStyle("-fx-border-color: black");
        trekkListe.setSpacing(20);
        trekkListe.setAlignment(Pos.CENTER);

        return trekkListe;
    }

    // Oppretter en meny for å vise trekk.
    public Group trekkMeny() {

        VBox trekkListe = new VBox();
        trekkListe.setPrefHeight(600);
        trekkListe.setPrefWidth(250);
        trekkListe.setStyle("-fx-border-color: black");
        trekkListe.setSpacing(20);
        trekkListe.setAlignment(Pos.CENTER);

        Group trekk = new Group();
        trekk.getChildren().add(trekkListe);
        return trekk;
    }

    // Henter ut trekkliste-arrayen fra fila.
    public ArrayList<Trekk> hentTrekk(int kamp) {
        ArrayList<Trekk> trekkliste = null;
        try {
            trekkliste = Filhandtering.lesFil(kamp + ".bin");
        } catch (IOException | ClassNotFoundException e) {
        }
        return trekkliste;
    }

    // Gjør om et trekk(a2a4) til koordinater[0, 6, 0, 4], og speilvendt pga hvit er
    // nederst.
    public ArrayList<Integer[]> trekkTilKoordinat(ArrayList<Trekk> trekkliste) {
        ArrayList<Integer[]> koordinatliste = new ArrayList<Integer[]>();

        for (Trekk trekk : trekkliste) {
            String tKode = trekk.getTrekkKode(); // a1b2

            char caX = tKode.charAt(0);
            int aX = Character.toUpperCase(caX) - 64; // a=1

            char caY = tKode.charAt(1);
            int aY = Character.getNumericValue(caY); //

            char cbX = tKode.charAt(2);
            int bX = Character.toUpperCase(cbX) - 64;

            char cbY = tKode.charAt(3);
            int bY = Character.getNumericValue(cbY);

            Integer[] fraTil = new Integer[4];
            fraTil[0] = aX - 1;
            fraTil[1] = 8 - aY;
            fraTil[2] = bX - 1;
            fraTil[3] = 8 - bY;

            koordinatliste.add(fraTil);

        }
        return koordinatliste;
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

    // Metode for sortering av spillere
    public static ArrayList<Spiller> sorterSpillere(ArrayList<Spiller> s) {

        Collections.sort(s);
        return s;
    }
}
