import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

//Subklasse av Brikke
public class Tarn extends Brikke{
    private Image tempBilde;
    private ImageView bilde = new ImageView();
    
    //Konstruktør 
    public Tarn(char farge, int startX, int startY) {
            super(farge, startX, startY);

        if (farge == 'h'){
            tempBilde = new Image("bilder/hTaarn.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        } else {
            tempBilde = new Image("bilder/sTaarn.png");
            bilde.setImage(tempBilde);
            bilde.setFitHeight(65);
            bilde.setFitWidth(65);
        }

        //Legger til bilde som child på noden
        this.getChildren().add(bilde);
    }
    
    //Sjekker om trekket er lovlig for Tårnet
    public boolean lovligTrekk(int startX, int startY, int endX, int endY){
        if(startX != endX && startY == endY || startY != endY && startX == endX){
            return true;
        } else {
            return false;
        }
    }
}