//Sub-klasse av Kamp
public class Trekk extends Kamp {

    private int trekkNr;
    private String trekkKode, kommentar;

    //konstruktører
    public Trekk(int kampNr, int trekkNr, String trekkKode, String kommentar) {
        super(kampNr);
        this.trekkNr = trekkNr;
        this.trekkKode = trekkKode;
        this.kommentar = kommentar;
    }

    public Trekk(int kampNr, int trekkNr, String trekkKode) {
        super(kampNr);
        this.trekkNr = trekkNr;
        this.trekkKode = trekkKode;
    }

    //Set- og Get-metoder

    public int getTrekkNr() {
        return trekkNr;
    }

    public void setTrekkNr(int trekkNr) {
        this.trekkNr = trekkNr;
    }

    public String getTrekkKode() {
        return trekkKode;
    }

    public void setTrekkKode(String trekkKode) {
        this.trekkKode = trekkKode;
    }

    public String getKommentar() {
        return kommentar;
    }

    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    //toString metode
    @Override
    public String toString() {
        String trekk = trekkNr + ": " + trekkKode + "(" + kommentar + ")";
        return trekk;
    }
}