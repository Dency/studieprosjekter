# TODO: README

## Sett innstillinger i GIT

```
git config --global user.name "Ditt navn"

git config --global user.email "Din e-mail"
```

## Clone repo / klone repo - laste ned

```
git clone git@gitlab.com:DITTBRUKERNAVN/project-gis.git, linker finner du inne på repo homepage
```

## Pulle change / laste ned changes fra repo

```
git pull 
```

## Pushe changes / laste opp lokale changes til repo

```
git add FILNAVN (. for alle filer)

2. git status for å se hvilke filer som pushes

3. git commit -m "Melding" for kommentar

git push for å pushe til repo
```

## Stashe changes (kan ikke pulle pga lokalt, lagre lokale endringer skjult)

```
git stash for å lagre lokalt men stashe alt av endringer

git stash pop for å hente tilbake lokale endringer
```

## Bytte branch (devel for mesteparten av endringer, master for endringer man vil beholde!)

```
git checkout branch_name (f.eks: git checkout master for master, git checkout devel for devel)
```

## Merge changes (slå sammen 2 branches)


**Du må være i branchen du vil MERGE med en anna branch, branchen du er i blir oppdatert!**

**Er du i master, og skal hente alt av endringer fra devel skriver du:**

```
git merge devel
```

## Sette opp XAMPP med Git

**Først åpne** ``C:\xampp\apache\conf\extra\httpd-vhosts.conf``
**Legg til følgende:**
```
<VirtualHost *:80>
    DocumentRoot C:\Users\sondr\git\project-gis
    ServerName projectgis.local
    <Directory C:\Users\sondr\git\project-gis>
        Require all granted
    </Directory>
</VirtualHost>
```
**Du bytter ut DocumentRoot og Directory med din egen lokale directory, deretter må du sette opp hosts filen.**
**Du finner hosts filen i "C:/Windows/System32/drivers/etc/hosts, legg til følgende på slutten av filen: ``127.0.0.1 projectgis.local``**
**Deretter restarter du apache, og du skal kunne nå siden på ``http://projectgis.local``**

## Ha filer i repo mappe som ignoreres
**Det er forsovet ikke vanlig å ha mapper slik som dette, men det kan gjøre det enklere dersom dere har kode dere vil teste og også unngå å pushe. Da kan dere lage en mappe som heter ``PERSONAL_IGNORE``, og så deretter legge filer inn i denne. Denne mappen vil bli ignorert helt av git (se .gitignore)** 

## PERMISSIONS 
**For øyeblikket, bruker vi følgende permissions:**
```Manage = level 1: Redigere slette courses/announcements etc
Owner = level 1337: Redigere brukere osv```

