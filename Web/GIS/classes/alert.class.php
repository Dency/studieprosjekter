<?php
/**
 * Vis en HTML alert div
 * 
 * Denne klassen tillater deg å vise advarsler laget i HTML. Fire typer er støttet (BS4): info, alert-success, warning og danger.
 * En advarsler kan bli initialisert ved bruk av new Alert() eller de fire alternative metodene Alert::info, Alert::success osv. 
 * Disse vil returnere html koden.
 */

class Alert
{
    private $type;
    private $title;
    private $message;
    private $classes;
    private static $allowed_types = ["success", "info", "warning", "danger"];

    /**
     * Opprett en advarsel
     * 
     * @param string $title
     * @param string $message
     * @param string $type Tillate verdier: success, info, warning, danger
     */
    public function __construct($title = NULL, $message = NULL, $type = NULL)
    {
        $this->title = $title;
        $this->message = $message;
        $this->classes = "";
        
        $type = strtolower($type);

        if (in_array($type, self::$allowed_types))
            $this->type = $type;
        else
            $this->type = "warning";   
    }

    // Setters
    /**
     * Set tittellen til advarslen
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Sett meldingen til advarslen
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Sett typen til advarslen
     * @param string $type Lovlige typer: success, info, warning, danger
     */
    public function setType($type)
    {
        $type = strtolower($type);
        if (in_array($type, self::$allowed_types))
            $this->type = $type;
        else
            $this->type = "warning";
        
        return $this;
    }

    /**
     * Get typen til advarslen
     * @return Alert advarslen
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set klassen(e) of advarslens parent div
     * @param string $class
     */
    public function setClass($class)
    {
        $this->classes = $class;
        return $this;
    }

    /**
     * Legg til en klasse til advarslens parent div
     * @param string $class
     */
    public function addClass($class)
    {
        $this->classes .= " " . $class;
        return $this;
    }

    /**
     * Returner advarslen som HTML
     * @return string
     */
    public function show()
    {
        if ($this->title === null && $this->message === null)
            return "";

        switch(self::getType())
        {
            case "warning": { $this->addClass("alert-warning"); break; }
            case "danger": { $this->addClass("alert-danger"); break; }
            case "info": { $this->addClass("alert-info"); break; }
            case "success": { $this->addClass("alert-success"); break; }
            default: {$this->addClass("alert-warning"); break; }
        }
        $this->addClass("alert");

        $html = "<div class='col-sm-12'><div style='margin-bottom: 10px; margin-top: 10px;' class='{$this->classes}'>";

        if (!empty($this->title))
            $html .= "<strong>" . htmlentities($this->title) . "</strong><br><br>";
            
        $html .= "{$this->message}</div></div>";

        return $html;
    }

    /**
     * Lag og returner en advarende advarsel
     * @param string $message
     * @param string $class
     * @return string html 
     */
    public static function warning($message, $class = "")
    {
        $alert = new Alert(null, $message, "warning");
        $alert->setType("warning");

        if (strlen($class))
            $alert->addClass($class);
        
        return $alert->show();
    }

    /**
     * Lag og returner en fare advarsel
     * @param string $message
     * @param string $class
     * @return string html 
     */
    public static function danger($message, $class = "")
    {
        $alert = new Alert(null, $message, "danger");
        $alert->setType("danger");

        if (strlen($class))
            $alert->addClass($class);
        
        return $alert->show();
    }

    /**
     * Lag og returner en informasjon advarsel
     * @param string $message
     * @param string $class
     * @return string html 
     */
    public static function info($message, $class = "")
    {
        $alert = new Alert(null, $message, "info");
        $alert->setType("info");

        if (strlen($class))
            $alert->addClass($class);
            
        return $alert->show();
    }

    /**
     * Lag og returner en suksess advarsel
     * @param string $message
     * @param string $class
     * @return string html 
     */
    public static function success($message, $class = "")
    {
        $alert = new Alert(null, $message, "success");
        $alert->setType("success");

        if (strlen($class))
            $alert->addClass($class);
        
        return $alert->show();
    }
}