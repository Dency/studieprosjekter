<?php

class Auth
{
    private static $user = null;
    
    /**
     * Prøv å autensiere en bruker med et brukernavn og passord
     * @param Array $credentials Et array som inneholder brukernavn, passord, og keep_logged_in
     * 
     *  For eksempel: $crendetials = [
     *  "username"          =>  "navn",
     *  "password"          => "passord",
     *  "keep_logged_in"    => true
     * ];
     * Array er brukt for å forhindre at passordet leakes i tilfelle exception.
     * @return boolean True om suksesfult, false hvis ikke
     */
    public static function attempt(Array $credentials)
    {
        self::$user = null;
    
        if (!array_key_exists("username", $credentials))
            throw new InvalidArgumentException("Brukernavnet må oppgis!");
        
        if (!array_key_exists("password", $credentials))
            throw new InvalidArgumentException("Passordet må oppgis!");

        if ($user_id = self::validate($credentials))
        {
            $keep_logged_in = array_key_exists("keep_logged_in", $credentials) ? (bool)$credentials["keep_logged_in"] : false;

            self::$user = new User($user_id);

            $session_id = Random::generateSalt(64);
            
            $query_create_session = 
            "INSERT INTO `user_sessions` (`user_id`, `session_id`, `ip`, `login_time`)
                VALUES
                        (:user_id, :session_id, :ip, NOW())";

            $statement = Db::getPdo()->prepare($query_create_session);
            $statement->execute([
                ":user_id"          => $user_id,
                ":session_id"       => $session_id,
                ":ip"               => $_SERVER['REMOTE_ADDR']
            ]);
            
            setcookie(
                Config::get("cookie.session"), 
                $session_id, 
                ($keep_logged_in ? time() + 3600 * 24 * 21 : 0),
                Config::get("cookie.session.path")
            );

            return true;
        }    

        return false;
    }

    /**
     * Prøv å autensiere en bruker med brukernavn og passord uten å faktisk logge inn.
     * 
     * @param Array $credentials Et array inneholdende brukernavn, passord og key keep_logged_in likt attempt()
     *  For eksempel: $crendetials = [
     *  "username"          =>  "navn",
     *  "password"          => "passord",
     *  "keep_logged_in"    => true
     * ];
     * 
     * @return integer Bruker IDen til brukeren, eller false om credentials ikke stemmer
     */
    public static function validate(Array $credentials)
    {
        if (!array_key_exists("username", $credentials))
            throw new InvalidArgumentException("Brukernavnet må oppgis!");
        
        if (!array_key_exists("password", $credentials))
            throw new InvalidArgumentException("Passordet må oppgis!");

        $query_get_user = "SELECT `id`, `username`, `password` FROM `users` WHERE `username` = :username";
       
        $statement = Db::getPdo()->prepare($query_get_user);
        $statement->execute([":username" => $credentials["username"]]);
        $user = $statement->fetchObject();

        if ($user !== false)
        {
            $hash = $user->password;

            if (password_verify($credentials["password"], $hash))
            {
                if (password_needs_rehash($hash, Config::get("password.algo"), ["cost" => Config::get("password.cost")]))
                {
                    $query_update_hash = "UPDATE `users` SET `password` = :hash WHERE `id` = :user_id";

                    $statement = Db::getPdo()->prepare($query_update_hash);
                    $statement->execute([
                        ":hash" => password_hash($credentials["password"], Config::get("password.algo"), ["cost" => Config::get("password.cost")]),
                        ":user_id" => $user->id
                    ]);
                }

                return $user->id;
            }
        }

        return false;
    }

    /**
     * Sjekk om en session eksisterer, altså om brukeren er logget inn
     * @return boolean True om en session eksisterer, false om ikke
     */
    public static function check()
    {
        if (self::$user !== null)
            return true;

        if (isset($_COOKIE[Config::get("cookie.session")]))
        {
            $query_check_session = "SELECT `user_id` FROM `user_sessions` WHERE `session_id` = :session_id";
            $statement = Db::getPdo()->prepare($query_check_session);
            $statement->execute([":session_id" => $_COOKIE[Config::get("cookie.session")]]);

            $session = $statement->fetchObject();
            
            if ($session !== false)
            {
                $user_id = $session->user_id;
                self::$user = new User($user_id);

                return true;
            }
        }

        return false;
    }

    /**
     * Returner user objektet til en bruker i aktiv session
     * @return User Object assosiert med den aktive sessionen
     */
    public static function user()
    {
        if (self::$user !== null)
            return self::$user;
        else
            throw new Exception("Funksjon Auth::user() kan ikke brukes når det ikke er noen aktiv session.");
    }

    /**
     * Returner user iden til user objektet til en bruker i aktiv session
     * @return integer brukerid assosiert med den aktive sessionen
     */
    public static function id()
    {
        if(self::$user !== null)
            return self::$user->getId();
        else
            throw new Exception("Function Auth::id() kan ikke brukes når det ikke er noen aktiv session.");
    }

    /**
     * Slett den aktive sessionen, altså logg ut brukeren
     * @return void
     */
    public static function logout()
    {
        if (isset($_COOKIE[Config::get("cookie.session")]))
        {
            $session_id = $_COOKIE[Config::get("cookie.session")];
            $query_delete_session = "DELETE FROM `user_sessions` WHERE `session_id` = :session_id";

            $statement = Db::getPdo()->prepare($query_delete_session);
            $statement->execute([":session_id" => $_COOKIE[Config::get("cookie.session")]]);

            @setcookie(Config::get("cookie.session"), "", 0, Config::get("cookie.session.path"));
        }      
    }

    /**
     * Prøv å registrer en bruker og logg deretter brukeren inn.
     * 
     * @param Array $credentials Et array inneholdende brukernavn, passord og navn
     * For eksempel: $credentials = [
     * "username"   => "navn",
     * "password"   => "passord",
     * "name"       => "Ola Nordmann
     * ];
     * @return integer Bruker ID til den nylige registrerte brukeren
     */
    public static function register(Array $credentials)
    {
        if (!array_key_exists("username", $credentials))
            throw new InvalidArgumentException("Brukernavnet må oppgis!");
            
        if (!array_key_exists("password", $credentials))
            throw new InvalidArgumentException("Passordet må oppgis!");

        if (!array_key_exists("name", $credentials))
            throw new InvalidArgumentException("Navnet må oppgis!");

        $query_register_user = "INSERT INTO `users`(`username`, `password`, `name`, `joined`) VALUES (:username, :password, :name, NOW())";
        $hash = password_hash($credentials["password"], Config::get("password.algo"), ["cost" => Config::get("password.cost")]);

        $db = Db::getPdo();
        $statement = $db->prepare($query_register_user);
        $statement->execute([
            ":username"     => $credentials["username"],
            ":password"     => $hash,
            ":name"         => $credentials["name"]
        ]);
        $return_id = $db->lastInsertId();
        // PDO::lastInsertId() -- id = $Db::getPdo()->lastInsertId();
        //(Auth::attempt(["username" => @$_POST["username"], "password" => $_POST["password"], "keep_logged_in" => isset($_POST["keep_logged_in"])])
        if (!self::attempt(["username" => $credentials["username"], "password" => $credentials["password"], "keep_logged_in" => 0]))
        {
            return -1;
        }
        else
        {
            return $return_id;
        }
    }
}