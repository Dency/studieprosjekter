<?php
/**
 * Box klasse for enkelt bruk av beholdere
 */
class Box
{
    private $title;
    private $content;
    private $style;
    private $class;
    private $centerText;

    /**
     * Opprett et objekt som representerer en Box beholder.
     * 
     * @param string $title Tittelen til boksen
     * @param string $content Inneholdet til boksen, altså bodien 
     * @param boolean $centerText For å sentrere tekst, true eller false 
     */
    public function __construct($title = "", $content = "", $centerText = false)
    {
        $this->title = $title;
        $this->content = $content;
        $this->style = "";
        $this->class = "";
        $this->centerText = $centerText;
    }

    /**
     * Sett tittelen til boksen
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Sett klassen til boksen, overskriv gamle klasser
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * Legg til en klasse til boksen, behold gamle klasser
     * @param string $class
     */
    public function addClass($class)
    {
        $this->class .= " ".$class;
        return $this;
    }

    /**
     * Sett stylen til boksen
     * @param string $value
     */
    public function setStyle($value)
    {
        $this->style = $value;
        return $this;
    }   

    /**
     * Sett innholdet til boksen
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = content;
        return $this;
    }

    /**
     * Sett boksen til å sentrere tekst 
     * @param boolean $value
     */
    public function setCenter($value)
    {
        $this->centerText = $value;
        return $this;
    }

    /**
     * Legg til bodien til boksen
     * @param string $text
     */
    public function append($text)
    {
        $this->content .= $text;
        return $this;
    }
    
    /**
     * Returner boksen som HTML
     * @return string
     */
    public function show()
    {
        $card_div = ($this->centerText) ? ("<div class='card text-center'>") : ("<div class='card'>");
        $body = "
        <div class='{$this->class}' style='{$this->style}' >
            {$card_div}
                <h5 class='card-header'>{$this->title}</h5>
                <div class='card-body'>
                    {$this->content}
                </div>
            </div>
        </div>";

        return $body;
    }
}