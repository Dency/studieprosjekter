<?php 

/**
 * Instillinger
 */
class Config
{
    private static $data = [];

    /**
     * Set config verdi
     * @params $key Nøkkelen å sette
     * @params $value Verdien å sette 
     */
    public static function set($key, $value)
    {
        Config::$data[$key] = $value;
    }

    /**
     * Hent verdi fra config
     * @params @key Nøkkelen å hente fra
     * @return @data_key om eksisterer, om ikke null
     */
    public static function get($key)
    {
        if (array_key_exists($key, Config::$data))
        {
            return Config::$data[$key];
        }
        else
        {
            trigger_error("Konfigurasjons nøkkel {$key} eksisterer ikke.", E_USER_WARNING);
            return null;
        }
    }
}
