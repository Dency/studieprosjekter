<?php 

/**
 * Db klasse for enklere bruk av DB 
 */
class Db
{

    /**
     * Returner PDO objekt
     * @return PDO object
     */
    public static function getPdo()
    {
        try
        {
            $pdo = new \PDO(
                "mysql:host=" . Config::get("mysql.host") . ";dbname=" . Config::get("mysql.database") . ";charset=utf8",
                Config::get("mysql.username"),
                Config::get("mysql.password")
            );

            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $pdo;
        }
        catch (Exception $e)
        {
            echo "Feil: Kunne ikke koble til databasen.";
            die();
        }
    }

    /**
     * Koble til MySQL serveren og returner et Mysqli objekt som representerer tilkoblingen.
     * @deprecated Bruk av PDO er anbefalt, skal se om vi kan unngå denne helt
     * @see getPdo
     */
    public static function getMysqli()
    {
        $link = new mysqli(Config::get("mysql.host"), Config::get("mysql.username"), Config::get("mysql.password"), Config::get("mysql.database"));

        if (!$link->ping())
        {
            echo "Mysqli feil: Kunne ikke koble til databasen!";
            die();
        }
        else
        {
            $link->set_charset("utf8");
        }
        return $link;
    }
}