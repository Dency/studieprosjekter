<?php
/**
 * Class for simpler editing of pages, this object represents a pageitem that is part of a DynamicPage
 */
class DynamicPageItem
{
    private $id;
    private $header;
    private $content;
    private $type;
    private $editable;
    private $lang;

    /**
     * Construct a pageitem: new DynamicPageItem($id, $header, $content, $type, $editable =false, $lang = "NO")
     * 
     * @param int $id The database id of the pageitem
     * @param String $header The header of the page item (for IMG: URL)
     * @param String $content The content of the page item (for IMG: alt description)
     * @param String $type The type of the page item: HTML, IMG, or TEXT
     * @param boolean $editable Whether or not edit links should be rendered for it (if user viewing has access)
     * @param String $lang The abbreviation of the language to use (NO/EN)
     */
    public function __construct($id, $header, $content, $type, $editable = false, $lang = "NO")
    {
        $this->id = $id;
        $this->header = $header;
        $this->content = $content;
        $this->type = $type;
        $this->editable = $editable;
        $this->lang = $lang;
    }

    /**
     * Returner HTMLen til objektet
     * @return String $src HTML koden til objektet
     */
    public function getHTML()
    {
        if ($this->editable)
        {
            $edit_string = ($this->lang == "NO") ? ("[endre]") : ("edit");
            $src = "<br><a href='/ucp/index.php?p=edit_page&id={$this->id}'>{$edit_string}</a><br>";
        }
        else 
        {
            $src = "";
        }

        if ($this->type == "TEXT")
        {
            if (strlen($this->header) && strlen($this->content))
            {
                $src .= "<h2>{$this->header}</h2><br><p>{$this->content}</p>";
            }
            else if (strlen($this->header) && !strlen($this->content))
            {
                $src .= "<h2>{$this->header}</h2>";
            }
            else if (!strlen($this->header) && strlen($this->content))
            {
                $src .= "<p>{$this->content}</p>";
            }
        }
        else if ($this->type == "IMG")
        {
            $src .= "<img src='{$this->header}' alt='{$this->content}'>";
        }
        else if ($this->type == "HTML")
        {
            $src .= $this->content;
        }
        return $src;
    }
}

/**
 * Representer et objekt av PageItems, for lettere bruk og redigering av dynamiske sider
 */
class DynamicPage
{
    private $items = array();
    private $page;
    private $lang;
    private $user;

    /**
     * Opprett et objekt av pageitems, henter fra database, for eks: new DynamicPage($page, $lang, $user)
     * 
     * @param String $page Siden som skal hentes ut i fra
     * @param String $lang Språket som skal benyttes
     * @param User $user Brukeren som skal se objektet
     */
    public function __construct($page, $lang, $user = null)
    {
        $this->page = $page;
        $this->lang = $lang;
        $this->user = $user;

        $query_get_page_items = "SELECT `id`, `page`, `header`, `content`, `type` FROM `dynamic_page` WHERE `page` = :page AND `lang` = :lang ORDER BY `priority`"; 

        $statement = Db::getPdo()->prepare($query_get_page_items);
        $statement->execute([":page" => $page, ":lang" => $lang]);
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        if ($statement->rowCount())
        {
            foreach($results as $result)
            {
                $this->items[] = new DynamicPageItem($result["id"], $result["header"], $result["content"], $result["type"], $this->canEdit(), $lang);
            }
        }
    }

    /**
     * Returnerer en boolean om hvorvidt brukeren har tillatelse til å redigere pageitemet
     * 
     * @return Boolean True om brukeren kan redigere, false om ikke
     */
    public function canEdit()
    {
        if ($this->user != null)
        {   
            if ($this->user->getGroup()->getPermission("manage") >= 1)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Returnerer HTML koden til objektet (alle pageitems i objektet)
     * 
     * @return String $src HTML koden til objektet
     */
    public function render()
    {
        $src = "";
        if (count($this->items))
        {
            foreach($this->items as $item)
            {
                $src .= $item->getHTML();
            }
        }
        return $src;
    }

    /**
     * Sletter listen over DynamicPageItems, denne funksjonen er for øyeblikket unødvendig
     * 
     */
    public function clear()
    {
        $this->items = array();
        $this->page = "";
    }
}