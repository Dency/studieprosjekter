<?php
/**
 * Klasse for enklere bruk av Grupper
 * 
 */
class Group 
{
    private $id = null;
    private $name;
    private $permissions;
    
    /**
     * Opprett et Group objekt som representerer en gruppe
     * Eksempel: new Group($group_id)
     * 
     * @params $group_id Gruppe IDen du vil opprette et objekt for
     */
    public function __construct($group_id)
    {
        $id = (int)$group_id;
                            // groups: id, name, permissions
        $query_get_group = "SELECT `id`, `name`, `permissions` FROM `groups` WHERE `id` = :id";
        
        $statement = Db::getPdo()->prepare($query_get_group);
        $statement->execute([":id" => $group_id]);
        $group = $statement->fetchObject();

        if ($group !== false)
        {
            $this->$id = (int) $group_id;
            $this->name = $group->name;
            $this->permissions = $group->permissions;
        }
    }

    /**
     * Returner gruppen sin ID
     * @return int id
     */
    public function getGroupId() { return $this->id; } 

    /**
     * Returner gruppen sitt navn
     * @return String name
     */
    public function getGroupName() { return $this->name; }

    /**
     * Returner en JSON string med gruppen alle sine permissions
     * @return String permissions
     */
    public function getPermissions() { return $this->permissions; }

    /**
     * Sjekk om gruppen har permissions over eller et spesifikt nivå
     * 
     * @params String $key Stringen som representerer permissionen
     * @params int $level Heltallet som representer nivået som må være likt eller over
     * @return boolean True om har permissions, false om ikke
     */
    public function hasPermission($key, $level)
    {
        $decoded = json_decode($this->permissions, true);

        if (isset($decoded[$key]))
        {
            if ($decoded[$key] >= $level)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returner permissionnivået for en spesifikk permission
     * 
     * @params String $key Nøkkelen som skal sjekkes
     * @return int Permissionnivået 
     */
    public function getPermission($key)
    {
        $decoded = json_decode($this->permissions, true);

        if (isset($decoded[$key]))
        {
            return $decoded[$key];                
        }

        return 0;
    }
}
