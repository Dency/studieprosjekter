<?php

/**
 * Class to see if input exist or return input 
 */
class Input
{
    /**
     * Check if POST/GET exists
     * @params $string post/get
     * 
     * @return boolean True if exists, false if not
     */
    public static function exists($type = "post")
    {
        switch($type)
        {
            case "post":
            {
                return (!empty($_POST)) ? true : false;
                break;
            }
            case "get":
            {
                return (!empty($_POST)) ? true : false;
                break;
            }
            default:
                return false;
                break;
        }
    }

    /**
     * Get POST / GET item if exists
     * @params $string post/get item, The item you want to return
     * 
     * @return $item If exists, if not return nothing 
     */
    public static function get($item)
    {
        if (isset($_POST[$item]))
        {
            return $_POST[$item];
        }
        else if (isset($_GET[$item]))
        {
            return $_GET[$item];
        }

        return "";
    }

}