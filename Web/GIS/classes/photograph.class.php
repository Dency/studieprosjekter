<?php
/**
 * Klasse for enklere bruk og visning av fotografer
 */
class Photograph
{
    private $id;
    private $filename;
    private $type;
    private $caption;
    private $time;

    private $lang;
    private $canDelete;
    private $upload_directory = "images";

    /**
     * Opprett et klassebjekt som representerer et bilde
     * For eksempel: new Photograph($id, $lang, $canDelete)
     * 
     * @param int $id Database IDen til fotografiet
     * @param String $lang Språket som bildeobjektene skal renderes i
     * @param Boolean $canDelete Hvorvidt brukeren skal få opp knapp for å slette bilde eller ikke
     */
    public function __construct($id, $lang = "NO", $canDelete = false)
    {
        $query_get_photograph = "SELECT `filename`, `type`, `caption`, `time` FROM `photographs` WHERE `id` = :id";
        $statement = Db::getPdo()->prepare($query_get_photograph);

        $statement->execute([":id" => $id]);
        if ($statement->rowCount())
        {
            $photo = $statement->fetchObject();

            $this->id = $id;
            $this->filename = $photo->filename;
            $this->type = $photo->type;
            $this->time = $photo->time;
            $this->caption = $photo->caption;

            $this->lang = $lang;
            $this->canDelete = $canDelete;
        }
    }

    /**
     * Returner IDen til bildet, nyttig for å se om klassen er instansiert eller ikke 
     */
    public function getId() { return $this->id; } 

    /**
     * Returner pathen til bildet
     */
    public function getImageURL()
    {
        return $this->upload_directory . "/" . $this->filename;
    }
    
    /**
     * Returner HTML kode for å vise bilde slik det ville vært i et album
     */
    public function show()
    {
        $subCaption = substr($this->caption, 0, 32);
        $subCaption = substr_replace($subCaption, "...", 32);
        $uploaded = ($this->lang == "NO") ? ("Lastet opp ") : ("Uploaded");
        $src = "<div class='card mb-4'>
            <img class='card-img-top img-thumbnail' id='img{$this->getId()}' src='" . $this->getImageURL() ."' alt='" . $this->caption . "'>
            <div class='card-body'>
                <p class='card-text'>{$subCaption}</p>
            </div>
            <div class='card-footer'>
                <small class='text-muted'>{$uploaded} {$this->time}</small>
                <div class='btn-group float-right'>
                    <a class='btn btn-sm btn-outline-secondary d-block' id='imglink{$this->getId()}' role='button'>View</a>";
        if ($this->canDelete)
            $src .= "<a class='btn btn-sm btn-outline-secondary d-block' role='button' href='?page=gallery&delete={$this->id}'>Delete</a>";

        $src .= "</div></div></div>";
        // TODO: Shorten this code, we could probably use an array and a loop to apply the onclick functions
        $src.= "
        <script>
        var img_{$this->getId()} = document.getElementById('img{$this->getId()}');
        var img_link{$this->getId()} = document.getElementById('imglink{$this->getId()}');

        img_link{$this->getId()}.onclick = function()
        {
            modalView.style.display = 'block';
            modalImg.src = '{$this->getImageURL()}';
            modalCaption.innerHTML = '"  . json_encode($this->caption) .  "';
        }

        var span_{$this->getId()} = document.getElementsByClassName('close')[0];

        span_{$this->getId()}.onclick = function()
        {
            modalView.style.display = 'none';
        }
        </script>
        ";
        return $src;
    }
}