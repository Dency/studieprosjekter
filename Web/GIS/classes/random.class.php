<?php
/**
 * Generer forskjellige tilfeldige strings for forskjellige ting
 */
class Random
{

    /**
     * Generer tilfeldig salt
     * @params $length lengden av salten
     * @params $encoding Hvilken encoding å bruke
     * 
     * @return random salt
     */
    public static function generateSalt($length = 12, $encoding = "base64")
    {
        //$salt = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
        $salt = random_bytes($length);

        switch($encoding)
        {
            case "sha":
            {
                $salt = hash("sha512", $salt);
                break;
            }
            case "base64";
            default:
            {
                $salt = base64_encode($salt);
                break;
            }
        }
        $salt = substr($salt, 0, $length);
        return $salt;
    }

}