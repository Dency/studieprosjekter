<?php
/**
 * Klasse for redirecting av brukere
 */
class Redirect
{
    /**
     * Redirect to a different page
     * @params url 
     */
    public static function to($url)
    {
        header("Location: {$url}");
        die();
    }

}