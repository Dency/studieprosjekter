<?php
/**
 * Et User objekt som representerer en bruker
 */
class User
{
    private $id = null;
    private $username;
    private $name;
    private $joined;
    private $group = null;
    private $logged_in = false;

    /**
     * Opprett et User objekt som representerer en bruker
     * Eksempel: new User($user_id)
     * 
     * @params int $user_id Bruker IDen du vil opprette et objekt for
     */
    public function __construct($user_id)
    {
        $user_id = (int)$user_id;

        $query_get_user = "SELECT u.`username`, u.`name`, u.`joined`, u.`group_id`, g.`name` AS `g_name` FROM `users` u
            LEFT JOIN `groups` g ON (g.`id` = u.`group_id`) WHERE u.`id` = :id"; 

        $statement = Db::getPdo()->prepare($query_get_user);
        $statement->execute([":id" => $user_id]);
        $user = $statement->fetchObject();

        if ($user !== false)
        {
            $this->id               = (int) $user_id;
            $this->username         = $user->username;
            $this->name             = $user->name;
            $this->joined           = $user->joined;
            $this->logged_in        = true;

            if ($user->group_id !== null)
            {
                $this->group = new Group($user->group_id);
            }
        }
    }

    /**
     * Returner brukerobjektet sin brukerID
     * @return int $id
     */
    public function getUserId() { return $this->id; }
    
    /**
     * Returner brukerobjektet sitt brukernavn
     * @return String $username
     */
    public function getUsername() { return $this->username; }

    /**
     * Returner brukerobjektet sitt navn
     * @return String $name
     */
    public function getName() { return $this->name; }

    /**
     * Returner registreringsdatoen til brukerobjektet
     * @return String $joined
     */
    public function getJoinDate() { return $this->joined; }

    /**
     * Returner brukerobjektet sitt gruppeobjekt
     * @return Group $group
     */
    public function getGroup() { return $this->group; }

    /**
     * Sjekk om brukeren er logget in
     * @return boolean True om logget inn, false om ikke
     */
    public function isLoggedIn() { return $this->logged_in; }

}