<?php
/**
 * Klasse for validering av input
 */
class Validation
{
    private $passed = false,
            $errors = array(),
            $error_count = 0,
            $language = "no";

    /**
     * Opprett en validator for validering av former
     * Eksempel: new Validation($language) (NO/EN)
     * 
     * @params String $sel_lang Språket validatoren skal benytte seg av
     */
    public function __construct($sel_lang)
    {
        if ($sel_lang == "no")
        {
            $this->language = "no";
        }
        else
        {
            $this->language = "en";
        }
    }

    /**
     * Sjekk en kilde om alle formene matcher valgt validering
     * @param $source $_POST/$_GET For eksempel, kilden som skal sjekkes
     * @param Array $items Former, og dens spesifikke regler
     * Om ikke reglene er fulgt, blir en error lagt til
     */
    public function check($source, $items = array())
    {
        foreach($items as $item => $rules)
        {
            foreach($rules as $rule => $rule_value)
            {
                $value = trim($source[$item]);

                if($rule === 'required' && empty($value))
                {
                    $this->addError(($this->language == "no") ? ("{$item} er nødvendig!") : ("{$item} is required!"));
                }
                else if (!empty($value))
                {
                    switch($rule)
                    {
                        case "min":
                        {
                            if (strlen($value) < $rule_value)
                            {
                                $this->addError(($this->language == "no") ? ("{$item} må være minst {$rule_value} tegn!") : ("{$item} has to be at least {$rule_value} characters!"));
                            }
                            break;
                        }
                        case "max":
                        {
                            if (strlen($value) > $rule_value)
                            {
                                $this->addError(($this->language == "no") ? ("{$item} kan være maksimum {$rule_value} tegn!") : ("{$item} should not exceed {$rule_value} characters!"));
                            }
                            break;
                        }
                        case "matches":
                        {
                            if ($value != $source[$rule_value])
                            {
                                $this->addError(($this->language == "no") ? ("{$rule_value} må være likt {$item}!") : ("{$rule_value} must match {$item}!"));
                            }
                            break;
                        }
                        case "unique":
                        {
                            $query_check_unique = "SELECT * FROM `{$rule_value}` WHERE `{$item}` = :{$item}";
                            $statement = Db::getPdo()->prepare($query_check_unique);
                            $statement->execute([":{$item}" => $value]);
                            if ($statement->rowCount())
                            {
                                $this->addError(($this->language == "no") ? ("{$item} eksisterer allerede!") : ("{$item} already exists!"));
                            }
                            break;
                        }
                    }
                }
            }
        }

        if (empty($this->errors))
           $this->passed = true;

        return $this;
    }

    /**
     * Legg til en error i valideringen
     * @param String $error Feilmeldingen som skal legges til
     */
    private function addError($error)
    {
        // Could potentially use an array for this but not sure if it's more efficient 
        if ($this->language == "no")
        {
            $error = str_replace("username", "Brukernavn", $error);
            $error = str_replace("password", "Passord", $error);
            $error = str_replace("confirm-Passord", "Bekreft passord", $error);
            $error = str_replace("full_name ", "Ditt navn ", $error);
            $error = str_replace("course_name ", "Kursets navn ", $error);
        }
        else
        {
            $error = str_replace("username", "Username", $error);
            $error = str_replace("password", "Password", $error);
            $error = str_replace("confirm-Password", "Confirm password", $error);
            $error = str_replace("full_name ", "Your name ", $error);
            $error = str_replace("course_name ", "Course name ", $error);
        }

        $this->errors[$this->error_count] = $error;
        $this->error_count++; 
    }

    /**
     * Returner alle feil
     * @return String $errors
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Returner hvorvidt valideringen er bestått eller ikke
     * @return Boolean True om bestått, false om ikke
     */
    public function getPassed()
    {
        return $this->passed;
    }

}