<?php
session_start();

require_once __DIR__ . "/../inc/config.php";

//require_once("classes/db.class.php");

spl_autoload_register(function($class) {
    require_once strtolower(__DIR__ . "/../classes/" . $class . ".class.php");
});

if (!isset($_GET['lang']))
{
    if (isset($_COOKIE[Config::get("cookie.selected_language")]))
    {
        $lang = $_COOKIE[Config::get("cookie.selected_language")];
        if ($lang == "no")
            require_once __DIR__ . "/../inc/lang/no.php";
        else
            require_once __DIR__ . "/../inc/lang/en.php";
    }
    else
    {
        $lang = "no";
        require_once __DIR__ . "/../inc/lang/no.php";
    }
}
else
{
    if ($_GET['lang'] == "no") 
    {
        require_once __DIR__ . "/../inc/lang/no.php";
        $lang = "no";
        setcookie(
            Config::get("cookie.selected_language"),
            $lang,
            time() + 3600 * 24 * 21,
            Config::get("cookie.session.path")
        );
    }
    else
    {
        require_once __DIR__ . "/../inc/lang/en.php";
        $lang = "en";
        setcookie(
            Config::get("cookie.selected_language"),
            $lang,
            time() + 3600 * 24 * 21,
            Config::get("cookie.session.path")
        );
    }
}
