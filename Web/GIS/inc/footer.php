<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>

<script>
    $("#cookies_hide").click(function()
    {
        $("#cookies_policy_holder").slideUp("slow");
        Cookies.set("<?php echo Config::get("cookie.accept_cookies");?>", "1", {
            expires: 365,
            domain:'',
            path:'/'
        });
    });
</script>

<footer>
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <!--Column1-->
                    <div class="footer-pad">
                        <h4>
                            <?php echo($message['footer_contact']); ?>
                        </h4>
                        <ul class="list-unstyled">
                            <li></li>
                            <li>Dieu Tien Bui</li>
                            <li>Dieu.T.Bui@usn.no</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-pad">
                        <h4>
                            <?php echo($message['footer_address']); ?>
                        </h4>
                        <address>
                            <ul class="list-unstyled">
                                <li>
                                    Elgfaret 36
                                    <br> 3800 Bø
                                </li>
                                <li>
                                    <?php echo($message['footer_address_phone']); ?> 35 95 25 48
                                </li>
                            </ul>
                        </address>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <!--Footer Bottom-->
                        <p class="text-xs-center">&copy; Copyright 2018 -
                            <?php echo($message['footer_school']); ?>.
                            <?php echo($message['footer_copyright']); ?>
                        </p>
                        <p class="text-xs-center">
                            <a href="?page=cookies">
                                <?php echo($message['footer_cookies']); ?>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</footer>
</body>

</html>