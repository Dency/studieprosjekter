<?php
require_once __DIR__ . "/../core/init.php";
define("IN_PROJECTGIS", 1);

$active = "home";

if (!isset($_GET["page"]))
{
    $active = "home";
}
else
{
    $page = $_GET["page"];

    switch($page)
    {
        case "courses":
        {
            $active = "courses";
            break;
        }
        case "contact":
        {
            $active = "contact";
            break;
        }
        case "gallery":
        {
            $active = "gallery";
            break;
        }
        case "home":
        {
            $active = "home";
            break;
        }
        default:
        {
            $active = "none";
            break;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <base href="http://projectgis.local">
    <link rel="stylesheet" type="text/css" href="inc/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="inc/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <title>Project GIS</title>
</head>
<body>
    <?php 
    if (!isset($_COOKIE[Config::get("cookie.accept_cookies")]))
    {
    ?>
    <div id='cookies_policy_holder'>
        <div id='cookies_policy'>
            <div class='title'>Cookies at GIS i USN</div>
            <div class='message'>We use cookies to ensure the best experience for you on our website. By using this website you accept our use of cookies.</div>
            <a class='btn btn-light' href='?page=cookies'>Read more</a>
            <a class='btn btn-light' id='cookies_hide'>Accept</a>
        </div>
    </div>
    <?php 
    } ?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="?page=home"><?php echo($message['nav_brand']); ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <?php 
                    if ($lang == "no")
                        echo '<a class="nav-link" href="?lang=en">EN</a>';
                    else
                        echo '<a class="nav-link" href="?lang=no">NO</a>';
                    ?>
                </li>
                <li class="nav-item <?php if ($active == "home") { echo "active"; } ?>">
                    <a class="nav-link" href="?page=home"><?php echo($message['nav_home']); ?><span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Info
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="?page=faq"><?php echo($message['nav_faq']); ?></a>
                        <a class="dropdown-item" href="?page=work"><?php echo($message['nav_work']); ?></a>
                        <a class="dropdown-item" href="?page=community"><?php echo($message['nav_community']); ?></a>
                        <a class="dropdown-item" href="?page=fou"><?php echo($message['nav_fou']); ?></a>
                        <a class="dropdown-item" href="?page=about"><?php echo($message['nav_about']); ?></a>
                    </div>
                </li>
                <li class="nav-item <?php if ($active == "courses") { echo "active"; } ?>">
                    <a class="nav-link" href="?page=courses"><?php echo($message['nav_study']); ?></a>
                </li>
                <li class="nav-item <?php if ($active == "gallery") { echo "active"; } ?>">
                    <a class="nav-link" href="?page=gallery"><?php echo($message['nav_gallery']); ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-primary" href="/ucp/login.php"><?php echo((Auth::check()) ? ($message['nav_ucp']) : ($message['nav_login'])); ?></a>
                </li>
            </ul>
        </div>
    </nav>
    
    

    
