<?php
// header.php
$message['nav_brand'] = "GIS, University College of Southeast Norway";
$message['nav_home'] = 'Home';
$message['nav_faq'] = 'Frequently asked questions';
$message['nav_work'] = 'Work with GIS';
$message['nav_community'] = 'GIS in the community';
$message['nav_fou'] = 'FoU';
$message['nav_study'] = 'GIS study';
$message['nav_contact'] = 'Contact GIS';
$message['nav_gallery'] = 'Gallery';
$message['nav_login'] = 'Login';
$message['nav_ucp']   = 'Control Panel';
$message['nav_about'] = 'About Us';

// footer.php
$message['footer_contact'] = 'Contact';
$message['footer_address'] = 'Address';
$message['footer_address_phone'] = 'Phone: ';
$message['footer_copyright'] = 'All rights reserved.';
$message['footer_school'] = 'GIS, University College of Southeast Norway';
$message['footer_cookies'] = 'Privacy';

// index.php
$message['index_jumbo_h1'] = 'This is a test';
$message['index_jumbo_button'] = 'See more!';

// ucp/login.php
$message['login_alertsuccess_title'] = 'You have logged in!';
$message['login_alertsuccess_description'] = 'Success!';
$message['login_alertdanger_invalid_title'] = 'Invalid username or password!';
$message['login_alertdanger_invalid_description'] = 'You have entered an invalid username or password.';
$message['login_alertdanger_unfilled_title'] = 'You haven\'t filled out the required fields!';
$message['login_ask_if_acc'] = 'Don\'t have an account yet?';
$message['login_sign_up_now'] = 'Sign up now!';
$message['login_logged_in'] = 'Keep me logged in';
$message['login_placeholder_password'] = 'Enter password';
$message['login_placeholder_username'] = 'Enter username';
$message['login_sign_in_button'] = 'Sign in';
$message['login_header'] = 'Sign in to access your account';

// ucp/register.php
$message['register_alertdanger_invalid_title'] = 'Unknown error!';
$message['register_alertdanger_invalid_description'] = 'Failed to insert your account into the database, please try again later...';
$message['register_alertdanger_unfilled_title'] = 'You haven\'t filled out the required fields!';
$message['register_h2_createuser'] = "Create Your Account";
$message['register_p_signuptextcenter'] = "Sign up to get started";
$message['register_button_submit'] = "Create Account";
$message['register_link_login'] = "Login Now";
$message['register_p_alreadyhave'] = "Already have an account?";
$message['register_placeholder_name'] = "Enter your name";
$message['register_placeholder_confirm_pass'] = "Confirm password";

// ucp/index.php
$message['ucp_welcome'] = 'Welcome';
$message['ucp_yourjoindate'] = 'You became a member at:';
$message['ucp_yourgroup'] = 'You are member of the group:';
$message['ucp_group_unregistered'] = 'Unregistered';
$message['ucp_logout'] = 'Log out';

// about.php
$message['about_jumbo_h1'] = 'About us';
$message['about_jumbo_description'] = 'Geografical informationssystems, GIS is a course where you use computer tools for collecting, processing, saving and presenting of geografical data. We have 4 employees at this moment in time. You can find their contact info underneath';
$message['about_Arne_Hjeltnes'] = 'Arne Hjeltnes';
$message['about_Stilling'] = 'Chief engineer';
$message['about_Arne_Hjeltnes_description'] = 'School of business <br> Department of Business and IT <br> Campus Bø (1-320)';
$message['about_Arne_Hjetnes_More'] = 'More Info';
$message['about_Dieu_Tien_Stilling'] = 'Associate Professor';
$message['about_Dieu_Tien_description'] = 'School of business <br> Department of Business and IT  <br> Campus Bø (1-324)';
$message['about_Dieu_Tien_More'] = 'More info';
$message['about_Endre_Før_Gjermundsen_Stilling'] = 'Associate Professor';
$message['about_Kjell_Øyvind_Kjenstad_Stilling'] = 'Associate Professor';
$message['about_Kjell_Øyvind_Kjenstad_description'] = 'School of Business <br> Department of Business and IT <br> Campus Bø (1-335)';
$message['about_Kjell_Øyvind_Kjenstad_More'] = 'More info';

// fou.php 
$message['fou_kartlegging_av_habitat'] = '2. Mapping of habitat to deer based on satellite images';
$message['fou_kartlegging_av_habitat_info'] = 'The goal with the maping work is to prepare a solid vegetation map/areatype map for three different location in Vestlandet which is about 2 600km^2. The map work is done within the period 2007-2011. We have produced four maps: Hauglandet, Sogn-Sunnfjord, Tingvoll-snillfjord og Orkdal. (reference 2) <br> An object based image analysis was used where satellite pictures are included (Spot5 and Landsat 7), pictures from flights and numerous digital maps(reference 3) <br>  ';
$message['fou_gis_sammarbeidet'] = '1. The GIS-teamwork';
$message['fou_gis_sammarbeidet_info'] = 'The GIS-teamwork represents municipalities, public agencies on county level and private firms in Buskerud, Vestfold and Telemark. The cooperation startet in 1998 under the direction of Telemarksforskning-Bø. Now it is under the direction of Høgskolen i Telemark, AF (in Bø). We arrange three to four gatherings yearly with focus on professional achievements in area planning, Building Matters and GIS/map. We want to creat a meatingplace between people in these municipalities within geodata, area planning and Building Matters in addition to public administration and private firms. (Reference 2 and 3)<ul><li>Arne hjeltnes has been the project manager since its inception in 1998</li></ul></p><br/>1. More information about the GIS-teamwork.';
$message['fou-vegetasjonskartlegging'] = '3. Vegetation mapping based on satellite images';

// contact.php
$message['contact_contact'] = 'Contact us';
$message['contact_name'] = 'Name';
$message['contact_name2'] = '"Your name"';
$message['contact_email'] = 'Your e-mail';
$message['contact_email2'] = '"Your e-mail"';
$message['contact_message'] = 'Your question';
$message['contact_message2'] = '"Please enter your question here..."';
$message['contact_submit'] = 'Submit';

// courses.php
$message['left_nav_apply'] = 'Apply for GIS here!';
$message['courses_heading'] = 'Courses';
$message['courses_code_heading'] = 'Course code';
$message['courses_name_heading'] = 'Course name';
$message['courses_points_heading'] = 'Study points';
$message['courses_oblig_heading'] = 'O/V';
$message['courses_gisogkart_heading'] = 'GIS and maps';
$message['courses_databaser_heading'] = 'Database';
$message['courses_land_heading'] = 'Database';
$message['courses_geografisk_heading'] = 'Geographic Analysis';
$message['courses_prosjekt_heading'] = 'Practical project in GIS';
$message['courses_fjernanalyse_heading'] = 'Remote Sensing';
$message['courses_arealplanlegging_heading'] = 'Areal planning and Environmental law';




?>