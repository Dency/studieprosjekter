<?php
// header.php
$message['nav_brand'] = "GIS, Høgskolen i Sørøst-Norge";
$message['nav_home'] = 'Startside';
$message['nav_faq'] = 'Ofte stilte spørsmål';
$message['nav_work'] = 'Jobbe med GIS';
$message['nav_community'] = 'GIS i samfunnet';
$message['nav_fou'] = 'FoU';
$message['nav_study'] = 'GIS studiet';
$message['nav_contact'] = 'Kontakt GIS';
$message['nav_gallery'] = 'Galleri';
$message['nav_login'] = 'Logg inn';
$message['nav_ucp']   = 'Kontrollpanel';
$message['nav_about'] = 'Om Oss';

// footer.php
$message['footer_contact'] = 'Kontakt';
$message['footer_address'] = 'Adresse';
$message['footer_address_phone'] = 'Telefon: ';
$message['footer_copyright'] = 'Alle rettigheter reservert.';
$message['footer_school'] = 'GIS, Høgskolen i Sørøst-Norge';
$message['footer_cookies'] = 'Personvern';

// index.php
$message['index_jumbo_h1'] = 'Dette er en test';
$message['index_jumbo_button'] = 'Se mer!';

// ucp/login.php
$message['login_alertsuccess_title'] = 'Du har logget inn!';
$message['login_alertsuccess_description'] = 'Suksess!';
$message['login_alertdanger_invalid_title'] = 'Feil brukernavn eller passord';
$message['login_alertdanger_invalid_description'] = 'Du har skrevet inn feil brukernavn eller passord';
$message['login_alertdanger_unfilled_title'] = 'Du har ikke fyllet ut feltene!';
$message['login_ask_if_acc'] = 'Har du ikke bruker?';
$message['login_sign_up_now'] = 'Registrer deg nå!';
$message['login_logged_in'] = 'Forbli innlogget';
$message['login_placeholder_username'] = 'Skriv inn brukernavn';
$message['login_placeholder_password'] = 'Skriv inn passord';
$message['login_sign_in_button'] = 'Logg inn';
$message['login_header'] = 'Logg inn for å få tilgang til din bruker';

// ucp/register.php
$message['register_alertdanger_invalid_title'] = 'Ukjent feil!';
$message['register_alertdanger_invalid_description'] = 'Klarte ikke å sette brukeren din inn i databasen, vennligst prøv igjen senere...';
$message['register_alertdanger_unfilled_title'] = 'Du har ikke fyllet ut de nødvendige feltene!';
$message['register_h2_createuser'] = "Opprett din bruker";
$message['register_p_signuptextcenter'] = "Opprett en bruker for å få startet!";
$message['register_button_submit'] = "Opprett Bruker";
$message['register_link_login'] = "Logg inn";
$message['register_p_alreadyhave'] = "Har du allerede en bruker?";
$message['register_placeholder_name'] = "Skriv inn ditt navn";
$message['register_placeholder_confirm_pass'] = "Bekreft passord";

// ucp/index.php
$message['ucp_welcome'] = 'Velkommen';
$message['ucp_yourjoindate'] = 'Du ble medlem:';
$message['ucp_yourgroup'] = 'Du er medlem av gruppen:';
$message['ucp_group_unregistered'] = 'Uregistrert';
$message['ucp_logout'] = 'Logg ut';

// about.php
$message['about_jumbo_h1'] = 'Om Oss';
$message['about_jumbo_description'] = 'Geografiske informasjonssystem,GIS, er et fag hvor en tar i bruk dataverktøy for innsamling, bearbeiding, lagring og presentasjon av geografiske data. Vi har for tiden 4 ansatte. Under finner du kontakt info!';
$message['about_Stilling'] = 'Overingeniør';
$message['about_Arne_Hjeltnes_description'] = 'Handelshøyskolen <br> Intitutt for økonomi og IT <br> Campus Bø (1-320)';
$message['about_Arne_Hjetnes_More'] = 'Mer info';
$message['about_Dieu_Tien_Stilling'] = 'Førsteamanuensis';
$message['about_Dieu_Tien_description'] = 'Handelshøyskolen <br> Institutt for økonomi og IT <br> Campus Bø (1-324)';
$message['about_Dieu_Tien_More'] = 'Mer info';
$message['about_Endre_Før_Gjermundsen_Stilling'] = 'Førsteamanuensis';
$message['about_Kjell_Øyvind_Kjenstad_Stilling'] = 'Førsteamanuensis';
$message['about_Kjell_Øyvind_Kjenstad_description'] = 'Handelshøyskolen <br> Institutt for økonomi og IT <br> Campus Bø (1-335)';
$message['about_Kjell_Øyvind_Kjenstad_More'] = 'Mer info';


// fou.php
$message['fou_kartlegging_av_habitat'] = '2. Kartlegging av habitat til hjort basert på satelittbilder';
$message['fou_kartlegging_av_habitat_info'] = 'Målet med kartleggingsarbeidet er å utarbeide et heldekkende vegetasjonskart/arealtypekart for 3 områder på Vestlandet på til sammen ca 26 300 km2. Kartarbeidet er utført i perioden 2007-2011. Vi har produsert 4 kart: Haugalandet, i Sogn-Sunnfjord, Tingvoll-Snillfjord og Orkdal. (referanse 2)
Det ble benyttet en objektbasert bildeanalyse hvor det inngår satellittbilder (Spot5 og Landsat 7), flybilder og en rekke digitale kart. (referanse 3)
Vegetasjonskartene skal danne et av flere forklaringsgrunnlag for analyser av sammenhengen mellom habitatpreferanser til hjort og målte GPS posisjoner til hjort. Arbeidet var et delprosjekt innenfor forskingsprosjektet «Hjorten i det norske kulturlandskapet - arealbruk, bærekraft og næring». (referanse 4)';
$message['fou_gis_sammarbeidet'] = '1. GIS-samarbeidet';
$message['fou_gis_sammarbeidet_info'] = 'Gis-samarbeidet representerer kommuner, offentlige etater på fylkesnivå og private firma i Buskerud, Vestfold og Telemark. Samarbeidet startet opp i 1998 i regi av Telemarksforsking-Bø. Nå går det i regi av Høgskolen i Telemark, AF (i Bø). Vi arrangerer 3-4 samlinger i året med fokus på faglige utfordringer innenfor arealplanlegging, byggesak og GIS/kart. Her ønsker vi å skape en møteplass mellom personer i kommunene innenfor geodata, arealplan og byggesak i tillegg til offentlig forvaltning og private firma. (referanse 1 og 2)<ul><li>Arne hjeltnes har vært prosjektleder fra starten i 1998</li></ul></p><br/> 1. Mer informasjon om GIS-samarbeidet.';
$message['fou-vegetasjonskartlegging'] = '3. vegetasjonskartlegging basert på satellittbilder';

// contact.php
$message['contact_contact'] = 'Kontakt oss';
$message['contact_name'] = 'Navn';
$message['contact_name2'] = '"Ditt Navn"';
$message['contact_email'] = 'Din e-mail';
$message['contact_email2'] = '"Din e-mail"';
$message['contact_message'] = 'Ditt spørsmål';
$message['contact_message2'] = '"Skriv ditt spørsmål her..."';
$message['contact_submit'] = 'Send';

// courses.php
$message['left_nav_apply'] = 'Søk her!';
$message['courses_heading'] = 'Fag';
$message['courses_code_heading'] = 'Emnekode';
$message['courses_name_heading'] = 'Emnets navn';
$message['courses_points_heading'] = 'Studiepoeng';
$message['courses_oblig_heading'] = 'O/V';
$message['courses_gisogkart_heading'] = 'GIS og kart';
$message['courses_databaser_heading'] = 'Databaser';
$message['courses_land_heading'] = 'Landmåling';
$message['courses_geografisk_heading'] = 'Geografisk analyse';
$message['courses_prosjekt_heading'] = 'Prosjektarbeid i GIS';
$message['courses_fjernanalyse_heading'] = 'Fjernanalyse';
$message['courses_arealplanlegging_heading'] = 'Arealplanlegging og miljørett';


?>