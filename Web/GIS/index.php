<?php
require_once "inc/header.php"; 

if (!isset($_GET["page"]))
{
    // Startside
    require_once "pages/main.php";
}
else
{
    $page = $_GET["page"];

    switch($page)
    {
        case "study":
        {
            require_once "pages/study.php";
            break;
        }
        case "contact":
        {
            require_once "pages/contact.php";
            break;
        }
        case "about":
        {
            require_once "pages/about.php";
            break;
        }
        case "gallery":
        {
            require_once "pages/gallery.php";
            break;
        }
        case "faq":
        {
            require_once "pages/faq.php";
            break;
        }
        case "fou":
        {
            require_once "pages/fou.php";
            break;
        }
        case "cookies":
        {
            require_once "pages/cookies.php";
            break;
        }
        case "community":
        {
            require_once "pages/community.php";
            break;
        }
        case "work":
        {
            require_once "pages/work.php";
            break;
        }
        case "courses":
        {
            require_once "pages/courses.php";
            break;
        }
        case "announcements":
        {
            require_once "pages/announcements.php";
            break;
        }
        default:
        {
            require_once "pages/main.php";
            break;
        }
    }
}


require_once "inc/footer.php";
