<div class="container mt-3">
    <div class="row">
        <div class="container-fluid bg-white col-sm-12 col-md-12 col-lg-12 ">
            <h1 class="display-4">
                <?php echo($message['about_jumbo_h1']); ?>
            </h1>
            <p>
                <?php echo($message['about_jumbo_description']); ?>
            </p>
            <hr class="my-3">
        </div>
        <div class="jumbotron bg-grey rounded-0 col-sm-12 col-md-6 col-lg-3 mt-1">
            <img src="inc/img/Arne.Hjeltnes-hit.no_employee.jpg" width="100%">
            <h2 class="mt-2">Arne Hjeltnes</h2>
            <h3 class="text-muted">
                <?php echo($message['about_Stilling']); ?>
            </h3>
            <p>
                <?php echo($message['about_Arne_Hjeltnes_description']); ?>
            </p>
            <a type="E-mail" href="mailto:Arne.Hjeltnes@usn.no" target="_blank" class="font-weight-bold">Arne.Hjeltnes@usn.no</a>
            <br>
            <a href="tel:35962726" class="font-weight-bold">35 95 27 26</a>
            <br>
            <a class="btn btn-primary" target="_blank" href="https://www.usn.no/about-usn/contact-us/employees/arne-william-hjeltnes-article201584-7531.html">
                <?php echo($message['about_Kjell_Øyvind_Kjenstad_More']); ?>
            </a>
        </div>
        <div class="jumbotron bg-grey rounded-0 col-sm-12 col-md-6 col-lg-3 mt-1">
            <img src="inc/img/DieuTBui.jpg" width="100%">
            <h2 class="mt-5 pt-4">Dieu Tien Bui</h2>
            <h3 class="text-muted">
                <?php echo($message['about_Dieu_Tien_Stilling']); ?>
            </h3>
            <p>
                <?php echo($message['about_Dieu_Tien_description']); ?>
            </p>
            <a type="E-mail" href="mailto:dieu.t.bui@usn.no" target="_blank" class="font-weight-bold">dieu.t.bui@usn.no</a>
            <br>
            <a href="tel:35962726" class="font-weight-bold">35 95 27 24</a> /
            <a href="tel:96677678" class="font-weight-bold">966 77 678</a>
            <br>
            <a class="btn btn-primary" target="_blank" href="https://www.usn.no/about-usn/contact-us/employees/dieu-tien-bui-article196922-7531.html">
                <?php echo($message['about_Kjell_Øyvind_Kjenstad_More']); ?>
            </a>
        </div>
        <div class="jumbotron bg-grey rounded-0 col-sm-12 col-md-6 col-lg-3 mt-1">
            <!-- <img src="inc/img/Arne.Hjeltnes-hit.no_employee.jpg" width="100%">    -->
            <h2 class="mt-2">Endre Før Gjermundsen</h2>
            <h3 class="text-muted">
                <?php echo($message['about_Endre_Før_Gjermundsen_Stilling']); ?>
            </h3>
            <p>
                <?php echo($message['about_Kjell_Øyvind_Kjenstad_description']); ?>
            </p>
            <a type="E-mail" href="mailto:Endre.F.Gjermundsen@usn.no" target="_blank" class="font-weight-bold">Endre.F.Gjermundsen@usn.no</a>
            <br>
            <a href="tel:35952882" class="font-weight-bold">35952882</a>
            <br>
            <a class="btn btn-primary" target="_blank" href="https://www.usn.no/about-usn/contact-us/employees/endre-for-gjermundsen-article211424-7531.html">
                <?php echo($message['about_Kjell_Øyvind_Kjenstad_More']); ?>
            </a>
        </div>
        <div class="jumbotron bg-grey rounded-0 col-sm-12 col-md-6 col-lg-3 mt-1">
            <img src="inc/img/KjellØyvindKjenstad.jpg" width="100%">
            <h2 class="mt-2">Kjell Øyvind Kjenstad</h2>
            <h3 class="text-muted">
                <?php echo($message['about_Kjell_Øyvind_Kjenstad_Stilling']); ?>
            </h3>
            <p>
                <?php echo($message['about_Kjell_Øyvind_Kjenstad_description']); ?>
            </p>
            <a type="E-mail" href="mailto:Kjell.Kjenstad@usn.no" target="_blank" class="font-weight-bold mb-2">Kjell.Kjenstad@usn.no</a>
            <br>
            <a href="tel:35952554" class="font-weight-bold">35 95 25 54</a> /
            <a href="tel:45422850" class="font-weight-bold">454 22 850</a>
            <br>
            <a class="btn btn-primary" target="_blank" href="https://www.usn.no/about-usn/contact-us/employees/kjell-oyvind-kjenstad-article195117-7531.html">
                <?php echo($message['about_Kjell_Øyvind_Kjenstad_More']); ?>
            </a>
        </div>
    </div>
</div>