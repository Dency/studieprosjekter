<?php 

if (isset($_GET["read"]))
{
    $query_select_announcement = "SELECT `id`, `title`, `content`, `time`, `image` FROM `announcements` WHERE `id` = :id";
    
    $statement = Db::getPdo()->prepare($query_select_announcement);
    $statement->execute([":id" => $_GET["read"]]);
    if ($statement->rowCount())
    {
        $article = $statement->fetchObject();
        $box = new Box($article->title, "", false);
        $box->append("<p><i>Updated: {$article->time}</i></p>");
        $box->setClass("col-lg-6 col-md-6 col-sm-6 col-xs-6 mt-3 mb-3 well mx-auto");
        if (strlen($article->image))
        {
            $box->append("<br><img class='img-thumbnail mx-auto d-block' src='{$article->image}' alt='{$article->title}'><br>");
        }
        $box->append("<p>{$article->content}</p>");
        $box->append("<br><div class='text-center'><a href='index.php?page=announcements'>See all announcements</a></div>");

        echo $box->show();
    }
    else
    {
        $alert = new Alert("Couldn't find announcement", "Failed to find the specified announcement.", "danger");

        $alert->setClass("col-lg-6 col-md-6 col-sm-6 col-xs-6 mt-3 mb-3 well mx-auto");
        echo $alert->show();
    }
}
else
{
    $query_select_announcements = "SELECT `id`, `title`, `content`, `time`, `image` FROM `announcements` ORDER BY `time` DESC";

    $statement = Db::getPdo()->prepare($query_select_announcements);
    $statement->execute();

    if ($statement->rowCount())
    {
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $news = new Box("Announcements", "");
        $news->setClass("col-lg-6 col-md-6 col-sm-6 col-xs-6 mt-3 mb-3 well mx-auto");
        foreach($results as $result)
        {
            $news->append("<h2>{$result['title']}</h2>");
            $news->append("<p><i>Updated: {$result['time']}</i></p>");
            if (strlen($result['image']))
            {
                $news->append("<br><img class='img-thumbnail mx-auto d-block' src='{$result['image']}' alt='{$result['title']}'><br>");
            }
            $news->append("<p>{$result['content']}</p><hr>");
        }
        $news->append("<div class='text-center'><a href='index.php'>Back to homepage</a></div>");
        echo $news->show();
    }
}
?>