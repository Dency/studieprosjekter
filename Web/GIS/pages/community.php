<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3 well">
			<h1 class="tag-title">
				GIS i samfunnet
			</h1>
			<hr>
			<p>
				Geografiske Informasjonssystemer finner vi arealer i samfunnet: Kartverket; Kartlegging; Turkart; O-kart; Planlegging; Nettjenester;
				Konsultant; Leverandører; Transport; Fagportal; Fylkesmannen; Undervisning; Standarden; Beredskap; Overvåking; Bygg og
				Anlegg; Flyfotografering; laserskanning; Landmåling; Varsler, ...
			</p>
			<br>
		</div>
	</div>



	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://kartverket.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-kartverket" data-toggle="tooltip" data-placement="top"
			    title="Kartverket">
				<img border="0" alt="Kartverket" src="inc/img/kartverket.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.ngu.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-ngu" data-toggle="tooltip" data-placement="top"
			    title="NGU">
				<img border="0" alt="NGU" src="inc/img/ngu.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.ngi.no/no/" target="_blank" class="btn btn-lg btn-block kpx_btn-ngi" data-toggle="tooltip" data-placement="top"
			    title="NGU">
				<img border="0" alt="NGI" src="inc/img/ngi.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://fylkesmannen.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-tumblr" data-toggle="tooltip" data-placement="top"
			    title="Fylkesmannen">
				<img border="0" alt="Fylkesmannen" src="inc/img/fylkesmannen.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="https://www.nve.no/" class="btn btn-lg btn-block kpx_btn-nve" data-toggle="tooltip" data-placement="top" title="NVE">
				<img border="0" alt="NVE" src="inc/img/nve.png" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.vegvesen.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-statens_vegvesen" data-toggle="tooltip"
			    data-placement="top" title="Statens vegvesen">
				<img border="0" alt="Statens vegvesen" src="inc/img/statens_vegvesen.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.skogoglandskap.no/temaer/" class="btn btn-lg btn-block kpx_btn-skog_og_landskap" data-toggle="tooltip"
			    data-placement="top" title="skog+landskap">
				<img border="0" alt="skog + landskap" src="inc/img/skog_og_landskap.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.umb.no/geomatikk/" target="_blank" class="btn btn-lg btn-block kpx_btn-nmbu" data-toggle="tooltip" data-placement="top"
			    title="NMBU">
				<img border="0" alt="NMBU" src="inc/img/nmbu.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://kartverket.no/Geonorge/Norge-digitalt/Om-Norge-digitalt-samarbeidet/" target="_blank" class="btn btn-lg btn-block kpx_btn-norge-digitalt"
			    data-toggle="tooltip" data-placement="top" title="Norge digitalt">
				<img border="0" alt="Norge digitalt" src="inc/img/norge-digitalt.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://kartverket.no/Standarder/SOSI/" target="_blank" class="btn btn-lg btn-block kpx_btn-sosi" data-toggle="tooltip"
			    data-placement="top" title="SOSI">
				<img border="0" alt="SOSI" src="inc/img/sosi.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.kartverket.no/Kart/Geodatasamarbeid/Geovekst/" target="_blank" class="btn btn-lg btn-block kpx_btn-geovekst"
			    data-toggle="tooltip" data-placement="top" title="GEOVEKST">
				<img border="0" alt="GEOVEKST" src="inc/img/geovekst.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://kartverket.no/Posisjonstjenester/" target="_blank" class="btn btn-lg btn-block kpx_btn-Posisjonstjenester"
			    data-toggle="tooltip" data-placement="top" title="Posisjonstjenester">
				<img border="0" alt="Posisjonstjenester" src="inc/img/posisjonstjenester.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.ksat.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-ksat" data-toggle="tooltip" data-placement="top"
			    title="KSAT">
				<img border="0" alt="KSAT" src="inc/img/ksat.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.terratec.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-terratec" data-toggle="tooltip" data-placement="top"
			    title="terratec">
				<img border="0" alt="terratec" src="inc/img/terratec.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.blomasa.com/" target="_blank" class="btn btn-lg btn-block kpx_btn-blom" data-toggle="tooltip" data-placement="top"
			    title="BLOM">
				<img border="0" alt="BLOM" src="inc/img/blom.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.nordeca.com/Analyse-og-planleggingstjenester/L%C3%B8sninger" target="_blank" class="btn btn-lg btn-block kpx_btn-nordeca"
			    data-toggle="tooltip" data-placement="top" title="nordeca">
				<img border="0" alt="nordeca" src="inc/img/nordeca.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.fugro-survey.no/services/landmapping/" target="_blank" class="btn btn-lg btn-block kpx_btn-fugro" data-toggle="tooltip"
			    data-placement="top" title="frugro">
				<img border="0" alt="fugro" src="inc/img/fugro.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://geomatikk-survey.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-geomatikk-survey" data-toggle="tooltip"
			    data-placement="top" title="Geomatikk Survey">
				<img border="0" alt="Geomatikk Survey" src="inc/img/geomatikk-survey.png" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.vianova.no/vnpt/index.htm" target="_blank" class="btn btn-lg btn-block kpx_btn-vianova" data-toggle="tooltip"
			    data-placement="top" title="VIANOVA">
				<img border="0" alt="VIANOVA" src="inc/img/vianova.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://byggcontrol.no/tjenester/camflight-aerial-mapping-robot/" target="_blank" class="btn btn-lg btn-block kpx_btn-bygg-control-as"
			    data-toggle="tooltip" data-placement="top" title="BYGG Control AS">
				<img border="0" alt="BYGG Control AS" src="inc/img/bygg-control-as.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.afgruppen.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-af-gruppen" data-toggle="tooltip"
			    data-placement="top" title="AF GRUPPEN">
				<img border="0" alt="AF GRUPPEN" src="inc/img/af-gruppen.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.powel.com/no/support1/courses/contracting/kurs-pa-foresporsel/oppmaling-anleggsoppmaling/" target="_blank"
			    class="btn btn-lg btn-block kpx_btn-powel" data-toggle="tooltip" data-placement="top" title="powel">
				<img border="0" alt="powel" src="inc/img/powel.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://geodata.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-geodata-as" data-toggle="tooltip" data-placement="top"
			    title="Geodata AS">
				<img border="0" alt="Geodata AS" src="inc/img/geodata-as.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.norconsult.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-norconsult" data-toggle="tooltip"
			    data-placement="top" title="Norconsult">
				<img border="0" alt="Norconsult" src="inc/img/norconsult.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.norgeodesi.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-norgeodesi-as" data-toggle="tooltip"
			    data-placement="top" title="Norgeodesi AS">
				<img border="0" alt="Norgeodesi AS" src="inc/img/norgeodesi-as.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.zenithsurvey.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-zenith-survey" data-toggle="tooltip"
			    data-placement="top" title="ZENITH SURVEY">
				<img border="0" alt="ZENITH SURVEY" src="inc/img/zenith-survey.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.agsurvey.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-AGSURVEY" data-toggle="tooltip" data-placement="top"
			    title="AGSURVEY">
				<img border="0" alt="AGSURVEY" src="inc/img/agsurvey.png" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.blinken.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-blinken-as" data-toggle="tooltip" data-placement="top"
			    title="Blinken a.s">
				<img border="0" alt="Blinken a.s" src="inc/img/blinken-as.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.okartkiosken.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-okartkiosken-no" data-toggle="tooltip"
			    data-placement="top" title="Okartkiosken.no">
				<img border="0" alt="Okartkiosken.no" src="inc/img/okartkiosken-no.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.orientering.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-norsk-orientering" data-toggle="tooltip"
			    data-placement="top" title="NORSK ORIENTERING">
				<img border="0" alt="NORSK ORIENTERING" src="inc/img/norsk-orientering.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="https://www.politi.no/politidirektoratet/aktuelt/nyhetsarkiv/2013_08/Nyhet_12708.xml" target="_blank" class="btn btn-lg btn-block kpx_btn-politiet"
			    data-toggle="tooltip" data-placement="top" title="POLITIET">
				<img border="0" alt="POLITIET" src="inc/img/politiet.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="https://maps.google.com/" target="_blank" class="btn btn-lg btn-block kpx_btn-google" data-toggle="tooltip" data-placement="top"
			    title="Google">
				<img border="0" alt="Google" src="inc/img/google.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
	<div class="row kpx_row-sm-offset-3 kpx_socialButtons">
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://kart.finn.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-finn" data-toggle="tooltip" data-placement="top"
			    title="FINN">
				<img border="0" alt="FINN" src="inc/img/finn.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://kart.gulesider.no/" target="_blank" class="btn btn-lg btn-block kpx_btn-gule-sider" data-toggle="tooltip"
			    data-placement="top" title="GULE SIDER">
				<img border="0" alt="GULE SIDER" src="inc/img/gule-sider.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="https://ruter.no/reiseplanlegger/kart/for/default" target="_blank" class="btn btn-lg btn-block kpx_btn-ruter" data-toggle="tooltip"
			    data-placement="top" title=Ruter# "">
				<img border="0" alt="Ruter#" src="inc/img/ruter.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
		<div class="col-lg-3 col-md-3 col-xs-6 col-sm-6">
			<a href="http://www.yr.no/kart/#lat=65.00146&lon=19.54158&zoom=3&laga=nedb%C3%B8rskyer&proj=3575" target="_blank" class="btn btn-lg btn-block kpx_btn-yr"
			    data-toggle="tooltip" data-placement="top" title="yr">
				<img border="0" alt="yr" src="inc/img/yr.jpg" height="50">
				<span class="hidden-xs"></span>
			</a>
		</div>
	</div>
	<br>
</div>
