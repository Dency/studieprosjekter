<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">
            Informasjon om cookies
        </h1>
        <p class="lead mb-4">
            Cookies(informasjonskapsler) er små midlertidige filer som blir lagret på enheten din i det du besøker nettside(som tar dette i bruk).
            Filene blir slettet automatisk etter en viss tidsperiode eller når nettsiden lukkes, men dette kan administreres om ønskelig. 
            Informasjonskapslene inneholder opplysninger som for eksempel innstillingene dine eller profilinformasjonen din for et nettsted.
        </p>

        <h3 class="mb-4">
            Hvis du ikke godtar at vi lagrer cookies, trykk på den nettleseren du bruker og følg stegene!
        </h3>
        
        <div id="accordion">
            <div class="card">
                    <div class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <img src="inc/img/icons8-google-50.png">
                        </button>
                        <span>Goole Chrome</span>
                    </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <ol>
                            <li>Gå til Chrome</li>
                            <li>Klikk på <span><i class="fas fa-ellipsis-v"></i></span> øverst til høyre og velg <strong>Innstillinger</strong>.</li>
                            <li>Klikk på <i>"Avansert"</i> helt nederst.</li>
                            <li>Under <i>Personvern og sikkerhet</i>, gå til <strong>Innholdsinnstillinger</strong>, deretter til <strong>Informasjonskapsler</strong></li>
                            <li>Du vil få opp et nytt vindu. Trykk på <i>Tillat at nettsteder lagrer og leser data i informasjonskapsler(anbefales)</i> og for å tillate eller blokkere alle nettside fra å lagre informasjonskapsler
                            på din maskin.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                    <div class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <img src="inc/img/icons8-internet-explorer-50.png">
                        </button>
                        <span>Internet Explorer</span>
                    </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <ol>
                            <li>Gå til Internet Explorer.</li>
                            <li>Klikk på <span><i class="fas fa-cog"></i> øverst til høyre hjørne og velg <strong>Alternativer for Internett</strong>.</span></li>
                            <li>Gå til fanen <strong>Personvern</strong> og velg <i>Avansert</i>.</li>
                            <li>Du vil få opp et nytt vindu med valg om hvordan informasjonskapsler håndteres.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                    <div class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <img src="inc/img/icons8-safari-50.png">
                        </button>
                        <span>Safari</span>
                    </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                        <ol>
                            <li>Gå til Safari-appen.</li>
                            <li>Klikk på <strong>Safari <span><i class="fas fa-long-arrow-alt-right"></i></span> Valg</strong> i menyen øverst til venstre.</li>
                            <li>Gå til <i>Personvern</i> og du vil kunne administrere informasjonskapsler.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                    <div class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <img src="inc/img/icons8-firefox-50.png">
                        </button>
                        <span>Firefox</span>
                    </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                    <div class="card-body">
                        <ol>
                            <li>Gå til Firefox.</li>
                            <li>Klikk på <span><i class="fas fa-bars"></i> øverst til høyre og velg <span><i class="fas fa-cog"></i>Innstillinger</span></span></li>
                            <li>Gå til <strong>Personvern</strong> i menyen til venstre.</li>
                            <li>Under <i>Historikk</i>, velg <i>"Firefox skal: Bruke egne innstillinger for historikk".</i></li>
                            <li>Du vil her kunne tillate eller blokkere bruken av informasjonskapsler(cookies).</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                    <div class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            <img src="inc/img/icons8-opera-50.png">
                        </button>
                        <span>Opera</span>
                    </div>
                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                    <div class="card-body">
                        <ol>
                            <li>Gå til Opera</li>
                            <li>Klikk på <span><img src="inc/img/icons8-opera-50.png" heigt="20" width="20"></span>Meny øverst til høyre og velg <span><i class="fas fa-cog"></i></span> Innstillinger</li>
                            <li>Gå til <strong>Personvern &amp; sikkerhet fra sidemenyen til venstre.</strong></li>
                            <li>Gå til infokapsler for å administrere.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="card">
                    <div class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            <img src="inc/img/icons8-microsoft-edge-50.png">
                        </button>
                        <span>Microsoft Edge</span>
                    </div>
                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                    <div class="card-body">
                        <ol>
                            <li>Åpne Edge.</li>
                            <li>Klikk på <span><i class="fas fa-ellipsis-h"></i></span> øverst i høyre hjørnet.</li>
                            <li>Velg <strong>Innstillinger</strong> nederst i lista med valg.</li>
                            <li>Klikk på knappen hvor det står <strong>Vis avanserte innstillinger</strong> nedesrt i lista med innstillinger.</li>
                            <li>Bla ned til du finner <strong>Informasjonskaplser</strong>, klikk på listevalget for å administrere.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
