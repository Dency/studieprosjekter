
<div class="container mt-2">
    <?php
        $query_select_courses = "SELECT `id`, `name`, `points`, `optional`, `season`, `points_per_semester`, `link` FROM `courses`";

        $statement = Db::getPdo()->prepare($query_select_courses);
        $statement->execute();

        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        if ($statement->rowCount())
        {
            $courses = new Box("Courses", "<div class='table-responsive'><table class='table'><tr><th class='thead-dark'>{$message['courses_code_heading']}</th><th>{$message['courses_name_heading']}</th><th>{$message['courses_points_heading']}</th><th>{$message['courses_oblig_heading']}</th><th>{$message['courses_points_heading']} (S)</th><th>{$message['courses_points_heading']} (A)</th></tr>");
            $courses->setClass("col-md-12 col-sm-12 mx-auto mt-3");
            $totalSpring = 0;
            $totalAutumn = 0;
            
            // Gjelder for hver enkelt rad
            foreach($results as $result)
            {
                $courses->append("<tr><td>{$result['id']}</td><td><a href='{$result['link']}'>{$result['name']}</a></td><td>{$result['points']}</td><td>{$result['optional']}</td>");
                if ($result["season"] == "AUTUMN")
                {
                    $courses->append("<td></td><td>{$result['points_per_semester']}</td></tr>");                   
                    $totalAutumn += $result["points_per_semester"];
                }
                else
                {
                    $courses->append("<td>{$result['points_per_semester']}</td><td></td></tr>");                   
                    $totalSpring += $result["points_per_semester"];
                }
            }
            $courses->append("<tr><td></td><td></td><td></td></td><td>Sum:</td><td>{$totalSpring}</td><td>{$totalAutumn}</td></tr>");
            $courses->append("</table></div>");
            echo $courses->show();
        }
    ?>
    <p class="font-weight-bold">*O - Obligatorisk emne, V - Valgfritt emne, Studiepoeng (S) - Første semester, Studiepoeng (A) - Andre semester</p>
   
    <div class="card-deck">
        <div class="card">
            <img class="card-img-top" src="inc/img/GISogkart.jpg" alt="Gis og kart">
            <div class="card-body mt-2">
                <h5 class="card-title">GIS og kart</h5>
                <p>Studenter på <a href="#">GIS og Kart </a>2015: Interaktivt web-kart</p>
            </div>
        </div>
        <div class="card">
            <img class="card-img-top" src="inc/img/databaser.png" alt="Databaser">
            <div class="card-body mt-4">
                <h5 class="card-title">Databaser</h5>
                <p>Les mer om databaser <a href="#">her!</a></p>
            </div>
        </div>
        <div class="card">
            <img class="card-img-top" src="inc/img/landmåling.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Landmåling</h5>
                <p>Les mer om landmåling <a href="#">her!</a></p>
            </div>
        </div>
    </div>
    <div class="card-deck mt-3">
        <div class="card">
            <img class="card-img-top" src="inc/img/geografisk_analyse.jpg" alt="Geografisk analyse">
            <div class="card-body mt-2">
                <h5 class="card-title">Geografisk analyse</h5>
                <p> Studenter i <a href="#">Geografisk analyse</a> 2016</p>
            </div>
        </div>
        <div class="card">
            <img class="card-img-top" src="inc/img/prosjektarbeidGIS.JPG" alt="Prosjektarbeid i GIS">
            <div class="card-body mt-2">
                <h5 class="card-title">Prosjektarbeid i GIS</h5>
                <p>Les mer om <a href="#">Prosjektarbeid i GIS!</a></p>
            </div>
        </div>
        <div class="card">
            <img class="card-img-top" src="inc/img/fjernanalyse.JPG" alt="Fjernanalyse">
            <div class="card-body mt-2">
                <h5 class="card-title">Fjernanalyse</h5>
                <p><a href="#">Fjernanalyse</a> med DJI Phantom3 Professional drone</p>
            </div>
        </div>
        <div class="card">
            <img class="card-img-top" src="inc/img/Arealplanlegging.jpg" alt="Arealplanlegging om miljørett">
            <div class="card-body mt-2">
                <h5 class="card-title">Arealplanlegging og Miljørett</h5>
                <p>Studenter i <a href="#">Arealplanlegging og Miljørett</a> 2016</p>
            </div>
        </div>
    </div>
    <div class="mx-auto mt-3 mb-3" style="width:200px;">
        <a class="nav-link btn btn-primary" target="_blank" href="https://www.usn.no/studier/finn-studier/ingenior-sivilingenior-teknologi-og-it/geografiske-informasjonssystem-pabygging/"><?php echo($message['left_nav_apply']); ?></a>
    </div>

    <div class="clearfix"></div>
</div>