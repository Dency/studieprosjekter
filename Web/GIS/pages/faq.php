<div class="container mb-4">
	<!-- <div class="jumbotron jumbotron-fluid"> -->
		<h1 class="display-4">FAQ</h1>

		<p class="lead mb-4">Ofte Stilte Spørsmål til GIS-studiet, Høgskolen i Sørøst-Norge</p>
		<p class="thick lead mb-4">1.Valg av studieplan</p>

		<div id="accordion">
			<div class="card">
				<div class="card-header" id="headingOne">
					<h5 class="mb-0">
						<button class="btn btn-link line-break" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							Jeg ønsker å ta en bachelor-grad ved HSN hvor jeg får litt grunnleggende kjennskap til bruk av GIS som verktøy
						</button>
					</h5>
				</div>
				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">
						Velg introduksjons-emnet 5708 «GIS og kart» som valg-emne i et bachelor-studie som tillater valg-emner..
						<img src="inc/img/spm1.jpg" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingTwo">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							Jeg ønsker å ta bachelor-grad ved HSN hvor mest mulig GIS inngår som en integrert del av studiet.
						</button>
					</h5>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
					<div class="card-body">
						Velg et eller flere GIS-emner som valg-emne i et bachelor-studium som tillater valg-emner
						<img src="inc/img/spm2.png" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingThree">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							Jeg er bachelor-student ved INMH ved HSN og ønsker å lære grunnleggende bruk av GIS-verktøy.
						</button>
					</h5>
				</div>
				<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
					<div class="card-body">
						I emnet 4112 «Planlegging og miljørett» som inngår i flere av studieretningene ved INHM, inneholder grunnleggende bruk av
						GIS-verktøy.
						<img src="inc/img/spm3.png" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingFour">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							Jeg er bachelor-student ved INMH ved HSN og ønsker å lære grunnleggende bruk av GIS-verktøy.
						</button>
					</h5>
				</div>
				<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
					<div class="card-body">
						I tillegg til det du lærer om GIS i emnet 4112 «Planlegging og miljørett» som inngår i flere av studieretningene ved INHM,
						bør du velge emnet 5708 «GIS og kart» som valg-emne.
						<img src="inc/img/spm4.jpg" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingFive">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
							Jeg er bachelor-student ved INMH ved HSN og ønsker å lære grunnleggende bruk av GIS-verktøy.
						</button>
					</h5>
				</div>
				<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
					<div class="card-body">
						Velg så mange som mulig av GIS-emnene 5708 «GIS og kart», 5709 «Landmåling» og 5702 «Geografisk analyse» som valg-emne.
						<img src="inc/img/spm5.jpg" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingSix">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
							Jeg har bachelor-grad og har arbeidet noen år, men ser at jeg har behov for litt påfyll av grunnleggende GIS-kunnskaper i
							jobben min.
						</button>
					</h5>
				</div>
				<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
					<div class="card-body">
						Ta et emnet 5708 «GIS og kart» ved siden av jobben. Du trenger noen oppmøtedager ila. høstsemesteret og resten av praktiske
						oppgaver og lesing kan du gjøre hjemme.
						<img src="inc/img/spm6.jpg" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingSeven">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
							Jeg er bachelor-student ved INMH ved HSN og ønsker å benytte GIS som verktøy i et master-studium
							ved instituttet.
						</button>
					</h5>
				</div>
				<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
					<div class="card-body">
						Velg relevante GIS-emnene blant emnene 5708 «GIS og kart», 5709 «Landmåling», 5702 «Geografisk analyse» eller 5704 «Fjernanalyse»
						som valg-emne i bachelor-studiet eller i master-studiet.
						<img src="inc/img/spm7.jpg" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingEight">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
							Jeg er bachelor-student ved INMH ved HSN og ønsker å benytte GIS som verktøy i et master-studium
							ved instituttet.
						</button>
					</h5>
				</div>
				<div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
					<div class="card-body">
						Velg flest mulig av GIS-emnene som valg-emne i bachelor-studiet eller i master-studiet. Søk om å få godkjent ytterligere
						spesial-pensum i GIS som en del av master-graden. Velg en master-oppgave hvor du kan få ansatte i GIS-gruppa som med-veileder.


						<img src="inc/img/spm8.jpg" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingNine">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
							Jeg er bachelor-student ved INMH ved HSN og ønsker å benytte GIS som verktøy i et master-studium
							ved instituttet.
						</button>
					</h5>
				</div>
				<div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
					<div class="card-body">
						Velg relevante GIS-emnene blant emnene 5708 «GIS og kart», 5709 «Landmåling», 5702 «Geografisk analyse» eller 5704 «Fjernanalyse»
						som valg-emne i bachelor-studiet eller i master-studiet.
						<img src="inc/img/spm9.jpg" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingTen">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
							Jeg er har bachelor-grad og har arbeidet noen år, men ser at jeg har behov for mye mer GIS-kunnskaper i jobben min.
						</button>
					</h5>
				</div>
				<div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
					<div class="card-body">
						Ta et friår og velg «Årsstudiet i GIS» .
						<img src="inc/img/spm10.jpg" width="20%">
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingEleven">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed line-break" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
							Jeg ønsker å arbeide deltid. Hvilke muligheter har jeg til å gjennomføre et årsstudium i GIS?
						</button>
					</h5>
				</div>
				<div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion">
					<div class="card-body">
						I utgangspunktet er dette ikke et nettstudium. Vi legger stor vekt på praktiske ferdigheter på avanserte verktøy og instru-menter.
						Derfor kreves det en del tilstedeværelse på studiestedet Bø.
						<img src="inc/img/spm11.jpg" width="20%">
					</div>
				</div>
			</div>



			<p class="thick lead mb-4">2.Enkeltemner som selvstudium</p>

			<!-- <div id="accordion"> -->
			<div class="card">
				<div class="card-header" id="headingTwelve">
					<h5 class="mb-0">
						<button class="btn btn-link line-break" data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
							Er det mulig å ta grunnkurset GIS og kart som et selvstudium?
						</button>
					</h5>
				</div>
				<div id="collapseTwelve" class="collapse show" aria-labelledby="headingTwelve" data-parent="#accordion">
					<div class="card-body">
						Alle øvelsene er videodokumentert. Alle kartdata og programvare går på privat PC. Du bør ha minimum 2 oppmøtedager på studiested
						Bø når vi jobber med spesialprogramvare og instrumenter. Teori i norsk og engelsk lærebok kan i prinsippet leses på
						egenhånd.
						<img src="inc/img/spm12.png" width="20%">
					</div>
				</div>
			</div>
		</div>
	<!-- </div> -->
</div>