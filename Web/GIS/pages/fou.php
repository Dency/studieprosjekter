<div class="container">
    <div class="row">
        <h2 class="display-2 text-center col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3">FOU</h2>
        <?php 
            if (!Auth::check())
                $page = new DynamicPage("FOU", strtoupper($lang));
            else
                $page = new DynamicPage("FOU", strtoupper($lang), Auth::user());
                    
                echo $page->render();
        ?>
        <?php 
        $x = 99;
        if ($x > 99)
        {
        ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3 bg-light text-center well">
            <h1 class="tag-title">
                <?php echo($message['fou_gis_sammarbeidet']); ?>
            </h1>
            <hr>
            <p>
                <?php echo($message['fou_gis_sammarbeidet_info']); ?>
            </p>
            <br>
            <a href="ppc.html" class="btn btn-block btn-primary">Learn more</a>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3 bg-light text-center well">
            <h1 class="tag-title">
                <?php echo($message['fou_kartlegging_av_habitat']); ?>
            </h1>
            <hr>
            <p>
                <?php echo($message['fou_kartlegging_av_habitat_info']); ?>
            </p>
            <br>
            <p>1. <a href="http://home.hit.no/~bui/Hjort_Orkdal_19klasser_a.JPG"> 3D visualisering av inndeling av vegetasjonen i 19 klasser. Orkdal tettsted midt i bildet.</a></p>
            <p>2. <a href="http://home.hit.no/~bui/Hjort_Kartlagt_areal.JPG"> Kartleggingsområder på Vestlandet</a></p>
            <p>3. <a href="http://home.hit.no/~bui/HiT-skrift_7-2012.pdf">  Vegetasjonskartlegging med satellittdata. Optimalisering av analysemetodikk</a></p>
            <p>4. <a href="http://home.hit.no/~bui/Hjort_Mysterud_mfl_2011_AREALsluttrapport_liten%20fil.pdf"> Sluttrapport</a></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3 mt-3 bg-light text-center well" text-center>
            <h1 class="tag-title">
                <?php echo($message['fou-vegetasjonskartlegging']);?>
            </h1>
            <hr>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra quam sollicitudin nibh aliquam finibus.
                Etiam efficitur felis vel imperdiet varius. Maecenas bibendum elementum molestie. Lorem ipsum dolor sit amet,
                consectetur adipiscing elit. Mauris cursus finibus semper. Fusce molestie tincidunt leo vel varius. Nam scelerisque
                nulla feugiat leo consequat, id dignissim sem tincidunt. Proin elit mauris, hendrerit in varius sed, facilisis
                sit amet neque.</p>
            <br>
            <a href="ppc.html" class="btn btn-block btn-primary">Learn more</a>
        </div>
        <?php } ?>
    </div>
</div>
</div>