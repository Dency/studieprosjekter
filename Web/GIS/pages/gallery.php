<div class="container-fluid mt-2">
    <div id="myModal" class="galModal">
        <span class="close">&times;</span>
        <img class="galModal-content" id="modalImg">
        <div id="galModalCaption"></div>
    </div>
<?php
// Hvorvidt brukeren kan slette bilder eller ikke
$canDelete = false;
if (Auth::check() && Auth::user()->getGroup()->getPermission("manage") >= 1)
    $canDelete = true;

if (isset($_GET["confirm"]) && $canDelete)
{
    $image = new Photograph($_GET["confirm"]);
    if ($image->getId())
    {
        $file = $image->getImageURL();
        if (unlink($file))
        {
            echo Alert::success("The image has been deleted!");
        }
        else
        {
            echo Alert::warning("Failed to delete the file, attempting to remove from database!");
        }
        $query_delete_photo = "DELETE FROM `photographs` WHERE `id` = :id";
        $statement = Db::getPdo()->prepare($query_delete_photo);
        $statement->execute([":id"  => $image->getId()]);
    }
}

if (isset($_GET["delete"]) && $canDelete)
{
    $confirm = new Box("Confirmation", "", true);
    $confirm->append("<p class='card-text'><strong>Are you sure you want to delete picture ID: {$_GET['delete']}?</strong></p>");
    $confirm->append("<a class='btn btn-lg btn-warning d-block mx-auto mt-1' style='width:50%' href='index.php?page=gallery&confirm={$_GET['delete']}'>Confirm</a>
                     <a class='btn btn-lg btn-primary d-block mx-auto mt-1' style='width:50%' href='index.php?page=gallery'>Return</a>");
    $confirm->setClass("col-lg-6 col-md-6 col-sm-6 col-xs-6 mt-3 mb-3 well mx-auto");

    echo $confirm->show();
}

$photos = array();

if (!isset($_GET["p"]))
{
    $page = 1;
}
else
{
    $page = $_GET["p"];
}

if ($page == 1)
{
    $offset = 0;
}
else
{
    $offset = (9 * $page - 9);
}

$box = new Box("Gallery", "");
$box->setClass("col-lg-8 col-md-8 col-sm-8 col-xs-8 mt-3 mb-3 well mx-auto");

$query_get_count = "SELECT COUNT(`id`) AS `count` FROM `photographs`";
$statement = Db::getPdo()->prepare($query_get_count);
$statement->execute();

$count = $statement->fetchObject();
$count = $count->count;

$query_get_photos = "SELECT `id` FROM `photographs` ORDER BY `time` DESC LIMIT 9 OFFSET " . Db::getMysqli()->escape_string($offset);

$statement = Db::getPdo()->prepare($query_get_photos);

$statement->execute();

if ($statement->rowCount())
{
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    foreach($results as $result) // Opprett objekter med fotografer og push de til slutten av array
    {
        $photo = new Photograph($result["id"], strtoupper($lang), $canDelete);
        array_push($photos, $photo);
    }
}
$box->append("
<script>
var modalView = document.getElementById('myModal');
var modalImg = document.getElementById('modalImg');
var modalCaption = document.getElementById('galModalCaption');
</script>
");
// Hvor mange bilder som er vist
$shown = 0;
foreach($photos as $photo)
{
    if ($shown == 0)
    {
        $box->append("<div class='card-group'>");
    }

    $box->append($photo->show());
    $shown++;
    // Om 3 bilder er vist, lukk div og resett shown
    if ($shown % 3 == 0)
    {
        $box->append("</div>");
        $shown = 0;
    }
} // Om shown er over 0 (om ikke fått 3 på siste rad, lukk div tag)
if ($shown > 0)
    $box->append("</div>");
// Legg til navigasjonsmeny for fram og tilbake om mulig
$box->append("<ul class='pagination justify-content-center mt-3' style='width:100%'>");
if ($page > 1)
{
    $box->append("<li class='page-item'><a class='page-link' href='?page=gallery&p=" . ($page-1) . "'>Previous</a></li>");
}
$new_offset = (9 * ($page+1) - 9);
if ($new_offset < $count)
{
    $box->append("<li class='page-item'><a class='page-link' href='?page=gallery&p=" . ($page+1) . "'>Next</a></li>");
}
        
$box->append("</ul>");
echo $box->show();
?>
</div>