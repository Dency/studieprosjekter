<style>
    .bgimage 
    {
        width: 100%;
        height: 400px;
        background: url('inc/img/index_bannerfoto.jpg');
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
    }

    .bgimage h2 
    {
        color: white;
        line-height: 200px;
    }
</style>

<div class="container">
    <section class="bgimage mb-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <h2>GIS, Høgskolen i Sørøst-Norge</h2>
                    <!-- <small class="text-muted">-Høgskolen i Sørøst-Norge, Bø</small> -->
                    <!-- <p>This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.</p> -->
                    <!-- <p><a href="#" class="btn btn-primary btn-large">Learn more »</a></p> -->
                </div>
            </div>
        </div> 
    </section> 
       
    <hr>

    <div class="card-deck">
        <div class="card">
            <img class="card-img-top" src="inc/img/GISogkart.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">GIS og kart</h5>
                <!-- <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content
                    is a little bit longer.</p> -->
                <a href="#" class="btn btn-primary">Se mer</a>
            </div>
            <!-- <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div> -->
        </div>
        <div class="card">
            <img class="card-img-top" src="inc/img/databaser.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Databaser</h5>
                <!-- <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p> -->
                <a href="#" class="btn btn-primary">Se mer</a>
            </div>
            <!-- <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div> -->
        </div>
        <div class="card">
            <img class="card-img-top" src="inc/img/landmåling.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Landmåling</h5>
                <!-- <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has
                    even longer content than the first to show that equal height action.</p> -->
                <a href="#" class="btn btn-primary">Se mer</a>
            </div>
            <!-- <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div> -->
        </div>
        <div class="card">
            <img class="card-img-top" src="inc/img/geografisk_analyse.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Geografisk analyse</h5>
                <!-- <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has
                    even longer content than the first to show that equal height action.</p> -->
                <a href="#" class="btn btn-primary">Se mer</a>
            </div>
            <!-- <div class="card-footer">
                    <small class="text-muted">Last updated 3 mins ago</small>
                </div> -->
        </div>
    </div>
    <?php
    // Announcements
    $query_select_announcements = "SELECT `id`, `title`, `content`, `time`, `image` FROM `announcements` ORDER BY `time` DESC LIMIT 3";

    $statement = Db::getPdo()->prepare($query_select_announcements);
    $statement->execute();

    $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    if ($statement->rowCount())
    {
        $news = new Box("Announcements", "");
        $news->setClass("mt-3 mb-3");
        foreach($results as $result)
        {
            $news->append("<h2>{$result['title']}</h2>");
            $news->append("<p><i>Updated: {$result['time']}</i></p>");
            if (strlen($result['image']))
            {
                $news->append("<br><img class='img-thumbnail mx-auto d-block' src='{$result['image']}' alt='{$result['title']}'><br>");
            }
            $content = substr($result['content'], 0, 240);
            $content = substr_replace($content, "...", 241);
            $news->append("<p>{$content}<br><a href='index.php?page=announcements&read={$result['id']}'>Read more</a></p><hr>");
        }
        $news->append("<div class='text-center'><a href='index.php?page=announcements'>See all announcements</a></div>");
        echo $news->show();
    }

    ?>
</div>