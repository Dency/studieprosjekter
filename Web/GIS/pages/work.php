<!-- Fjern php koden om du vil se innholdet. -->

<div class="container">
    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3 text-center">Jobbmuligheter og yrkeserfaring</h3>
        <hr>
        <h4 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3">Hva skal du bli?</h4>
        <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3">GIS utdanningen ved HiT tilbyr kunnskap og kompetanse som gir deg mange muligheter for å få relevant jobb innenfor studiet når studiene er ferdig. Her kan du lese mer om noen tidligere GIS studenter fra HiT og hvilke jobber de har.</p>
        <hr>
        <?php 
            if (!Auth::check())
                $page = new DynamicPage("WORK", strtoupper($lang));
            else
                $page = new DynamicPage("WORK", strtoupper($lang), Auth::user());
                    
                echo $page->render();
        ?>

        <?php 
        $x = 99;
        if ($x > 99)
        {
        ?>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <figure>
                <p>Arealplanlegging</p>
                <a href="inc/pdf/Mari.pdf" target="_blank"><img src="inc/img/mari-s.jpg" class="img-thumbnail" alt="Cinque Terre"></a>
                <figcaption>Mari Solbrekken</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <figure>
            <p>Miljøvernleder</p>
            <a href="inc/pdf/Håvard.pdf" target="_blank"><img src="inc/img/håvard-b.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Håvard Bjordal</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>Seksjonsleder</p>
            <a href="inc/pdf/Bård.pdf" target="_blank"><img src="inc/img/bård-s.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Bård Strige Øyen</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>Geographical analyst</p>
            <a href="inc/pdf/Gjermund.pdf" target="_blank"><img src="inc/img/gjermund-j.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Gjermund Jakobsen</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>GIS-koordinator</p>
            <a href="inc/pdf/Tone.pdf" target="_blank"><img src="inc/img/tone-r.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Tone Reinsos Knutsen</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>Landmåling og database</p>
            <a href="inc/pdf/Martine.pdf" target="_blank"><img src="inc/img/martine-v.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Martine Vad Almankaas</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>Systemutvikler</p>
            <a href="inc/pdf/Turid.pdf" target="_blank"><img src="inc/img/turid-w.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Turid Wiik Melve</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>Systemkonsulent</p>
            <a href="inc/pdf/Anne.pdf" target="_blank"><img src="inc/img/anne-l.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Anne Lundgren</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>Teknisk-assistent</p>
            <a href="inc/pdf/Monica.pdf" target="_blank"><img src="inc/img/monica-n.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Monica Nordmarken</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>GIS ingeniør</p>
            <a href="inc/pdf/Sigrid.pdf" target="_blank"><img src="inc/img/sigrid-s.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Sigrid Steig</figcaption>
            </figure>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
        <figure>
            <p>GIS-medarbeider</p>
            <a href="inc/pdf/Suleyman.pdf" target="_blank"><img src="inc/img/suleyman-c.jpg" class="img-thumbnail" alt="Cinque Terre"></a> 
            <figcaption>Suleyman Chukeli</figcaption>
            </figure>
        </div>
        <?php } ?>
    </div>
</div>