<?php
require_once __DIR__ . "/../inc/header.php"; 

if (!Auth::check())
{
    Redirect::to("login.php");
}    

/**
 * TODO:
 * Vil gjøre om uutsendet, inkludere en fast navigasjon som er lastet inn her og da også til alle undersidene
 * Pretty messy writing, will see if I can refactor this
 * IDEA: Om vi får en del submenyer, kan jeg reskrive switchen slik at jeg kan legge mesteparten av navigasjonen under, og inkludere sider deretter
 * IDEA: Write a submenu class
 */

$menu = "<ul class='nav justify-content-center nav-tabs'>";
$menu_end = "</ul>";
if (!isset($_GET['p']))    
{
    $menu .= "<li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=profile'>Profile</a></li>";
    if (Auth::user()->getGroup() != null)
    {
        if (Auth::user()->getGroup()->getPermission("owner") >= 1)
        {
            $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=users'>Users</a></li>";
        }
        if (Auth::user()->getGroup()->getPermission("manage") >= 1)
        {
            $menu .= "
                <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=courses'>Courses</a></li>
                <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=gallery'>Gallery</a></li>
                <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=announcements'>News</a></li>";
        }
    }
    $menu .= $menu_end;
    echo $menu;
    
    echo "<div class='container'><div class='row'>";
    require(__DIR__ . "/pages/profile.php");
}
else
{
    $page = $_GET['p'];
    switch ($page)
    {
        case "users":
        {
            $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=profile'>Profile</a></li>";
            if (Auth::user()->getGroup() != null)
            {
                if (Auth::user()->getGroup()->getPermission("owner") >= 1)
                {
                    $menu .= "<li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=users'>Users</a></li>";
                }
                if (Auth::user()->getGroup()->getPermission("manage") >= 1)
                {
                    $menu .= "
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=courses'>Courses</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=gallery'>Gallery</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=announcements'>News</a></li>";
                }                
            }
            $menu .= $menu_end;
            echo $menu;

            echo "<div class='container'><div class='row'>";
            require(__DIR__ . "/pages/users.php");
            break;
        }
        case "profile":
        {
            $menu .= "<li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=profile'>Profile</a></li>";
            if (Auth::user()->getGroup() != null)
            {
                if (Auth::user()->getGroup()->getPermission("owner") >= 1)
                {
                    $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=users'>Users</a></li>";
                }
                if (Auth::user()->getGroup()->getPermission("manage") >= 1)
                {
                    $menu .= "
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=courses'>Courses</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=gallery'>Gallery</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=announcements'>News</a></li>";               
                }                
            }
            $menu .= $menu_end;
            echo $menu;

            echo "<div class='container'><div class='row'>";
            require(__DIR__ . "/pages/profile.php");
            break;
        }
        case "edit_page":
        {
            $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=profile'>Profile</a></li>";
            if (Auth::user()->getGroup() != null)
            {
                if (Auth::user()->getGroup()->getPermission("owner") >= 1)
                {
                    $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=users'>Users</a></li>";
                }
                if (Auth::user()->getGroup()->getPermission("manage") >= 1)
                {
                    $menu .= "
                        <li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=courses'>Courses</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=gallery'>Gallery</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=announcements'>News</a></li>";            
                }                
            }
            $menu .= $menu_end;
            echo $menu;

            echo "<div class='container'><div class='row'>";
            require(__DIR__ . "/pages/edit_page.php");
            break;
        }
        case "edit_page_info":
        {
            $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=profile'>Profile</a></li>";
            if (Auth::user()->getGroup() != null)
            {
                if (Auth::user()->getGroup()->getPermission("owner") >= 1)
                {
                    $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=users'>Users</a></li>";
                }
                if (Auth::user()->getGroup()->getPermission("manage") >= 1)
                {
                    $menu .= "
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                        <li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=courses'>Courses</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=gourses'>Gallery</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=announcements'>News</a></li>";                
                }                
            }
            $menu .= $menu_end;
            echo $menu;

            echo "<div class='container'><div class='row'>";
            require(__DIR__ . "/pages/edit_page_info.php");
            break;
        }
        case "courses":
        {
            $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=profile'>Profile</a></li>";
            if (Auth::user()->getGroup() != null)
            {
                if (Auth::user()->getGroup()->getPermission("owner") >= 1)
                {
                    $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=users'>Users</a></li>";
                }
                if (Auth::user()->getGroup()->getPermission("manage") >= 1)
                {
                    $menu .= "
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                        <li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=courses'>Courses</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=gallery'>Gallery</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=announcements'>News</a></li>";                
                }                
            }
            $menu .= $menu_end;
            echo $menu;

            echo "<div class='container'><div class='row'>";
            require(__DIR__ . "/pages/courses.php");
            break;
        }
        case "announcements":
        {
            $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=profile'>Profile</a></li>";
            if (Auth::user()->getGroup() != null)
            {
                if (Auth::user()->getGroup()->getPermission("owner") >= 1)
                {
                    $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=users'>Users</a></li>";
                }
                if (Auth::user()->getGroup()->getPermission("manage") >= 1)
                {
                    $menu .= "
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=courses'>Courses</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=gallery'>Gallery</a></li>
                        <li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=announcements'>News</a></li>";                
                }                
            }
            $menu .= $menu_end;
            echo $menu;

            echo "<div class='container'><div class='row'>";
            require(__DIR__ . "/pages/announcements.php");
            break;
        }
        case "gallery":
        {
            $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=profile'>Profile</a></li>";
            if (Auth::user()->getGroup() != null)
            {
                if (Auth::user()->getGroup()->getPermission("owner") >= 1)
                {
                    $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=users'>Users</a></li>";
                }
                if (Auth::user()->getGroup()->getPermission("manage") >= 1)
                {
                    $menu .= "
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=courses'>Courses</a></li>
                        <li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=gallery'>Gallery</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=announcements'>News</a></li>";                
                }                
            }
            $menu .= $menu_end;
            echo $menu;

            echo "<div class='container'><div class='row'>";
            require(__DIR__ . "/pages/gallery.php");
            break;
        }
        default:
        {
            $menu .= "<li class='nav-item'><a class='nav-link active' href='ucp/index.php?p=profile'>Profile</a></li>";
            if (Auth::user()->getGroup() != null)
            {
                if (Auth::user()->getGroup()->getPermission("owner") >= 1)
                {
                    $menu .= "<li class='nav-item'><a class='nav-link' href='ucp/index.php?p=users'>Users</a></li>";
                }
                if (Auth::user()->getGroup()->getPermission("manage") >= 1)
                {
                    $menu .= "
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page'>Edit page</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=edit_page_info'>Edit page information</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=courses'>Courses</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=gallery'>Gallery</a></li>
                        <li class='nav-item'><a class='nav-link' href='ucp/index.php?p=announcements'>News</a></li>";                
                }                    
            }
            $menu .= $menu_end;
            echo $menu;
            
            echo "<div class='container'><div class='row'>";
            require(__DIR__ . "/pages/profile.php");
            break;
        }
    }
}
echo "</div></div>";
require_once __DIR__ . "/../inc/footer.php";
?>