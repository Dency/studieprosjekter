<?php
require_once __DIR__ . "/../inc/header.php";

if (Auth::check())
    Redirect::to("index.php");

if (Input::exists())
{
    $validator = new Validation($lang);
    $validation = $validator->check($_POST, array(
        "username"  =>  array("required" => "true"),
        "password"  =>  array("required" => "true")
    ));

    if ($validation->getPassed())
    {
        if (Auth::attempt(["username" => @$_POST["username"], "password" => $_POST["password"], "keep_logged_in" => isset($_POST["keep_logged_in"])]))
        {
            Redirect::to("index.php");
        }
        else
        {
            $fail_alert = new Alert($message['login_alertdanger_invalid_title'], $message['login_alertdanger_invalid_description'], "danger");
        }
    }
    else
    {
        $fail_alert = new Alert($message['login_alertdanger_unfilled_title'], implode("<br>", $validator->getErrors()), "danger");
    }
}

?>

<div class="container mt-3 mb-3">
    <div class="row">
        <div class="col-md-6 col-sm-8 mx-auto">
            <div class="card border-none">
                <div class="card-body">
                    <?php if (isset($fail_alert)){ echo $fail_alert->show();} ?>
                    <p class="mt-4 lead text-center">
                        <?php echo($message['login_header']); ?>
                    </p>
                    <div class="mt-4">
                        <form role="form" method="post" action="">
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" value="" placeholder="<?php echo($message['login_placeholder_username']); ?>">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" value="" placeholder="<?php echo($message['login_placeholder_password']); ?>">
                            </div>
                            <label class="custom-control custom-checkbox mt-2">
                                <input type="checkbox" class="custom-control-input" name="keep_logged_in">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description"><?php echo($message['login_logged_in']); ?></span>
                            </label>
                            <button type="submit" class="btn btn-primary float-right"><?php echo($message['login_sign_in_button']); ?></button>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <p class="text-center">
                        <?php echo($message['login_ask_if_acc']); ?> <a href="ucp/register.php"><?php echo($message['login_sign_up_now']); ?></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
require_once __DIR__ . "/../inc/footer.php";
?>