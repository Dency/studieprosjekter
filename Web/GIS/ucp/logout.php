<?php
require_once __DIR__ . "/../inc/header.php"; 

if (!Auth::check())
{
    Redirect::to("login.php");
}    

Auth::logout();
Redirect::to("/../");