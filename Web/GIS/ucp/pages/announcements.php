<?php 
require_once __DIR__ . "/../../core/init.php";

if (!defined("IN_PROJECTGIS"))
    die();

if (isset($_GET["delete"]))
{
    $query_delete_announcement = "DELETE FROM `announcements` WHERE `id` = :id";

    $statement = Db::getPdo()->prepare($query_delete_announcement);
    $statement->execute([":id" => $_GET["delete"]]);

    $alert = new Alert("Announcement deleted: {$_GET['delete']}", "The announcement has been deleted from the database", "success");
    echo $alert->show();
}

if (isset($_GET["edit"]))
{
    $query_select_announcement = "SELECT `id`, `title`, `content`, `time`, `image` FROM `announcements` WHERE `id` = :id";
    
    $statement = Db::getPdo()->prepare($query_select_announcement);
    $statement->execute([":id" => $_GET["edit"]]);

    if ($statement->rowCount())
    {
        $edit_box = new Box("Edit: {$_GET['edit']}", "<form action='ucp/index.php?p=announcements' method='POST'><table class='table table-striped'>");
        $edit_box->setClass("col-md-12 col-sm-12 mx-auto mt-3");

        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $edit_box->append("<input type='hidden' name='ID' value='{$_GET['edit']}'>");
        foreach($results as $result)
        {
            foreach($result as $key=>$value)
            {
                if ($key == "id") continue;
                if ($key == "time") continue;

                if ($key != "content")
                {
                    $input = "<input type='text' class='form-control' name='{$key}' value='{$value}'><input type='hidden' name='{$key}_prev' value='{$value}'>";
                }
                else
                {    
                    $input = "<textarea class='form-control' name='{$key}' rows='16' required>{$value}</textarea><input type='hidden' name='{$key}_prev' value='{$value}'>";
                }
                $edit_box->append("<tr><td>" . ucfirst($key) . "</td><td>{$input}</td></tr>");
            }
        }
        $edit_box->append(
            "<tr>
            <td></td>
            <td><button type='submit' name='submit_edit' class='btn btn-primary btn-block'>Save changes</button></td>
        </tr></form></table>"
        );
        echo $edit_box->show();
    }
    else
    {
        $alert = new Alert("Invalid announcement", "Couldn't find announcement: {$_GET['edit']}!", "danger");
        echo $alert->show();
    }
}

if (isset($_POST["submit_edit"]))
{
    $db = Db::getMysqli();
    $id = $db->escape_string($_POST["ID"]); 
    
    $query_update = "UPDATE `announcements` SET ";
    $altered = "";

    $content = str_replace("<br>", "\n", $_POST["content"]);
    $content = htmlentities($content);
    $content = str_replace("\n", "<br>", $content);

    foreach($_POST as $key => $value)
    {
        if (strpos($key, "_prev") !== false) continue;
        if ($key == "submit_edit") continue;
        if ($key == "ID") continue;

        if ($_POST[$key] != $_POST["{$key}_prev"])
        {
            if ($key == "content")
            {
                $query_update .= "`" . $db->escape_string($key) . "` = '{$db->escape_string($content)}', ";
            }
            else
            {
                $query_update .= "`" . $db->escape_string($key) . "` = '{$db->escape_string($value)}', ";
            }
            $altered .= ucFirst($key) . " (" . $_POST["{$key}_prev"] . " <br>to<br> " . $_POST[$key] . ")<br>";
        }
    }
    $query_update = trim($query_update, ", ");
    $query_update .= ", `time` = NOW()";
    $query_update .= " WHERE `id` = {$id}";

    $db->query($query_update);

    $altered = trim($altered, "<br>");
    if (!strlen($altered)) $altered = "None";

    $success_alert = new Alert("Fields altered", $altered, "success");
    $success_alert->addClass("mt-3");
    echo $success_alert->show();
}

if (isset($_POST["submit_new"]))
{
    $content = str_replace("<br>", "\n", $_POST["content"]);
    $content = htmlentities($content);
    $content = str_replace("\n", "<br>", $content);

    $query_insert_announcement = "INSERT INTO `announcements`(`title`, `content`, `time`, `image`) VALUES (:title, :content, NOW(), :image)";

    if (strlen($_POST["title"]))
    {
        $statement = Db::getPdo()->prepare($query_insert_announcement);
        $statement->execute(
            [":title" => $_POST["title"],
            ":content" => $content,
            ":image"    => $_POST["image"]]
        );
        $alert = new Alert("Announcement has been created!", "Successfully inserted announcement into the database.", "success");
    }
    else
    {
        $alert = new Alert("Failed to create announcement!", "A title is required!", "danger");
    }
    echo $alert->show();
}

$query_select_announcements = "SELECT `id`, `title`, `content`, `time`, `image` FROM `announcements`";

$statement = Db::getPdo()->prepare($query_select_announcements);
$statement->execute();

$results = $statement->fetchAll(PDO::FETCH_ASSOC);
if ($statement->rowCount())
{
    $news = new Box("Edit announcements", "<table class='table'><tr><th>Title</th><th>Updated</th><th>Edit</th><th>Delete</th></tr>");
    $news->setClass("col-md-12 col-sm-12 mx-auto mt-3");
    
    foreach($results as $result)
    {
        $news->append("<tr><td>{$result['title']}</td><td>{$result['time']}</td><td><a href='ucp/index.php?p=announcements&edit={$result['id']}'>Edit</a></td><td><a href='ucp/index.php?p=announcements&delete={$result['id']}'>Delete</a></td></tr>");
    }
    $news->append("</table>");
    echo $news->show();
}

$create = new Box("Create announcement", "
<form method='post' action='ucp/index.php?p=announcements'> 
    <input type='text' class='form-control mt-2' name='title' placeholder='Title' required>
    <input type='text' class='form-control mt-2' name='image' placeholder='Image URL, leave empty for none'>
    <textarea name='content' class='form-control mt-2' rows='16' required></textarea>
    <button type='submit' name='submit_new' class='btn btn-primary btn-block mt-3'>Create new</button>    
</form>
");
$create->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5 mb-5");

echo $create->show();