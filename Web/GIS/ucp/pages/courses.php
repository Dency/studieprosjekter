<?php 
require_once __DIR__ . "/../../core/init.php";

if (!defined("IN_PROJECTGIS"))
    die();

if (isset($_POST["submit_new"]))
{
    $validate = new Validation($lang);

    // Only validating the most crucial fields since they are able to edit it afterwards
    $validation = $validate->check($_POST, array(
        "id"  => array(
            "required"  => true,
            "min"       => 1,
            "max"       => 6,
            "unique"    => "courses"
        ),
        "course_name"  => array(
            "required"  => true,
            "min"       => 4
        )
    ));

    if ($validation->getPassed())
    {
        $query_insert_course = "INSERT INTO `courses`(`id`, `name`, `points`, `optional`, `season`, `points_per_semester`, `link`) 
                                VALUES (:id, :name, :points, :optional, :season, :semesterpoints, :link)";

        $statement = Db::getPdo()->prepare($query_insert_course);
        $statement->execute(
                            [
                            ":id"                   => $_POST["id"], 
                            ":name"                 => $_POST["course_name"],
                            ":points"               => $_POST["points"],
                            ":optional"             => $_POST["optional"],
                            ":season"               => $_POST["season"],
                            ":semesterpoints"       => $_POST["points_per_semester"],
                            ":link"                 => $_POST["link"]
                            ]
                        );

        $alert = new Alert("Success", "The course has been inserted into the database", "success");
    }
    else
    {
        $alert = new Alert("Error", implode("<br>", $validation->getErrors()), "danger");
    }

    echo $alert->show();
}

if (isset($_POST["submit_edit"]))
{
    $db = Db::getMysqli();
    $id = $db->escape_string($_POST["ID"]); 
    
    $query_update = "UPDATE `courses` SET ";
    $altered = "";

    foreach($_POST as $key => $value)
    {
        if (strpos($key, "_prev") !== false) continue;
        if ($key == "submit_edit") continue;
        if ($key == "ID") continue;

        if ($_POST[$key] != $_POST["{$key}_prev"])
        {
            $query_update .= "`" . $db->escape_string($key) . "` = '{$db->escape_string($value)}', ";
            $altered .= ucFirst($key) . " (" . $_POST["{$key}_prev"] . " to " . $_POST[$key] . ")<br>";
        }
    }
    $query_update = trim($query_update, ", ");
    $query_update .= " WHERE `id` = {$id}";

    $db->query($query_update);

    $altered = trim($altered, "<br>");
    if (!strlen($altered)) $altered = "None";

    $success_alert = new Alert("Fields altered", $altered, "success");
    $success_alert->addClass("mt-3");
    echo $success_alert->show();
}

if (isset($_GET["delete"]))
{
    $query_delete_course = "DELETE FROM `courses` WHERE `id` = :id";

    $statement = Db::getPdo()->prepare($query_delete_course);
    $statement->execute([":id" => $_GET["delete"]]);

    $alert = new Alert("Course deleted: {$_GET['delete']}", "The course has been deleted from the database", "success");
    echo $alert->show();
}

if (isset($_GET["edit"]))
{
    $query_select_course = "SELECT `name`, `points`, `optional`, `season`, `points_per_semester`, `link` FROM `courses` WHERE `id` = :id";
    $statement = Db::getPdo()->prepare($query_select_course);
    $statement->execute([":id" => $_GET["edit"]]);

    if ($statement->rowCount())
    {
        $edit_box = new Box("Edit: {$_GET['edit']}", "<form action='ucp/index.php?p=courses' method='POST'><table class='table table-striped'>");
        $edit_box->setClass("col-md-12 col-sm-12 mx-auto mt-3");

        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $edit_box->append("<input type='hidden' name='ID' value='{$_GET['edit']}'>");
        foreach($results as $result)
        {
            foreach($result as $key=>$value)
            {
                // By disallowing edits of these fields, we don't need a dropdown menu. These fields probably won't need editing either way, and if so a new can be created!
                if ($key == "season") continue;
                if ($key == "optional") continue;

                $input = "<input type='text' class='form-control' name='{$key}' value='{$value}'><input type='hidden' name='{$key}_prev' value='{$value}'>";
                $edit_box->append("<tr><td>" . ucfirst($key) . "</td><td>{$input}</td></tr>");
            }
        }
        $edit_box->append(
            "<tr>
            <td></td>
            <td><button type='submit' name='submit_edit' class='btn btn-primary btn-block'>Save changes</button></td>
        </tr></form></table>"
        );
        echo $edit_box->show();
    }
    else
    {
        $alert = new Alert("Invalid course", "Couldn't find course: {$_GET['edit']}!", "danger");
        echo $alert->show();
    }
}

$query_select_courses = "SELECT `id`, `name`, `points`, `optional`, `season`, `points_per_semester` FROM `courses`";

$statement = Db::getPdo()->prepare($query_select_courses);
$statement->execute();

$results = $statement->fetchAll(PDO::FETCH_ASSOC);
if ($statement->rowCount())
{
    $courses = new Box("Edit courses", "<table class='table'><tr><th>Code</th><th>Name</th><th>Points</th><th>Obligatory</th><th>Edit</th><th>Delete</th></tr>");
    $courses->setClass("col-md-12 col-sm-12 mx-auto mt-3");
    $totalSpring = 0;
    $totalAutumn = 0;
    
    foreach($results as $result)
    {
        $courses->append("<tr><td>{$result['id']}</td><td>{$result['name']}</td><td>{$result['points']}</td><td>{$result['optional']}</td><td><a href='ucp/index.php?p=courses&edit={$result['id']}'>Edit</a></td><td><a href='ucp/index.php?p=courses&delete={$result['id']}'>Delete</a></td></tr>");
    }
    $courses->append("</table>");
    echo $courses->show();
}

$create = new Box("Add courses", "
<form method='post' action='ucp/index.php?p=courses'> 
    <input type='number' class='form-control' name='id' placeholder='Course code (ID)'>
    <input type='text' class='form-control mt-2' name='course_name' placeholder='Name'>
    <input type='number' class='form-control mt-2' name='points' placeholder='Points'>
    <select name='optional' class='form-control mt-2'>
        <option value='O'>Obligatory</option>
        <option value='V'>Optional</option>
    </select>
    <select name='season' class='form-control mt-2'>
        <option value='AUTUMN'>Autumn</option>
        <option value='SPRING'>Spring</option>
    </select>
    <input type='number' class='form-control mt-2' name='points_per_semester' placeholder='Points per semester'>
    <input class='form-control mt-2' name='link' placeholder='A complete link to the courses page'>
    <button type='submit' name='submit_new' class='btn btn-primary btn-block mt-3'>Create new</button>    
</form>
");
$create->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5 mb-5");

echo $create->show();