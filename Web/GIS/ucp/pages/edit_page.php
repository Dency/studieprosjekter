<?php 
require_once __DIR__ . "/../../core/init.php";

if (!defined("IN_PROJECTGIS"))
    die();

$field_allowed = array("page", "lang", "header", "content", "priority");

if (isset($_POST["submit_new_db"]))
{
    $query_insert_page = "INSERT INTO `dynamic_page`(`page`, `lang`, `header`, `content`, `type`, `priority`) VALUES (:page, :lang, :header, :content, :type, :priority)";
    // For øyeblikket lite serverside validering av data med tanke på at kun administratorer bruker siden
    $statement = Db::getPdo()->prepare($query_insert_page);
    
    $lang = strtoupper($_POST["lang"]);

    $header;
    if (!isset($_POST["header"]))
        $header = "";
    else
        $header = $_POST["header"];


    if ($lang == "NO" || $lang == "EN")
    {       
        $statement->execute([":page" => $_POST["page"],
                            ":lang" => $lang,
                            ":header" => $header,
                            ":content" => $_POST["content"],
                            ":type" => $_POST["type"],
                            ":priority" => $_POST["priority"]]);
    
        Redirect::to("index.php?p=edit_page&list_page={$_POST["page"]}");    
    }
    else
    {
        echo Alert::danger("You haven't filled out the required forms properly. Available languages are: NO, EN.", "mt-3 text-center");
    }
}

if (isset($_POST["submit_new"]) && isset($_POST["type"]))
{
    $type = $_POST["type"];

    $new_box = new Box("New page item - {$type}", "", false);

    $new_box->setClass("col-md-12 col-sm-12 mx-auto mt-3");
    $new_box->append("<table class='table table-striped text-center'>
        <form id='new_page' method='post' action='ucp/index.php?p=edit_page'>
        <input type='hidden' name='type' value='{$_POST["type"]}'>");

    switch($type)
    {
        case "TEXT":
        {
            $new_box->append("
                            <tr><td>Page</td><td><input type='text' name='page' class='form-control'></td></tr>
                            <tr><td>Language (EN/NO)</td><td><input type='text' name='lang' class='form-control'></td></tr>
                            <tr><td>Header</td><td><input type='text' name='header' maxlength='128' class='form-control'></td></tr>
                            <tr><td>Content</td><td><textarea name='content' rows='16' class='form-control' form='new_page'></textarea></td></tr>
                            <tr><td>Priority</td><td><input type='number' min='1' max='99' name='priority' class='form-control' required></td></tr>
                            ");
            break;
        }
        case "IMG":
        {
            $new_box->append("
                            <tr><td>Page</td><td><input type='text' name='page' class='form-control'></td></tr>
                            <tr><td>Language (EN/NO)</td><td><input type='text' name='lang' class='form-control' required></td></tr>
                            <tr><td>Image URL</td><td><input type='text' maxlength='128' minlength='10' name='header' class='form-control' required></td></tr>
                            <tr><td>Description</td><td><input type='text' maxlength='999' name='content' class='form-control' required></td></tr>
                            <tr><td>Priority</td><td><input type='number' min='1' max='99' name='priority' class='form-control' required></td></tr>
                            ");
            break;
        }
        case "HTML":
        {
            $new_box->append("
                            <tr><td>Page</td><td><input type='text' name='page'></td></tr>
                            <tr><td>Language (EN/NO)</td><td><input type='text' name='lang'></td></tr>
                            <tr><td>HTML Code</td><td><textarea name='content' form='new_page' class='form-control' rows='16' required></textarea></td></tr>
                            <tr><td>Priority</td><td><input type='number' min='1' max='99' name='priority' class='form-control' required></td></tr>
                            ");
            break;
        }
    }
    $select_copy = "";
    if ($type == "HTML")
    {
        $select_copy = "
            <select class='form-control' onchange='copyToClipboard(this)'>
                <option value='<p>Text goes here</p>'>Select item to copy</option>
                <option value='<p>Text goes here</p>'>Copy text tag</option>
                <option value='<ul><li>Skriv tekst for første her</li><li>Skriv tekst for andre her</li></ul>'>Copy unordered list tag</option>
                <option value='<a href=\"https://usn.no\">https://www.usn.no</a>'>Copy link tag</option>
                <option value='<ol><li>Skriv tekst for første her</li><li>Skriv tekst for andre her</li></ol>'>Copy ordered list tag</option>
            </select>";
    }

    $new_box->append("
    <tr>
        <td>
            {$select_copy}
        </td>
        <td><button type='submit' name='submit_new_db' class='btn btn-primary btn-block'>Create page</button></td>
    </tr></form></table>");


    $new_box->append(
        '<script>function copyToClipboard(selectObject) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(selectObject.value).select();
            
            //$temp.val().select();
            document.execCommand("copy");
            $temp.remove();
        }</script>'
    );

    
    echo $new_box->show();
}

if (isset($_GET["delete_id"]))
{
    $delete_id = $_GET["delete_id"];
    $query_delete_page_item = "DELETE FROM `dynamic_page` WHERE `id` = :id";
    
    $statement = Db::getPdo()->prepare($query_delete_page_item);
    $statement->execute([":id" => $delete_id]);

    $success_alert = new Alert("Deleted page item", "Deleted page item {$delete_id}", "success");
    echo $success_alert->show();
}

if (isset($_GET["duplicate_id"]))
{
    $duplicate_id = $_GET["duplicate_id"];
    $query_copy_page_item = "INSERT INTO `dynamic_page`(`page`, `lang`, `header`, `content`, `type`, `priority`) 
        SELECT `page`, `lang`, `header`, `content`, `type`, `priority` FROM `dynamic_page` WHERE `id` = :id";
    
    $statement = Db::getPdo()->prepare($query_copy_page_item);
    $statement->execute([":id" => $duplicate_id]);

    $success_alert = new Alert("Copied page item", "Successfully copied page item {$duplicate_id}", "success");
    echo $success_alert->show();
}

if (isset($_POST["submit_edit"]) && isset($_POST["ID"]))
{
    $db = Db::getMysqli();
    $id = $db->escape_string($_POST["ID"]); 
    
    $query_update = "UPDATE `dynamic_page` SET ";
    $altered = "";

    foreach($_POST as $key => $value)
    {
        if (strpos($key, "_prev") !== false) continue;
        if ($key == "submit_edit") continue;
        if ($key == "type") continue;

        if (in_array($key, $field_allowed) && $_POST[$key] != $_POST["{$key}_prev"])
        {
            $query_update .= "`" . $db->escape_string($key) . "` = '{$db->escape_string($value)}', ";
            $altered .= ucFirst($key) . " (" . htmlentities($_POST["{$key}_prev"]) . "<br> to <br>" . htmlentities($_POST[$key]) . ")<br>";
        }
    }
    $query_update = trim($query_update, ", ");
    $query_update .= " WHERE `id` = {$id}";

    $db->query($query_update);

    $altered = trim($altered, "<br>");
    if (!strlen($altered)) $altered = "None";

    $success_alert = new Alert("Fields altered", $altered, "success");
    $success_alert->addClass("mt-3");
    echo $success_alert->show();
}

if (isset($_GET["id"]))
{
    $edit_id = $_GET["id"];

    $query_select_page_item = "SELECT `page`, `lang`, `header`, `content`, `type`, `priority` FROM `dynamic_page` WHERE `id` = :id";

    $statement = Db::getPdo()->prepare($query_select_page_item);
    $statement->execute([":id" => $edit_id]);
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    if (!$statement->rowCount())
    {
        echo Alert::warning("Couldn't find specified page item ID: {$edit_id}");
    }
    else
    {
        $edit_box = new Box("Edit page item - {$edit_id}", "", false);

        $edit_box->setClass("col-md-12 col-sm-12 mx-auto mt-3");
        $edit_box->append("<table class='table table-striped text-center'>
            <form id='edit_page' method='post' action='ucp/index.php?p=edit_page'>
            <input type='hidden' value='{$edit_id}' name='ID'>");
        
        $db_page;
        $db_lang;
        $db_header;
        $db_content;
        $db_type;
        $db_priority;

        foreach($results as $result)
        {
            $db_page = $result["page"];
            $db_lang = $result["lang"];
            $db_header = $result["header"];
            $db_content = htmlentities($result["content"]);
            $db_type = $result["type"];
            $db_priority = $result["priority"];
        }

        $edit_box->append("
                        <input type='hidden' value='{$db_page}' name='page_prev'>
                        <input type='hidden' value='{$db_lang}' name='lang_prev'>
                        <input type='hidden' value='{$db_header}' name='header_prev'>
                        <input type='hidden' value='{$db_content}' name='content_prev'>
                        <input type='hidden' value='{$db_priority}' name='priority_prev'>
                        <input type='hidden' value='{$db_type}' name='type'>
                        ");
        switch($db_type)
        {
            case "TEXT":
            {
                $edit_box->append("
                                <tr><td>Page</td><td><input type='text' value='{$db_page}' name='page' class='form-control'></td></tr>
                                <tr><td>Language (EN/NO)</td><td><input type='text' value='{$db_lang}' name='lang' class='form-control'></td></tr>
                                <tr><td>Header</td><td><input type='textarea' value='{$db_header}' name='header' maxlength='128' class='form-control'></td></tr>
                                <tr><td>Content</td><td><textarea name='content' rows='16' class='form-control' maxlength='999' form='edit_page'>{$db_content}</textarea></td></tr>
                                <tr><td>Priority</td><td><input type='number' min='1' max='99' value='{$db_priority}' name='priority' class='form-control' required></td></tr>
                                ");
                break;
            }
            case "IMG":
            {
                $edit_box->append("
                                <tr><td>Page</td><td><input type='text' value='{$db_page}' name='page' class='form-control'></td></tr>
                                <tr><td>Language (EN/NO)</td><td><input type='text' value='{$db_lang}' name='lang' class='form-control' required></td></tr>
                                <tr><td>Image URL</td><td><input type='text' value='{$db_header}' maxlength='128' minlength='10' name='header' class='form-control' required></td></tr>
                                <tr><td>Description</td><td><input type='text' value='{$db_content}' maxlength='999' name='content' class='form-control' required></td></tr>
                                <tr><td>Priority</td><td><input type='number' min='1' max='99' value='{$db_priority}' name='priority' class='form-control' required></td></tr>
                                ");
                break;
            }
            case "HTML":
            {
                $edit_box->append("
                                <tr><td>Page</td><td><input type='text' value='{$db_page}' name='page' class='form-control'></td></tr>
                                <tr><td>Language (EN/NO)</td><td><input type='textarea' value='{$db_lang}' name='lang' class='form-control'></td></tr>
                                <tr><td>HTML Code</td><td><textarea name='content' maxlength='999' form='edit_page' class='form-control' rows='16' required>{$db_content}</textarea></td></tr>
                                <tr><td>Priority</td><td><input type='number' min='1' max='99' value='{$db_priority}' name='priority' class='form-control' required></td></tr>
                                ");
                break;
            }
        }
        $select_copy = "";
        if ($db_type == "HTML")
        {
            $select_copy = "
                <select class='form-control' onchange='copyToClipboard(this)'>
                    <option value='<p>Text goes here</p>'>Select item to copy</option>
                    <option value='<p>Text goes here</p>'>Copy text tag</option>
                    <option value='<ul><li>Skriv tekst for første her</li><li>Skriv tekst for andre her</li></ul>'>Copy unordered list tag</option>
                    <option value='<a href=\"https://usn.no\">https://www.usn.no</a>'>Copy link tag</option>
                    <option value='<ol><li>Skriv tekst for første her</li><li>Skriv tekst for andre her</li></ol>'>Copy ordered list tag</option>
                </select>";
        }
        $edit_box->append("
        <tr>
            <td>
                {$select_copy}
            </td>
            <td>
                <button type='submit' name='submit_edit' class='btn btn-primary btn-block'>Save changes</button>
            </td>
        </tr></form></table>");

        $edit_box->append(
            '<script>function copyToClipboard(selectObject) {
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val(selectObject.value).select();
                
                //$temp.val().select();
                document.execCommand("copy");
                $temp.remove();
            }</script>'
        );

        echo $edit_box->show();
    }
}

if (isset($_GET["list_page"]))
{
    $page = $_GET["list_page"];

    $query_select_page_items = "SELECT `id`, `header`, `content`, `type`, `priority`, `lang` FROM `dynamic_page` WHERE `page` = :page ORDER BY `lang`, `priority`";
    $statement = Db::getPdo()->prepare($query_select_page_items);
    $statement->execute([":page" => $page]);
    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    if (!$statement->rowCount())
    {
        echo Alert::warning("Couldn't find any page items for: {$page}");
    }
    else
    {
        $page_items = new Box("Page items - {$page}", "", false);

        $page_items->append("
            <table class='table table-striped'>
                <tr>
                    <th>Edit/ID</th>
                    <th>Header</th>
                    <th>Content</th>
                    <th>Type</th>
                    <th>Priority</th>
                    <th>Language</th>
                    <th>Copy</th>
                    <th>Delete</th>
                </tr>"
            );
        
        foreach($results as $result)
        {
            $page_items->append("
            <tr>
                <td><a href='ucp/index.php?p=edit_page&id={$result["id"]}'>Edit: {$result["id"]}</a></td>
                <td>" . substr(htmlentities($result["header"]), 0, 12) . "...</td>
                <td>" . substr(htmlentities($result["content"]), 0, 32) . "...</td>
                <td>{$result["type"]}</td>
                <td>{$result["priority"]}</td>
                <td>{$result["lang"]}</td>
                <td><a href='ucp/index.php?p=edit_page&duplicate_id={$result["id"]}'>Copy</a></td>
                <td><a href='ucp/index.php?p=edit_page&delete_id={$result["id"]}'>Delete</a></td>
            </tr> 
            ");
        }
        $page_items->append("</table>");
        $page_items->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5");

        echo $page_items->show();
    }
}

$pages = new Box("Pages", "", true);
$pages->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5");

$query_select_pages_distinct = "SELECT DISTINCT(`page`) FROM `dynamic_page`";
$statement = Db::getPdo()->prepare($query_select_pages_distinct);
$statement->execute();
$results = $statement->fetchAll(PDO::FETCH_ASSOC);
$append = "";
foreach ($results as $result)
{
    $append .= "
    <h3>
        <a href='ucp/index.php?p=edit_page&list_page={$result["page"]}'>{$result["page"]}</a>
    </h3>
    <br>";
}

$append = trim($append, "<br>");
$pages->append($append);

echo $pages->show();

$new_page = new Box("New page", "
    <form method='post' action='ucp/index.php?p=edit_page'> 
        <select name='type' class='form-control'><option value='TEXT'>TEXT</option><option value='IMG'>IMG</option><option value='HTML'>HTML</option></select>
        <button type='submit' name='submit_new' class='btn btn-primary btn-block mt-3'>Create new</button>    
    </form>
        ");

$new_page->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5 mb-5");

echo $new_page->show();

