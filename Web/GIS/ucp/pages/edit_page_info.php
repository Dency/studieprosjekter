<?php
require_once __DIR__ . "/../../core/init.php";

if (!defined("IN_PROJECTGIS"))
    die();

if (isset($_POST["request_info"]) && isset($_POST["page_info"]))
{
    $curPage = $_POST["page_info"];
    $content = "";
    $info = new Box("Edit page information - " . strtoupper(str_replace("_", " ", $curPage)), "");
    $info->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5");
    switch(strtolower($curPage))
    {
        case "general":
        {   
            $content .= "
            <div class='alert alert-info' role='alert'>
                <p>
                    For each individual page there is a template and additional information on how to edit/post content.</p>
                    It is highly recommended to read through this page to understand the basic formating. There is also a dropdown menu in the <b>Edit page</b> where you
                    simply click on the tag you want(it get's automatically copied for you), and paste it in the editor.
                </p>
            </div>

            <hr>

            <div>
                <h3>Priority explanation</h3>
                
                <p>This webpage use something called priority. The priority attribute can be found in<b> Edit page</b>.
                The priority spans from 1-99, where 1 is the highest, and 99 is the lowest priority.</p>
                <p>The priority number decides which content is being shown in an ascending order, where some <mark>Example_content_1 with priority level of 1</mark> will be shown at the verry top
                and an other <mark>Example_content_2 with priority level of 2</mark> will be shown under Example_content_1, and so forth.</p>

                <hr>

                <h3>Walkthrough of useful text formating:</h3>

                <p>The only thing you need to know/do is where and how to write the header, content, ordered lists, unordered lists, links and some simple fonts such as eg.<i> italic</i>.</p>
                <p>To the left you can see how the formating looks like, and to the right you can see how you can achieve the formating you want by following the explanation.</p>
                <p>The <q>formating rules</q> are as follows. Let's say you want to write a paragraph, then you would need to use the p-tag.</p>
                <p>The p-tag consists of an opening tag ". htmlentities("<p> and </p>") ." a closing tag.<br> All the content you want to write goes in the middle of the opening tag ". htmlentities("<p>") ." the closing tag ". htmlentities("</p>") .".</p>
                <strong class='mb-3'>It is highly important to close the tag(s) you are using.</strong>

                <h4><u>Example:</u></h4>

                <p>". htmlentities("<p>") ."<u>The content goes here in the middle between the opening tag and the closing tag.</u> ". htmlentities("</p>") ."</p>
                
                <hr>
               
                <div class='row'>

                    <div class='col-lg-6 mb-4 border-right'>
                        <h1>Here I use an H1-tag, which is used as a heading.</h1>
                    </div>

                    <div class='col-lg-6 '>
                        " . htmlentities("<h1>This is a H1-tag, which is used as a heading.</h1>") .  "
                    </div>

                    <div class='col-lg-6 mb-4 border-right'>
                        <h6>Here I use an H6-tag, which is used as a heading, as you can see, the H6 element is much smaller than the H1 element. H1 is the biggest, then comes H2 which some smaller than H1, then H3, and so on.</h6>
                    </div>
                    <div class='col-lg-6'>
                        " . htmlentities("This is a H6-tag, which is used as a heading, as you can see, the H6-tag is much smaller than the H1-tag. H1 is the biggest, then comes H2 which some smaller than H1, then H3, and so on.") . "
                    </div>

                    <div class='col-lg-6 border-right'>
                        <p>Here I use a p-tag.</>
                    </div>
                    <div class='col-lg-6'>
                        " . htmlentities("<p>This is a p-tag.</p>") . "
                    </div>

                    <div class='col-lg-6 mb-4 border-right'>
                        <a href='https://vg.no' target='_blank'>This is a link, press me!</a>
                    </div>
                    <div class='col-lg-6'>
                        " . htmlentities('<a href="https://vg.no" target="_blank">https://www.vg.no</a>') . "
                    </div>

                    <div class='col-lg-6 mb-2 border-right'>
                        <p>In this sentence I'm breaking the line, <br> as such. Look to the right to see how you can do it.</p>
                    </div>
                    <div class='col-lg-6'>
                        " . htmlentities("<p>In this sentence I'm breaking the line, <br> as such. Look to the right to see how you can do it.</p>") . "
                    </div>

                    <div class='col-lg-6 mb-4 border-right'>
                        <i>Here I use the italic-tag, which is handy to use sometimes.</i>
                    </div>
                    <div class='col-lg-6'>
                        " . htmlentities("<i>Here I use the italic-tag, which is handy to use sometimes.</i>") . "
                    </div>
                    
                    <div class='col-lg-6 mb-3 border-right'>
                        <p>Here I use the strong-tag, use <strong> when the context says that the bold text is more important than the other.</strong></p>
                    </div>
                    <div class='col-lg-6'>
                        " .  htmlentities("<p>Here I use the strong -, use <strong> when the context says that the bold text is more important than the other.</strong></p>") . "
                    </div>

                    <div class='col-lg-6 mb-2 border-right'>
                        <p>Here I use the <b>bold-tag</b>, use bold tags when you want to emphasize something.</p>
                    </div>
                    <div class='col-lg-6'>
                        " . htmlentities("<p>Here I use the <b>bold-tag</b>, use bold tags when you want to emphasize something.</p>") . "
                    </div>

                    <div class='col-lg-6 mb-3 border-right'>
                        <p>If you want to underline some text, <u>you can use the u-tag</u></p>
                    </div>
                    <div class='col-lg-6'>
                        " . htmlentities("<p>If you want to underline some text, <u>you can use the u-tag</u></p>") . "
                    </div>

                    <div class='col-lg-6 border-right'>
                        <p>If you want to list something in a numerical order, you can do it like this:</p>
                        <ol>
                            <li>List item one</li>
                            <li>List item two</li>
                            <li>List item three</li>
                        </ol>
                    </div>
                    <div class='col-lg-6' style='padding-top:1em;'>
                        " . htmlentities("<ol>") . "
                        <div class='clearfix'></div>
                            <div style='padding-left:2em;'>
                                " . htmlentities("<li>List item one</li>") . "
                                <div class='clearfix'></div>
                                " . htmlentities("<li>List item two</li>") . "
                                <div class='clearfix'></div>
                                " . htmlentities("<li>List item three</li>") . "
                            </div>
                        " . htmlentities("</ol>") . "
                        <p>ol means <i>Ordered list</i></p>
                    </div>

                    <div class='col-lg-6 border-right'>
                        <p>If you want to list something in a numerical order, you can do it like this:</p>
                        <ul>
                            <li>List item one</li>
                            <li>List item two</li>
                            <li>List item three</li>
                        </ul>
                        <p><u>As shown above, you need to add as many opening and closing " . htmlentities("<li> & </li>") . " for each list item you want in your list.</u></p>
                    </div>
                    <div class='col-lg-6' style='padding-top:1em;'>
                        " . htmlentities("<ul>") . "
                        <div class='clearfix'></div>
                            <div style='padding-left:2em;'>
                                " . htmlentities("<li>List item one</li>") . "
                                <div class='clearfix'></div>
                                " . htmlentities("<li>List item two</li>") . "
                                <div class='clearfix'></div>
                                " . htmlentities("<li>List item three</li>") . "
                            </div>
                        " . htmlentities("</ul>") . "
                        <p>ul means <i>Unordered list</i></p>
                    </div>

                    <div class='col-lg-12'>
                        <hr>
                        <p>This is only the very basic tags that is commonly used. If you understand how to use the tags above, and want to explore some more visit <a href='https://www.w3schools.com/html/html_formatting.asp' target='_blank'>this page</a> for more information.
                    </div>

                </div>

            </div>
            ";
            break;
        }
        case "fou":
        {
            $code = htmlentities(
'
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3 bg-light text-center well">
<h1 class="tag-title">'.$message["fou_gis_sammarbeidet"].'</h1>
<hr>
<p>'
.$message["fou_gis_sammarbeidet_info"].'
</p>
<br>
<a href="ppc.html" class="btn btn-block btn-primary">Learn more</a>
</div>
'
            );

            $content .= "
            <h3 class='mb-3'>This is how the text box looks like:</h3>
            <div class='border border-dark mb-5'>
                <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3 mb-3 bg-light well'>
                    <h1 class='tag-title'>
                        {$message['fou_gis_sammarbeidet']}
                    </h1>

                    <hr>

                    <p>
                        {$message['fou_gis_sammarbeidet_info']}
                    </p>

                    <br>
                    
                    <a href='ppc.html' class='btn btn-block btn-primary'>Learn more</a>
                </div>
            </div>
            <h3>And this is the code that makes the box above(all text, dots etc):</h3>

            <hr>
            <pre><code>
                {$code}
            </code></pre>
            <hr class='mb-5'>
            <div>
                <h3></h3>
                <hr>
              
            </div>
            ";
            break;
        }
        case "gis_study":
        {
            $content .= "gis study";
            break;
        }
        case "about":
        {
            $code = htmlentities(
'
<div class="jumbotron bg-grey rounded-0 col-sm-12 col-md-6 col-lg-3 mt-1">

<img src="inc/img/Arne.Hjeltnes-hit.no_employee.jpg" width="100%">   
<h2 class="mt-2">Arne Hjeltnes</h2>
<h3 class="text-muted">'.$message['about_Stilling'].'</h3>
<p>'.$message['about_Arne_Hjeltnes_description'].'</p>
<a type="E-mail" href="mailto:Arne.Hjeltnes@usn.no" target="_blank" class="font-weight-bold">Arne.Hjeltnes@usn.no</a>
<br>
<a href="tel:35962726" class="font-weight-bold">35 95 27 26</a>
<br>
<a class="btn btn-primary" target="_blank" href="https://www.usn.no/about-usn/contact-us/employees/arne-william-hjeltnes-article201584-7531.html"><'.$message['about_Kjell_Øyvind_Kjenstad_More'].'</a>

</div>
'
);
            $content .= "
            <h3 class='mb-3'>This is how the about info persona looks like</h3>
            <div class='border border-dark mb-5'>
                <div class='jumbotron bg-grey rounded-0 col-sm-12 col-md-6 col-lg-3 mt-1'>
                    <img src='inc/img/Arne.Hjeltnes-hit.no_employee.jpg' width='100%'>   
                    <h2 class='mt-2'>Arne Hjeltnes</h2>
                    <h3 class='text-muted'>{$message['about_Stilling']}</h3>
                    <p>{$message['about_Arne_Hjeltnes_description']}</p>
                    <a type='E-mail' href='mailto:Arne.Hjeltnes@usn.no' target='_blank' class='font-weight-bold'>Arne.Hjeltnes@usn.no</a>
                    <br>
                    <a href='tel:35962726' class='font-weight-bold'>35 95 27 26</a>
                    <br>
                    <a class='btn btn-primary' target='_blank' href='https://www.usn.no/about-usn/contact-us/employees/arne-william-hjeltnes-article201584-7531.html'>{$message['about_Kjell_Øyvind_Kjenstad_More']}</a>
                </div>
            </div>

            <h3>And this is the code that makes the box above(all text, dots etc):</h3>

            <hr>
            <pre><code>
                {$code}
            </code></pre>
            <hr class='mb-5'>
            <div>
                <h3></h3>
                <hr>
              
            </div>
            ";
            break;
        }
        default:
        {
            $content .= "unknown / general, up to you matti";
            break;
        }
    }
    $info->append($content);
    echo $info->show(); 
}

$info_box = new Box("Edit page information", "", true);
$info_box->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5 mb-5");

$info_box->append(
    "<h5>Which page would you like to see information regarding?</h5>
    <form action='ucp/index.php?p=edit_page_info' method='POST'>
        <select name='page_info' class='form-control'>
            <option value='general'>General</option>
            <option value='fou'>FOU</option>
            <option value='gis_study'>GIS studiet</option>
            <option value='about'>About us</option>
        </select>
        <button type='submit' name='request_info' class='btn btn-primary btn-block mt-3'>See Information</button>
    </form>"
);

echo $info_box->show();