<?php 
require_once __DIR__ . "/../../core/init.php";

if (!defined("IN_PROJECTGIS"))
    die();

class ValidationException extends Exception { }

$upload_directory = "../images";

if (isset($_POST["submitFile"]))
{
    $target_file = $upload_directory . "/" . basename($_FILES["fileUpload"]["name"]);

    $file = $_FILES["fileUpload"];
    
    $allowed_types = array("image/gif", "image/png", "image/jpg", "image/jpeg");
    $allowed_extensions = array("gif", "png", "jpg", "jpeg");

    if (is_uploaded_file($file["tmp_name"]))
    {
        try
        {
            if (!in_array(strtolower($file["type"]), $allowed_types))
                throw new ValidationException("Only gif, png, jpg, and jpeg files are allowed.");

            $size = getimagesize($file["tmp_name"]);
            if (empty($size))
                throw new ValidationException("The image could not be processed. Make sure the image is not corrupt and of the correct format.");

            $extension = pathinfo($file["name"], PATHINFO_EXTENSION);
            if (!in_array(strtolower($extension), $allowed_extensions))
                throw new ValidationException("Only gif, png, jpg, and jpeg files are allowed.");
            
            move_uploaded_file($file["tmp_name"], $target_file);
            
            $query_insert_image = "INSERT INTO `photographs`(`filename`, `type`, `caption`, `time`) VALUES (:filename, :type, :caption, NOW())";

            $fileCaption = "";
            
            if (isset($_POST["fileCaption"]))
            {
                $fileCaption = $_POST["fileCaption"];
            }

            $statement = Db::getPdo()->prepare($query_insert_image);
            $statement->execute(
                [":filename" => $file["name"],
                ":type"      => $file["type"],
                ":caption"   => $fileCaption]
            );

            echo Alert::success("The file has been uploaded successfully.");
        }
        catch (ValidationException $e)
        {
            echo Alert::warning($e->getMessage());
        }
    }
    else
    {
        echo Alert::warning("Failed to upload file!");
    }
}

$upload = new Box(
    "Upload images",
    "<form action='ucp/index.php?p=gallery' method='POST' enctype='multipart/form-data'>
        <div class='form-group'>
            <label for='fileUpload' class='mt-1'>Select file to upload</label>
            <input type='file' class='form-control-file mt-1' id='fileUpload' name='fileUpload'>
            <label for='fileCaption' class='mt-1'>Enter caption</label>
            <textarea class='form-control mt-1' name='fileCaption' rows='3'></textarea>
            <button type='submit' name='submitFile' class='form-control btn-primary mt-1 mx-auto' style='width:50%'>Submit file</button>
        </div>
    </form>
    "
);
$upload->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5 mb-5");
echo $upload->show();
