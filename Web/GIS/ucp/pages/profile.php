<?php 
require_once __DIR__ . "/../../core/init.php";

if (!defined("IN_PROJECTGIS"))
    die();

$profil = new Box("Your Profile", "<h5 class='card-title'>" . $message['ucp_welcome'] . ", " . Auth::user()->getName() . " (" . Auth::user()->getUserName() . ")</h5>", true);
$profil->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5 mb-5");
$profil->append("<p class='card-text'>
        " . $message['ucp_yourjoindate'] . " <strong>" . Auth::user()->getJoinDate() . "</strong><br>" . $message['ucp_yourgroup'] . " <strong>"
    );
        
if (Auth::user()->getGroup() == null)
{
    $profil->append($message['ucp_group_unregistered'] . "</strong></p>");
}
else
{
    $profil->append(Auth::user()->getGroup()->getGroupName() . "</strong></p>");

    if (Auth::user()->getGroup()->getPermission("manage") >= 1)
        $profil->append("<br><strong>You have the rights to manage the page.</strong>");
    
    if (Auth::user()->getGroup()->getPermission("owner") >= 0 && Auth::user()->getGroup()->getPermission("owner") < 1337)
        $profil->append("<br><strong>You have the rights to look up and modify user accounts.</strong>");
    else
        $profil->append("<br><strong>You have full ownership rights of the page.</strong>");

    $profil->append("</p>");
}

$profil->append("<a href='/ucp/logout.php' class='btn btn-primary'>" . $message['ucp_logout'] .  "</a>");

echo $profil->show();
?>

