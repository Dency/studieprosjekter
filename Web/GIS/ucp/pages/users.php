<?php 
require_once __DIR__ . "/../../core/init.php";

/**
 * TODO: Use jQuery to hide obsolete table headers / data for better look on mobile
 */

if (!defined("IN_PROJECTGIS"))
    die();

if (!Auth::check())
    Redirect::to("login.php");

if (Auth::user()->getGroup()->getPermission("owner") != 1337)
    Redirect::to("login.php");

$edit_id = (int) @$_GET["edit"];
$field_hidden = array("password", "group_id", "group_name");
$field_disabled = array("joined");
$field_allowed = array("group_id", "name", "username");
$search_url = "";

if (isset($_POST["submit_edit"]) && isset($_POST["ID"]))
{
    $db = Db::getMysqli();
    $id = $db->escape_string($_POST["ID"]); 
    
    $query_update = "UPDATE `users` SET ";
    $altered = "";

    foreach($_POST as $key => $value)
    {
        if (strpos($key, "_prev") !== false) continue;
        if ($key == "submit_edit") continue;
        if ($key == "group_id") continue;

        if (in_array($key, $field_allowed) && $_POST[$key] != $_POST["{$key}_prev"])
        {
            $query_update .= "`" . $db->escape_string($key) . "` = '{$db->escape_string($value)}', ";
            $altered .= ucFirst($key) . " (" . $_POST["{$key}_prev"] . " to " . $_POST[$key] . ")<br>";
        }
    }
    
    // Do group seperately so we can show the groups name
    if ($_POST["group_id"] != $_POST["group_id_prev"])
    {
        if ($_POST["group_id"] == 0)
        {
            $group_prev = "Unregistered";
            if ($_POST["group_id_prev"] != 0)
            {
                $group = new Group($_POST["group_id_prev"]);
                $group_prev = $group->getGroupName();
            }

            $query_update .= "`group_id` = NULL, ";
            $altered .= "Group (" . $group_prev . " to Unregistered)<br>";
        }
        else
        {
            $group_prev = "Unregistered";
            if ($_POST["group_id_prev"] != 0)
            {
                $group = new Group($_POST["group_id_prev"]);
                $group_prev = $group->getGroupName();
            }
            $group = new Group($_POST["group_id"]);
            $group_new = $group->getGroupName();
            
            $query_update .= "`group_id` = " . $db->escape_string($_POST["group_id"]) . ", ";
            $altered .= "Group ({$group_prev} to {$group_new})<br>";
        }
    }
    
    $query_update = trim($query_update, ", ");
    $query_update .= " WHERE `id` = {$id}";

    $db->query($query_update);

    if (isset($_POST["password"]))
    {
        $password = $_POST["password"];
        if (strlen($password))
        {
            if (strlen($password) >= 6)
            {
                $password = password_hash($password, Config::get("password.algo"), ["cost" => Config::get("password.cost")]);
                $query_update_password = "UPDATE `users` SET `password` = :password WHERE `id` = :id"; 
                $statement = Db::getPdo()->prepare($query_update_password);
                $statement->execute([":password" => $password, ":id" => $id]);
    
                $altered .= "Password";
            }
            else
            {
                $error_alert = new Alert("Error", "Password must be at least 6 characters long!<br>The password was not changed.<br>", "danger");
                $error_alert->addClass("mt-3");
                echo $error_alert->show();
            }
        }
    }

    $altered = trim($altered, "<br>");
    if (!strlen($altered)) $altered = "None";

    $success_alert = new Alert("Fields altered", $altered, "success");
    $success_alert->addClass("mt-3");
    echo $success_alert->show();
}

if (isset($_GET["edit"]))
{
    $query_check_user = "SELECT u.`username`, u.`password`, u.`name`, u.`joined`, u.`group_id`, g.`name` AS `group_name` FROM `users` u LEFT JOIN `groups` g ON (g.`id` = u.`group_id`) WHERE u.`id` = :id";

    $statement = Db::getPdo()->prepare($query_check_user);
    $statement->execute([":id" => $edit_id]);
    if (!$statement->rowCount())
    {
        echo Alert::warning("The requested user does not exist.");
        return;
    }
    else
    {
        // Not used for queries, but for display mostly, might be used in the future
        $user = new User($edit_id);

        $edit_box = new Box("User Attributes - " . $user->getUsername());
        $edit_box->setClass("col-md-12 col-sm-12 mx-auto mt-3");
        $edit_box->append("<table class='table table-striped text-center'>
            <form method='post' action='ucp/index.php?p=users'>
            <input type='hidden' value='{$edit_id}' name='ID'>");
         
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $current_group_id;
        foreach($results as $result)
        {
            $current_group_id = $result["group_id"]; 
            foreach($result as $key=>$value)
            {
                $capitalizedKey = ucFirst($key);
                if (!in_array($key, $field_hidden))
                {
                    if (!in_array($key, $field_disabled))
                    {
                        $input = "<input type='text' value='{$value}' name='{$key}'><input type='hidden' name='{$key}_prev' value='{$value}'>";
                        $edit_box->append("<tr><td>{$capitalizedKey}</td><td>{$input}</td></tr>");
                    }
                    else
                    {
                        //$input = "<input type='text' value='{$value}' name='{$key}' readonly='readonly'>";
                        $edit_box->append("<tr><td>{$capitalizedKey}</td><td>{$value}</td></tr>");
                    }
                }
            }
        }
        $edit_box->append("<tr><td>Password</td><td><input type='password' value='' name='password'></td></tr>");
        
        $query_check_groups = "SELECT `id`, `name` FROM `groups`"; 
        $statement = Db::getPdo()->prepare($query_check_groups);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        $edit_box->append("<tr><td>Set group</td><td><select name='group_id'><option value='0'>Unregistered</option>");
        foreach($results as $result)
        {
            $edit_box->append(($result["id"] != $current_group_id) ? ("<option value='{$result['id']}'>{$result['name']}</option>") : ("<option selected='selected' value='{$result['id']}'>{$result['name']}</option>"));
        }
        $edit_box->append("</select></td>");
        $edit_box->append("<input type='hidden' name='group_id_prev' value='{$current_group_id}'>");
        $edit_box->append("
        <tr>
            <td></td>
            <td><button type='submit' name='submit_edit' class='btn btn-primary'>Save changes</button></td>
        </tr></form></table>");

        echo $edit_box->show();
    }
}

if (isset($_GET["search-user"]) || isset($_GET["search-name"]))
{
    $username = trim($_GET["search-user"], " %_ ");
    if (!strlen($username))
        $username = "*";
    
    $name = trim($_GET["search-name"], " %_ ");
    if (!strlen($name))
        $name = "*";

    $results = new Box("Search results: {$username}", "");
    $results->setClass("col-md-12 col-sm-12 mx-auto mt-3");
    
    $sql_username = str_replace("*", "%", $username);
    $sql_name = str_replace("*", "%", $name);

    $query_search_user = "SELECT COUNT(*) AS `rows` FROM `users` WHERE `username` LIKE :username AND `name` LIKE :name";
    
    $statement = Db::getPdo()->prepare($query_search_user);
    $statement->execute([":username" => $sql_username, ":name" => $sql_name]);

    if ($statement->rowCount())
    {
        $order_valid = array
        (
            "id",
            "username",
            "joined"
        );

        $order_by = @$_GET["order_by"];

        if (!in_array($order_by, $order_valid))
            $order_by = "id";

        $order = @$_GET["order"];

        if ($order != "ASC" && $order != "DESC")
            $order = "ASC";

        $lim_min;
        $lim_max;
        
        if (isset($_GET["lim_min"]) && is_numeric(@$_GET["lim_min"]))
            $lim_min = htmlentities($_GET["lim_min"]);
        else
            $lim_min = 0;

        if (isset($_GET["lim_max"]) && is_numeric(@$_GET["lim_max"]))
            $lim_max = htmlentities($_GET["lim_max"]);
        else
            $lim_max = $lim_min+25;

        if (abs($lim_max - $lim_min) > 100)
        {
            $lim_min = 0;
            $lim_max = 25;
        }

        $lim = $lim_max-$lim_min;

        $query_search_result = "SELECT u.`id`, u.`username`, u.`joined`, u.`group_id`, g.`name` AS `group_name` FROM `users` u 
        LEFT JOIN `groups` g ON (g.`id` = u.`group_id`) WHERE u.`username` LIKE :username AND u.`name` LIKE :name ORDER BY `{$order_by}` {$order} LIMIT {$lim_min}, {$lim}";
        
        $statement = Db::getPdo()->prepare($query_search_result);
        
        $statement->bindParam(":username", $sql_username);
        $statement->bindParam(":name", $sql_name);

        $statement->execute();
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);

        $order_url;
        if ($order == "DESC")
            $order_url = "ASC";
        else
            $order_url = "DESC";

        $search_url = "ucp/index.php?p=users&search-user={$username}&search-name={$name}&lim_min={$lim_min}&lim_max={$lim_max}&order={$order_url}";

        $table = "<table class='table'>
        <tr>
            <th><a href='{$search_url}&order_by=id'>ID</a></th>
            <th><a href='{$search_url}&order_by=username'>Username</a></th>
            <th><a href='{$search_url}&order_by=joined'>Join date</a></th>
            <th>Group</th>
            <th>Edit user</th>
        </tr>";

        $row_id = 0;
        foreach($data as $row)
        {
            if ($row_id % 25 == 0)
                $results->append($table);

            $results->append("<tr>
                <td>" . $row["id"] . "</td>
                <td>" . $row["username"] . "</td>
                <td>" . $row["joined"] . "</td>
                <td>" . $row["group_name"] . "</td>
                <td><a class='btn btn-primary' href='ucp/index.php?p=users&edit={$row['id']}'>Edit</a></td>
            </tr>
            ");
            $row_id++;
        }
    
        $results->append("</table>");

        $lim_min2 = $lim_min;
        $lim_max2 = $lim_max;
        $limit = $lim_max - $lim_min; 

        $results->append("<ul class='pagination justify-content-center'>");
        if ($lim_min > 0)
        {
            if ($lim_min - $limit < 0)
            {
                $lim_min2 = 0;
                $lim_max2 = $limit;
            }
            else
            {
                $lim_min2 -= $limit;
                $lim_max2 -= $limit;
            }
            $results->append("<li class='page-item'><a class='page-link' href='ucp/index.php?p=users&search-user={$username}&search-name={$name}&lim_min={$lim_min2}&lim_max={$lim_max2}&order={$order_url}'>Previous</a></li>");
        }

        $lim_min2 = $lim_min + $limit;
        $lim_max2 = $lim_max + $limit;
     
        $results->append("<li class='page-item'><a class='page-link' href='ucp/index.php?p=users&search-user={$username}&search-name={$name}&lim_min={$lim_min2}&lim_max={$lim_max2}&order={$order_url}'>Next</a></li>");
        echo $results->show();
    }
    else
    {
        $results->append("No users found.");

        echo $results->show();
    }
}

$test = new Box("Search Users", '
    <form method="get" action="">
        <input type="hidden" name="p" value="users">
        <div class="form-group mx-auto" style="width:50%;">
            <label for="search-user">Search up the user you want to modify</label>
            <input type="text" class="form-control mt-1" name="search-user" aria-describedby="helpId" placeholder="Username">
            <input type="text" class="form-control mt-3" name="search-name" aria-describedby="helpId" placeholder="Name">
            <button type="submit" class="btn btn-info btn-lg btn-block mt-3">Search</button>
            <small id="helpId" class="form-text text-muted">You can write exact names for exact matches, or you may use * for a wildcard at any length.</small>
        </div>
    </form>');
$test->setClass("col-md-12 col-sm-12 col-lg-12 mx-auto mt-5 mb-5");
echo $test->show();
?>
    