<?php
require_once __DIR__ . "/../inc/header.php"; 

if (Input::exists())
{
    $validate = new Validation($lang);

    //     $validation = $validator->check($_POST, array(
    //     "username"  =>  array("required" => "true"),
    //     "password"  =>  array("required" => "true")
    // ));


    $validation = $validate->check($_POST, array(
        "username"  => array(
            "required"  => true,
            "min"       => 4,
            "max"       => 20,
            "unique"    => "users"
        ),
        "password"  => array(
            "required"  => true,
            "min"       => 6
        ),
        "confirm-password" => array(
            "required"  => true,
            "matches"   => "password"
        ),
        "full_name"      => array(
            "required"  => true,
            "min"       => 8,
            "max"       => 64
        )
    ));
// * For eksempel: $credentials = [
//      * "username"   => "navn",
//      * "password"   => "passord",
//      * "name"       => "Ola Nordmann
//      * ];
        // if (Auth::attempt(["username" => @$_POST["username"], "password" => $_POST["password"], "keep_logged_in" => isset($_POST["keep_logged_in"])]))

    if ($validation->getPassed())
    {
        $id = Auth::register(["username" => $_POST["username"], "password" => $_POST["password"], "name" => $_POST["full_name"]]);
        if ($id > 0)
        {
            Redirect::to("index.php");
        }
        else
        {
            $fail_alert = new Alert($message['register_alertdanger_invalid_title'], $message['register_alertdanger_invalid_description'], "danger");
        }
    }
    else
    {
        $fail_alert = new Alert($message['register_alertdanger_unfilled_title'], implode("<br>", $validation->getErrors()), "danger");
    }
}
?>
<!-- TODO: les opp på bootstrap form control -->
<div class="container mt-3 mb-3">
    <div class="row">
        <div class="col-md-6 col-sm-8 mx-auto">
            <div class="card border-none">
                <div class="card-body">
                    <?php if (isset($fail_alert)){ echo $fail_alert->show();} ?>
                    <div class="mt-2 text-center">
                        <h2><?php echo $message['register_h2_createuser']; ?></h2>
                    </div>
                    <p class="mt-4 text-white lead text-center">
                        <?php echo $message['register_p_signuptextcenter']; ?>
                    </p>
                    <div class="mt-4">
                        <form role="form" method="POST" action="">
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" value="" placeholder="<?php echo $message['login_placeholder_username'];?>">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="full_name" value="" placeholder="<?php echo $message['register_placeholder_name'];?>" >
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" value="" placeholder="<?php echo $message['login_placeholder_password']; ?>">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="confirm-password" value="" placeholder="<?php echo $message['register_placeholder_confirm_pass'];?>">
                            </div>
                            <button type="submit" class="btn btn-primary btn-block"><?php echo $message['register_button_submit']; ?></button>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <p class="text-center">
                        <?php echo $message['register_p_alreadyhave']; ?> <a href="ucp/login.php"><?php echo $message['register_link_login']; ?></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once __DIR__ . "/../inc/footer.php";
?>